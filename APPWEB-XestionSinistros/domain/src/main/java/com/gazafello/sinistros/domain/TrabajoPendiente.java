package com.gazafello.sinistros.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "TrabajoPendiente.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.TrabajoPendiente(tp.id, tp.gremio, tp.trabajoRealizado) FROM TrabajoPendiente tp ORDER BY tp.id")
@NamedEntityGraph(name = "trabajoPendiente", includeAllAttributes = true)
@Table(name = "trabajo_pendiente")
public class TrabajoPendiente implements Serializable {

    private static final long serialVersionUID = -7270136878976048330L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_gremio")
    private Gremio gremio;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_trabajo_realizado", nullable = false)
    private TrabajoRealizado trabajoRealizado;

    public TrabajoPendiente() {
	super();
    }

    public TrabajoPendiente(Integer id, Gremio gremio, TrabajoRealizado trabajoRealizado) {
	super();
	this.id = id;
	this.gremio = gremio;
	this.trabajoRealizado = trabajoRealizado;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Gremio getGremio() {
	return gremio;
    }

    public void setGremio(Gremio gremio) {
	this.gremio = gremio;
    }

    public TrabajoRealizado getTrabajoRealizado() {
	return trabajoRealizado;
    }

    public void setTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
	this.trabajoRealizado = trabajoRealizado;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((gremio == null) ? 0 : gremio.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((trabajoRealizado == null) ? 0 : trabajoRealizado.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TrabajoPendiente other = (TrabajoPendiente) obj;
	if (gremio == null) {
	    if (other.gremio != null)
		return false;
	} else if (!gremio.equals(other.gremio))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (trabajoRealizado == null) {
	    if (other.trabajoRealizado != null)
		return false;
	} else if (!trabajoRealizado.equals(other.trabajoRealizado))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "TrabajoPendiente [id=" + id + ", gremio=" + gremio + ", trabajoRealizado=" + trabajoRealizado + "]";
    }

}
