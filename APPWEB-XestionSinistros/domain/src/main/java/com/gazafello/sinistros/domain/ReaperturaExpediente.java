package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@NamedQuery(name = "ReaperturaExpediente.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.ReaperturaExpediente(e.id, e.fechaReapertura) FROM ReaperturaExpediente e WHERE e.fechaBaja IS NULL ORDER BY e.fechaReapertura DESC")
@NamedEntityGraph(name = "reaperturaExpediente", includeAllAttributes = true)
@Table(name = "reapertura_expediente")
public class ReaperturaExpediente implements Serializable {

    private static final long serialVersionUID = -8026955812763556864L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "motivo", nullable = true, unique = false)
    private String motivo;

    @Column(name = "fecha_reapertura", nullable = true, unique = false)
    private Date fechaReapertura;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_EXPEDIENTE", nullable = false)
    @JsonBackReference(value = "expedienteReaperturaExpediente")
    private Expediente expediente;

    public ReaperturaExpediente(Integer id, Date fechaReapertura) {
	super();
	this.id = id;
	this.fechaReapertura = fechaReapertura;
    }

    public ReaperturaExpediente() {
	super();
    }

    public ReaperturaExpediente(Integer id, String motivo, Date fechaReapertura, Date fechaAlta, String usuarioAlta, Date fechaModificacion, String usuarioModificacion,
	    Date fechaBaja, String usuarioBaja, Expediente expediente) {
	super();
	this.id = id;
	this.motivo = motivo;
	this.fechaReapertura = fechaReapertura;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.expediente = expediente;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getMotivo() {
	return motivo;
    }

    public void setMotivo(String motivo) {
	this.motivo = motivo;
    }

    public Date getFechaReapertura() {
	return fechaReapertura;
    }

    public void setFechaReapertura(Date fechaReapertura) {
	this.fechaReapertura = fechaReapertura;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Expediente getExpediente() {
	return expediente;
    }

    public void setExpediente(Expediente expediente) {
	this.expediente = expediente;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((expediente == null) ? 0 : expediente.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((fechaReapertura == null) ? 0 : fechaReapertura.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((motivo == null) ? 0 : motivo.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ReaperturaExpediente other = (ReaperturaExpediente) obj;
	if (expediente == null) {
	    if (other.expediente != null)
		return false;
	} else if (!expediente.equals(other.expediente))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (fechaReapertura == null) {
	    if (other.fechaReapertura != null)
		return false;
	} else if (!fechaReapertura.equals(other.fechaReapertura))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (motivo == null) {
	    if (other.motivo != null)
		return false;
	} else if (!motivo.equals(other.motivo))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ReaperturaExpediente [id=" + id + ", motivo=" + motivo + ", fechaReapertura=" + fechaReapertura + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta
		+ ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja
		+ ", expediente=" + expediente + "]";
    }

}
