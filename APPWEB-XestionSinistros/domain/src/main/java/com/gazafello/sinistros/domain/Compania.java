package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

@Entity
@NamedQuery(name = "Compania.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Compania(c.id, c.nombre, c.fechaAlta, c.usuarioAlta) FROM Compania c WHERE c.fechaBaja IS NULL ORDER BY c.nombre")
@NamedEntityGraph(name = "compania", includeAllAttributes = true)
public class Compania implements Serializable {
    @Transient
    private static final long serialVersionUID = -9121332635976602513L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre", length = 200, nullable = false, unique = true)
    private String nombre;

    @Column(name = "tipo_modelo", length = 1, nullable = true, unique = false)
    private String tipoModelo;

    @Column(name = "nombre_modelo", length = 200, nullable = true, unique = false)
    private String nombreModelo;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getTipoModelo() {
	return tipoModelo;
    }

    public void setTipoModelo(String tipoModelo) {
	this.tipoModelo = tipoModelo;
    }

    public String getNombreModelo() {
	return nombreModelo;
    }

    public void setNombreModelo(String nombreModelo) {
	this.nombreModelo = nombreModelo;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Compania() {
	super();
    }

    public Compania(Integer id, String nombre, Date fechaAlta, String usuarioAlta) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
    }

    public Compania(String nombre, String tipoModelo, String nombreModelo) {
	super();
	this.nombre = nombre;
	this.tipoModelo = tipoModelo;
	this.nombreModelo = nombreModelo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
	result = prime * result + ((nombreModelo == null) ? 0 : nombreModelo.hashCode());
	result = prime * result + ((tipoModelo == null) ? 0 : tipoModelo.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Compania other = (Compania) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nombre == null) {
	    if (other.nombre != null)
		return false;
	} else if (!nombre.equals(other.nombre))
	    return false;
	if (nombreModelo == null) {
	    if (other.nombreModelo != null)
		return false;
	} else if (!nombreModelo.equals(other.nombreModelo))
	    return false;
	if (tipoModelo == null) {
	    if (other.tipoModelo != null)
		return false;
	} else if (!tipoModelo.equals(other.tipoModelo))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Compania [id=" + id + ", nombre=" + nombre + ", tipoModelo=" + tipoModelo + ", nombreModelo=" + nombreModelo + ", fechaAlta=" + fechaAlta + ", usuarioAlta="
		+ usuarioAlta + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja="
		+ usuarioBaja + "]";
    }

}
