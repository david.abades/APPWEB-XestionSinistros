package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Gremio.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Gremio(g.id, g.nombre, g.fechaAlta, g.usuarioAlta) FROM Gremio g WHERE g.fechaBaja IS NULL ORDER BY g.nombre")
@NamedEntityGraph(name = "gremio", includeAllAttributes = true)
public class Gremio implements Serializable {
    private static final long serialVersionUID = -9121332635976602513L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre", length = 200, nullable = false, unique = true)
    private String nombre;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    public Gremio() {
	super();
    }

    public Gremio(Integer id, String nombre, Date fechaAlta, String usuarioAlta) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Gremio other = (Gremio) obj;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nombre == null) {
	    if (other.nombre != null)
		return false;
	} else if (!nombre.equals(other.nombre))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Gremio [id=" + id + ", nombre=" + nombre + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion
		+ ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja + "]";
    }

}
