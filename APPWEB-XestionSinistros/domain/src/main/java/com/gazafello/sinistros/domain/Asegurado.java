package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

@Entity
@NamedQuery(name = "Asegurado.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Asegurado(a.id, a.nombre, a.numeroDocumento) FROM Asegurado a WHERE a.fechaBaja IS NULL ORDER BY a.nombre")
@NamedEntityGraph(name = "asegurado", includeAllAttributes = true)
public class Asegurado implements Serializable {
    @Transient
    private static final long serialVersionUID = 3801381557531918309L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre", length = 400, nullable = false, unique = true)
    private String nombre;

    @Column(name = "numero_documento", length = 10, nullable = true, unique = true)
    private String numeroDocumento;

    @Column(name = "telefono_fijo", length = 500, nullable = true, unique = false)
    private String telefonoFijo;

    @Column(name = "telefono_mobil", length = 500, nullable = true, unique = false)
    private String telefonoMobil;

    @Column(name = "fax", length = 500, nullable = true, unique = false)
    private String fax;

    @Column(name = "email", length = 500, nullable = true, unique = false)
    private String email;

    @Column(name = "persona_contacto", length = 1000, nullable = true, unique = false)
    private String personaContacto;

    @Column(name = "observaciones", length = 2000, nullable = true, unique = false)
    private String observaciones;

    @Column(name = "direccion", length = 1000, nullable = true, unique = false)
    private String direccion;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_codigo_postal")
    private CodigoPostal codigoPostal;

    public Asegurado() {
	super();
    }

    public Asegurado(Integer id, String nombre, String numeroDocumento) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.numeroDocumento = numeroDocumento;
    }

    public Asegurado(Integer id, String nombre, String numeroDocumento, String telefonoFijo, String telefonoMobil, String fax, String email, String personaContacto,
	    String observaciones, String direccion, Date fechaAlta, String usuarioAlta, Date fechaModificacion, String usuarioModificacion, Date fechaBaja, String usuarioBaja,
	    CodigoPostal codigoPostal) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.numeroDocumento = numeroDocumento;
	this.telefonoFijo = telefonoFijo;
	this.telefonoMobil = telefonoMobil;
	this.fax = fax;
	this.email = email;
	this.personaContacto = personaContacto;
	this.observaciones = observaciones;
	this.direccion = direccion;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.codigoPostal = codigoPostal;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getNumeroDocumento() {
	return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
	this.numeroDocumento = numeroDocumento;
    }

    public String getTelefonoFijo() {
	return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
	this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMobil() {
	return telefonoMobil;
    }

    public void setTelefonoMobil(String telefonoMobil) {
	this.telefonoMobil = telefonoMobil;
    }

    public String getFax() {
	return fax;
    }

    public void setFax(String fax) {
	this.fax = fax;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPersonaContacto() {
	return personaContacto;
    }

    public void setPersonaContacto(String personaContacto) {
	this.personaContacto = personaContacto;
    }

    public String getObservaciones() {
	return observaciones;
    }

    public void setObservaciones(String observaciones) {
	this.observaciones = observaciones;
    }

    public String getDireccion() {
	return direccion;
    }

    public void setDireccion(String direccion) {
	this.direccion = direccion;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public CodigoPostal getCodigoPostal() {
	return codigoPostal;
    }

    public void setCodigoPostal(CodigoPostal codigoPostal) {
	this.codigoPostal = codigoPostal;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((codigoPostal == null) ? 0 : codigoPostal.hashCode());
	result = prime * result + ((direccion == null) ? 0 : direccion.hashCode());
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + ((fax == null) ? 0 : fax.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
	result = prime * result + ((numeroDocumento == null) ? 0 : numeroDocumento.hashCode());
	result = prime * result + ((observaciones == null) ? 0 : observaciones.hashCode());
	result = prime * result + ((personaContacto == null) ? 0 : personaContacto.hashCode());
	result = prime * result + ((telefonoFijo == null) ? 0 : telefonoFijo.hashCode());
	result = prime * result + ((telefonoMobil == null) ? 0 : telefonoMobil.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Asegurado other = (Asegurado) obj;
	if (codigoPostal == null) {
	    if (other.codigoPostal != null)
		return false;
	} else if (!codigoPostal.equals(other.codigoPostal))
	    return false;
	if (direccion == null) {
	    if (other.direccion != null)
		return false;
	} else if (!direccion.equals(other.direccion))
	    return false;
	if (email == null) {
	    if (other.email != null)
		return false;
	} else if (!email.equals(other.email))
	    return false;
	if (fax == null) {
	    if (other.fax != null)
		return false;
	} else if (!fax.equals(other.fax))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nombre == null) {
	    if (other.nombre != null)
		return false;
	} else if (!nombre.equals(other.nombre))
	    return false;
	if (numeroDocumento == null) {
	    if (other.numeroDocumento != null)
		return false;
	} else if (!numeroDocumento.equals(other.numeroDocumento))
	    return false;
	if (observaciones == null) {
	    if (other.observaciones != null)
		return false;
	} else if (!observaciones.equals(other.observaciones))
	    return false;
	if (personaContacto == null) {
	    if (other.personaContacto != null)
		return false;
	} else if (!personaContacto.equals(other.personaContacto))
	    return false;
	if (telefonoFijo == null) {
	    if (other.telefonoFijo != null)
		return false;
	} else if (!telefonoFijo.equals(other.telefonoFijo))
	    return false;
	if (telefonoMobil == null) {
	    if (other.telefonoMobil != null)
		return false;
	} else if (!telefonoMobil.equals(other.telefonoMobil))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Asegurado [id=" + id + ", nombre=" + nombre + ", numeroDocumento=" + numeroDocumento + ", telefonoFijo=" + telefonoFijo + ", telefonoMobil=" + telefonoMobil
		+ ", fax=" + fax + ", email=" + email + ", personaContacto=" + personaContacto + ", observaciones=" + observaciones + ", direccion=" + direccion + ", fechaAlta="
		+ fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja="
		+ fechaBaja + ", usuarioBaja=" + usuarioBaja + ", codigoPostal=" + codigoPostal + "]";
    }
}
