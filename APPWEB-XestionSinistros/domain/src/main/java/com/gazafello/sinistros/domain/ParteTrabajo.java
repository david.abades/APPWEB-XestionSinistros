package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@NamedQuery(name = "ParteTrabajo.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.ParteTrabajo(pt.id, pt.fechaParte, pt.expediente) FROM ParteTrabajo pt WHERE pt.fechaBaja IS NULL ORDER BY pt.fechaParte")
@NamedEntityGraph(name = "parteTrabajo", includeAllAttributes = true)
@Table(name = "parte_trabajo")
public class ParteTrabajo implements Serializable {
    private static final long serialVersionUID = -7463847505432272869L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fecha_parte", nullable = false)
    private Date fechaParte;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_expediente")
    @Fetch(FetchMode.SELECT)
    @JsonBackReference(value = "expedienteParteTrabajo")
    private Expediente expediente;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "id_gremio")
    private Gremio gremio;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "id_reparador")
    private Reparador reparador;

    @OneToOne(mappedBy = "parteTrabajo")
    @JsonManagedReference(value = "parteTrabajoPerjudicado")
    private Perjudicado perjudicado;

    @OneToMany(mappedBy = "parteTrabajo", fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JsonManagedReference(value = "parteTrabajoTrabajosRealizados")
    private List<TrabajoRealizado> trabajosRealizados;

    public ParteTrabajo() {
	super();
	this.trabajosRealizados = new ArrayList<TrabajoRealizado>();
    }

    public ParteTrabajo(Integer id, Date fechaParte, Expediente expediente) {
	super();
	this.id = id;
	this.fechaParte = fechaParte;
	this.expediente = expediente;
    }

    public ParteTrabajo(Integer id, Date fechaParte, Date fechaAlta, String usuarioAlta, Date fechaModificacion, String usuarioModificacion, Date fechaBaja, String usuarioBaja,
	    Expediente expediente, Gremio gremio, Reparador reparador, Perjudicado perjudicado) {
	super();
	this.id = id;
	this.fechaParte = fechaParte;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.expediente = expediente;
	this.gremio = gremio;
	this.reparador = reparador;
	this.perjudicado = perjudicado;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getFechaParte() {
	return fechaParte;
    }

    public void setFechaParte(Date fechaParte) {
	this.fechaParte = fechaParte;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Expediente getExpediente() {
	return expediente;
    }

    public void setExpediente(Expediente expediente) {
	this.expediente = expediente;
    }

    public Gremio getGremio() {
	return gremio;
    }

    public void setGremio(Gremio gremio) {
	this.gremio = gremio;
    }

    public Reparador getReparador() {
	return reparador;
    }

    public void setReparador(Reparador reparador) {
	this.reparador = reparador;
    }

    public Perjudicado getPerjudicado() {
	return perjudicado;
    }

    public void setPerjudicado(Perjudicado perjudicado) {
	this.perjudicado = perjudicado;
    }

    public List<TrabajoRealizado> getTrabajosRealizados() {
	return trabajosRealizados;
    }

    public void setTrabajosRealizados(List<TrabajoRealizado> trabajosRealizados) {
	this.trabajosRealizados = trabajosRealizados;
    }

    public void addTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
	this.trabajosRealizados.add(trabajoRealizado);
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((expediente == null) ? 0 : expediente.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((fechaParte == null) ? 0 : fechaParte.hashCode());
	result = prime * result + ((gremio == null) ? 0 : gremio.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((reparador == null) ? 0 : reparador.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ParteTrabajo other = (ParteTrabajo) obj;
	if (expediente == null) {
	    if (other.expediente != null)
		return false;
	} else if (!expediente.equals(other.expediente))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (fechaParte == null) {
	    if (other.fechaParte != null)
		return false;
	} else if (!fechaParte.equals(other.fechaParte))
	    return false;
	if (gremio == null) {
	    if (other.gremio != null)
		return false;
	} else if (!gremio.equals(other.gremio))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (reparador == null) {
	    if (other.reparador != null)
		return false;
	} else if (!reparador.equals(other.reparador))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ParteTrabajo [id=" + id + ", fechaParte=" + fechaParte + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion
		+ ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja + ", expediente=" + expediente + ", gremio=" + gremio
		+ ", reparador=" + reparador + ", trabajosRealizados=" + trabajosRealizados + "]";
    }

}
