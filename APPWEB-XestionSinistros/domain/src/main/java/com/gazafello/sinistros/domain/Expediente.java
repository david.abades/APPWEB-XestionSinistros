package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@NamedQuery(name = "Expediente.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Expediente(e.id, e.fechaInicial, e.urgente, e.tratamientoTiempoReal, e.numeroExpediente, e.estado, e.asegurado) FROM Expediente e WHERE e.fechaBaja IS NULL ORDER BY e.fechaInicial")
@NamedEntityGraph(name = "expediente", includeAllAttributes = true)
public class Expediente implements Serializable {
    private static final long serialVersionUID = -6980455737044714025L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fecha_inicial", nullable = false, unique = false)
    private Date fechaInicial;

    @Column(name = "fecha_creacion", nullable = false, unique = false)
    private Date fechaCreacion;

    @Column(name = "numero_poliza", length = 100, nullable = true, unique = false)
    private String numeroPoliza;

    @Column(name = "franquicia", nullable = true, unique = false)
    private Double franquicia;

    @Column(name = "urgente", length = 1, nullable = true, unique = false)
    private String urgente;

    @Column(name = "ttr", length = 1, nullable = true, unique = false)
    private String tratamientoTiempoReal;

    @Column(name = "numero_expediente", length = 100, nullable = true, unique = false)
    private String numeroExpediente;

    @Column(name = "descripcion_servicio", nullable = true, unique = false)
    private String descripcionServicio;

    @Column(name = "danos_producidos", nullable = true, unique = false)
    private String danosProducidos;

    @Column(name = "ruta_carpeta", length = 1000, nullable = true, unique = false)
    private String rutaCarpeta;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_perito")
    private Perito perito;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_asegurado")
    private Asegurado asegurado;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_compania")
    private Compania compania;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_estado")
    private Estado estado;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "expediente")
    @Fetch(FetchMode.SELECT)
    @JsonManagedReference(value = "expedienteParteTrabajo")
    private List<ParteTrabajo> partesTrabajo;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "expediente")
    @Fetch(FetchMode.SELECT)
    @JsonManagedReference(value = "expedientePerjudicado")
    private List<Perjudicado> perjudicados;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "expediente")
    @JsonManagedReference(value = "expedienteReaperturaExpediente")
    private List<ReaperturaExpediente> reaperturasExpediente;

    public Expediente() {
	super();
	this.reaperturasExpediente = new ArrayList<ReaperturaExpediente>();
	this.partesTrabajo = new ArrayList<ParteTrabajo>();
	this.perjudicados = new ArrayList<Perjudicado>();
    }

    public Expediente(Integer id, Date fechaInicial, String urgente, String tratamientoTiempoReal, String numeroExpediente, Estado estado, Asegurado asegurado) {
	super();
	this.id = id;
	this.fechaInicial = fechaInicial;
	this.urgente = urgente;
	this.tratamientoTiempoReal = tratamientoTiempoReal;
	this.numeroExpediente = numeroExpediente;
	this.estado = estado;
	this.asegurado = asegurado;
	this.reaperturasExpediente = new ArrayList<ReaperturaExpediente>();
	this.partesTrabajo = new ArrayList<ParteTrabajo>();
	this.perjudicados = new ArrayList<Perjudicado>();
    }

    public Expediente(Integer id, Date fechaInicial, Date fechaCreacion, String numeroPoliza, Double franquicia, String urgente, String tratamientoTiempoReal,
	    String numeroExpediente, String descripcionServicio, String danosProducidos, String rutaCarpeta, Date fechaAlta, String usuarioAlta, Date fechaModificacion,
	    String usuarioModificacion, Date fechaBaja, String usuarioBaja, Perito perito, Asegurado asegurado, Compania compania, Estado estado) {
	super();
	this.id = id;
	this.fechaInicial = fechaInicial;
	this.fechaCreacion = fechaCreacion;
	this.numeroPoliza = numeroPoliza;
	this.franquicia = franquicia;
	this.urgente = urgente;
	this.tratamientoTiempoReal = tratamientoTiempoReal;
	this.numeroExpediente = numeroExpediente;
	this.descripcionServicio = descripcionServicio;
	this.danosProducidos = danosProducidos;
	this.rutaCarpeta = rutaCarpeta;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.perito = perito;
	this.asegurado = asegurado;
	this.compania = compania;
	this.estado = estado;
	this.reaperturasExpediente = new ArrayList<ReaperturaExpediente>();
	this.partesTrabajo = new ArrayList<ParteTrabajo>();
	this.perjudicados = new ArrayList<Perjudicado>();
    }

    public Expediente(Integer id, Date fechaInicial, Date fechaCreacion, String numeroPoliza, Double franquicia, String urgente, String tratamientoTiempoReal,
	    String numeroExpediente, String descripcionServicio, String danosProducidos, String rutaCarpeta, Date fechaAlta, String usuarioAlta, Date fechaModificacion,
	    String usuarioModificacion, Date fechaBaja, String usuarioBaja, Perito perito, Asegurado asegurado, Compania compania, Estado estado, List<ParteTrabajo> partesTrabajo,
	    List<Perjudicado> perjudicados, List<ReaperturaExpediente> reaperturasExpediente) {
	super();
	this.id = id;
	this.fechaInicial = fechaInicial;
	this.fechaCreacion = fechaCreacion;
	this.numeroPoliza = numeroPoliza;
	this.franquicia = franquicia;
	this.urgente = urgente;
	this.tratamientoTiempoReal = tratamientoTiempoReal;
	this.numeroExpediente = numeroExpediente;
	this.descripcionServicio = descripcionServicio;
	this.danosProducidos = danosProducidos;
	this.rutaCarpeta = rutaCarpeta;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.perito = perito;
	this.asegurado = asegurado;
	this.compania = compania;
	this.estado = estado;
	this.partesTrabajo = partesTrabajo;
	this.perjudicados = perjudicados;
	this.reaperturasExpediente = reaperturasExpediente;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getFechaInicial() {
	return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
	this.fechaInicial = fechaInicial;
    }

    public Date getFechaCreacion() {
	return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
	this.fechaCreacion = fechaCreacion;
    }

    public String getNumeroPoliza() {
	return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
	this.numeroPoliza = numeroPoliza;
    }

    public Double getFranquicia() {
	return franquicia;
    }

    public void setFranquicia(Double franquicia) {
	this.franquicia = franquicia;
    }

    public String getUrgente() {
	return urgente;
    }

    public void setUrgente(String urgente) {
	this.urgente = urgente;
    }

    public String getTratamientoTiempoReal() {
	return tratamientoTiempoReal;
    }

    public void setTratamientoTiempoReal(String tratamientoTiempoReal) {
	this.tratamientoTiempoReal = tratamientoTiempoReal;
    }

    public String getNumeroExpediente() {
	return numeroExpediente;
    }

    public void setNumeroExpediente(String numeroExpediente) {
	this.numeroExpediente = numeroExpediente;
    }

    public String getDescripcionServicio() {
	return descripcionServicio;
    }

    public void setDescripcionServicio(String descripcionServicio) {
	this.descripcionServicio = descripcionServicio;
    }

    public String getDanosProducidos() {
	return danosProducidos;
    }

    public void setDanosProducidos(String danosProducidos) {
	this.danosProducidos = danosProducidos;
    }

    public String getRutaCarpeta() {
	return rutaCarpeta;
    }

    public void setRutaCarpeta(String rutaCarpeta) {
	this.rutaCarpeta = rutaCarpeta;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Perito getPerito() {
	return perito;
    }

    public void setPerito(Perito perito) {
	this.perito = perito;
    }

    public Asegurado getAsegurado() {
	return asegurado;
    }

    public void setAsegurado(Asegurado asegurado) {
	this.asegurado = asegurado;
    }

    public Compania getCompania() {
	return compania;
    }

    public void setCompania(Compania compania) {
	this.compania = compania;
    }

    public Estado getEstado() {
	return estado;
    }

    public void setEstado(Estado estado) {
	this.estado = estado;
    }

    public List<ReaperturaExpediente> getReaperturasExpediente() {
	return reaperturasExpediente;
    }

    public void setReaperturasExpediente(List<ReaperturaExpediente> reaperturasExpediente) {
	this.reaperturasExpediente = reaperturasExpediente;
    }

    public List<ParteTrabajo> getPartesTrabajo() {
	return partesTrabajo;
    }

    public void setPartesTrabajo(List<ParteTrabajo> partesTrabajo) {
	this.partesTrabajo = partesTrabajo;
    }

    public List<Perjudicado> getPerjudicados() {
	return perjudicados;
    }

    public void setPerjudicados(List<Perjudicado> perjudicados) {
	this.perjudicados = perjudicados;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((asegurado == null) ? 0 : asegurado.hashCode());
	result = prime * result + ((compania == null) ? 0 : compania.hashCode());
	result = prime * result + ((danosProducidos == null) ? 0 : danosProducidos.hashCode());
	result = prime * result + ((descripcionServicio == null) ? 0 : descripcionServicio.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
	result = prime * result + ((fechaInicial == null) ? 0 : fechaInicial.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((franquicia == null) ? 0 : franquicia.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((numeroExpediente == null) ? 0 : numeroExpediente.hashCode());
	result = prime * result + ((numeroPoliza == null) ? 0 : numeroPoliza.hashCode());
	result = prime * result + ((perito == null) ? 0 : perito.hashCode());
	result = prime * result + ((rutaCarpeta == null) ? 0 : rutaCarpeta.hashCode());
	result = prime * result + ((tratamientoTiempoReal == null) ? 0 : tratamientoTiempoReal.hashCode());
	result = prime * result + ((urgente == null) ? 0 : urgente.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Expediente other = (Expediente) obj;
	if (asegurado == null) {
	    if (other.asegurado != null)
		return false;
	} else if (!asegurado.equals(other.asegurado))
	    return false;
	if (compania == null) {
	    if (other.compania != null)
		return false;
	} else if (!compania.equals(other.compania))
	    return false;
	if (danosProducidos == null) {
	    if (other.danosProducidos != null)
		return false;
	} else if (!danosProducidos.equals(other.danosProducidos))
	    return false;
	if (descripcionServicio == null) {
	    if (other.descripcionServicio != null)
		return false;
	} else if (!descripcionServicio.equals(other.descripcionServicio))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaCreacion == null) {
	    if (other.fechaCreacion != null)
		return false;
	} else if (!fechaCreacion.equals(other.fechaCreacion))
	    return false;
	if (fechaInicial == null) {
	    if (other.fechaInicial != null)
		return false;
	} else if (!fechaInicial.equals(other.fechaInicial))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (franquicia == null) {
	    if (other.franquicia != null)
		return false;
	} else if (!franquicia.equals(other.franquicia))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (numeroExpediente == null) {
	    if (other.numeroExpediente != null)
		return false;
	} else if (!numeroExpediente.equals(other.numeroExpediente))
	    return false;
	if (numeroPoliza == null) {
	    if (other.numeroPoliza != null)
		return false;
	} else if (!numeroPoliza.equals(other.numeroPoliza))
	    return false;
	if (perito == null) {
	    if (other.perito != null)
		return false;
	} else if (!perito.equals(other.perito))
	    return false;
	if (rutaCarpeta == null) {
	    if (other.rutaCarpeta != null)
		return false;
	} else if (!rutaCarpeta.equals(other.rutaCarpeta))
	    return false;
	if (tratamientoTiempoReal == null) {
	    if (other.tratamientoTiempoReal != null)
		return false;
	} else if (!tratamientoTiempoReal.equals(other.tratamientoTiempoReal))
	    return false;
	if (urgente == null) {
	    if (other.urgente != null)
		return false;
	} else if (!urgente.equals(other.urgente))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Expediente [id=" + id + ", fechaInicial=" + fechaInicial + ", fechaCreacion=" + fechaCreacion + ", numeroPoliza=" + numeroPoliza + ", franquicia=" + franquicia
		+ ", urgente=" + urgente + ", tratamientoTiempoReal=" + tratamientoTiempoReal + ", numeroExpediente=" + numeroExpediente + ", descripcionServicio="
		+ descripcionServicio + ", danosProducidos=" + danosProducidos + ", rutaCarpeta=" + rutaCarpeta + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta
		+ ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja
		+ ", perito=" + perito + ", asegurado=" + asegurado + ", compania=" + compania + "]";
    }

}
