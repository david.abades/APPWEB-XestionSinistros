package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@NamedQuery(name = "Perjudicado.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Perjudicado(p.id, p.nombre) FROM Perjudicado p WHERE p.fechaBaja IS NULL ORDER BY p.nombre")
@NamedEntityGraph(name = "perjudicado", includeAllAttributes = true)
public class Perjudicado implements Serializable {
    @Transient
    private static final long serialVersionUID = 3404770546049727336L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "nombre", length = 400, nullable = false, unique = true)
    private String nombre;

    @Column(name = "telefono_fijo", length = 500, nullable = true, unique = false)
    private String telefonoFijo;

    @Column(name = "telefono_mobil", length = 500, nullable = true, unique = false)
    private String telefonoMobil;

    @Column(name = "email", length = 500, nullable = true, unique = false)
    private String email;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne
    @JoinColumn(name = "id_expediente", nullable = false)
    @JsonBackReference(value = "expedientePerjudicado")
    private Expediente expediente;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_parte_trabajo")
    @JsonBackReference(value = "parteTrabajoPerjudicado")
    private ParteTrabajo parteTrabajo;

    public Perjudicado() {
	super();
    }

    public Perjudicado(Integer id, String nombre) {
	super();
	this.id = id;
	this.nombre = nombre;
    }

    public Perjudicado(Integer id, String nombre, String telefonoFijo, String telefonoMobil, String email, Date fechaAlta, String usuarioAlta, Date fechaModificacion,
	    String usuarioModificacion, Date fechaBaja, String usuarioBaja, Expediente expediente) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.telefonoFijo = telefonoFijo;
	this.telefonoMobil = telefonoMobil;
	this.email = email;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
    }

    public Perjudicado(Integer id, String nombre, String telefonoFijo, String telefonoMobil, String email, Date fechaAlta, String usuarioAlta, Date fechaModificacion,
	    String usuarioModificacion, Date fechaBaja, String usuarioBaja, Expediente expediente, ParteTrabajo parteTrabajo) {
	super();
	this.id = id;
	this.nombre = nombre;
	this.telefonoFijo = telefonoFijo;
	this.telefonoMobil = telefonoMobil;
	this.email = email;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.expediente = expediente;
	this.parteTrabajo = parteTrabajo;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getNombre() {
	return nombre;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public String getTelefonoFijo() {
	return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
	this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMobil() {
	return telefonoMobil;
    }

    public void setTelefonoMobil(String telefonoMobil) {
	this.telefonoMobil = telefonoMobil;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Expediente getExpediente() {
	return expediente;
    }

    public void setExpediente(Expediente expediente) {
	this.expediente = expediente;
    }

    public ParteTrabajo getParteTrabajo() {
	return parteTrabajo;
    }

    public void setParteTrabajo(ParteTrabajo parteTrabajo) {
	this.parteTrabajo = parteTrabajo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((email == null) ? 0 : email.hashCode());
	result = prime * result + ((expediente == null) ? 0 : expediente.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
	result = prime * result + ((telefonoFijo == null) ? 0 : telefonoFijo.hashCode());
	result = prime * result + ((telefonoMobil == null) ? 0 : telefonoMobil.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Perjudicado other = (Perjudicado) obj;
	if (email == null) {
	    if (other.email != null)
		return false;
	} else if (!email.equals(other.email))
	    return false;
	if (expediente == null) {
	    if (other.expediente != null)
		return false;
	} else if (!expediente.equals(other.expediente))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nombre == null) {
	    if (other.nombre != null)
		return false;
	} else if (!nombre.equals(other.nombre))
	    return false;
	if (telefonoFijo == null) {
	    if (other.telefonoFijo != null)
		return false;
	} else if (!telefonoFijo.equals(other.telefonoFijo))
	    return false;
	if (telefonoMobil == null) {
	    if (other.telefonoMobil != null)
		return false;
	} else if (!telefonoMobil.equals(other.telefonoMobil))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Perjudicado [id=" + id + ", nombre=" + nombre + ", telefonoFijo=" + telefonoFijo + ", telefonoMobil=" + telefonoMobil + ", email=" + email + ", fechaAlta="
		+ fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja="
		+ fechaBaja + ", usuarioBaja=" + usuarioBaja + ", expediente=" + expediente + "]";
    }
}
