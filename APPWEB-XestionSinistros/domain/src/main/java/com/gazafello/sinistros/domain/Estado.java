package com.gazafello.sinistros.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "Estado.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.Estado(e.id, e.descripcion) FROM Estado e ORDER BY e.descripcion")
@NamedEntityGraph(name = "estado", includeAllAttributes = true)
public class Estado implements Serializable {

    private static final long serialVersionUID = -259304751745481190L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "descripcion", length = 100, nullable = false, unique = true)
    private String descripcion;

    public Estado() {
	super();
    }

    public Estado(Integer id, String descripcion) {
	super();
	this.id = id;
	this.descripcion = descripcion;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Estado other = (Estado) obj;
	if (descripcion == null) {
	    if (other.descripcion != null)
		return false;
	} else if (!descripcion.equals(other.descripcion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Estado [id=" + id + ", descripcion=" + descripcion + "]";
    }

}
