package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@NamedQuery(name = "ConceptoFacturable.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.ConceptoFacturable(c.id, c.codigo, c.descripcion, c.compania) FROM ConceptoFacturable c WHERE c.fechaBaja IS NULL ORDER BY c.codigo")
@NamedEntityGraph(name = "conceptoFacturable", includeAllAttributes = true)
@Table(name = "concepto_facturable")
public class ConceptoFacturable implements Serializable {
    @Transient
    private static final long serialVersionUID = 3416605520808008000L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "baremo_compania", length = 1, nullable = false, unique = true)
    private String baremoCompania;

    @Column(name = "codigo", length = 200, nullable = true, unique = false)
    private String codigo;

    @Column(name = "descripcion", length = 2000, nullable = true, unique = false)
    private String descripcion;

    @Column(name = "unidad", length = 10, nullable = true, unique = false)
    private String unidad;

    @Column(name = "precio_coste", length = 6, nullable = true, unique = false)
    private Double precioCoste;

    @Column(name = "importe", length = 6, nullable = true, unique = false)
    private Double importe;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_compania")
    private Compania compania;

    public ConceptoFacturable() {
	super();
    }

    public ConceptoFacturable(Integer id, String codigo, String descripcion, Compania compania) {
	super();
	this.id = id;
	this.codigo = codigo;
	this.descripcion = descripcion;
	this.compania = compania;
    }

    public ConceptoFacturable(Integer id, String baremoCompania, String codigo, String descripcion, String unidad, Double precioCoste, Double importe, Date fechaAlta,
	    String usuarioAlta, Date fechaModificacion, String usuarioModificacion, Date fechaBaja, String usuarioBaja, Compania compania) {
	super();
	this.id = id;
	this.baremoCompania = baremoCompania;
	this.codigo = codigo;
	this.descripcion = descripcion;
	this.unidad = unidad;
	this.precioCoste = precioCoste;
	this.importe = importe;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.compania = compania;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getBaremoCompania() {
	return baremoCompania;
    }

    public void setBaremoCompania(String baremoCompania) {
	this.baremoCompania = baremoCompania;
    }

    public String getCodigo() {
	return codigo;
    }

    public void setCodigo(String codigo) {
	this.codigo = codigo;
    }

    public String getDescripcion() {
	return descripcion;
    }

    public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
    }

    public String getUnidad() {
	return unidad;
    }

    public void setUnidad(String unidad) {
	this.unidad = unidad;
    }

    public Double getPrecioCoste() {
	return precioCoste;
    }

    public void setPrecioCoste(Double precioCoste) {
	this.precioCoste = precioCoste;
    }

    public Double getImporte() {
	return importe;
    }

    public void setImporte(Double importe) {
	this.importe = importe;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public Compania getCompania() {
	return compania;
    }

    public void setCompania(Compania compania) {
	this.compania = compania;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((baremoCompania == null) ? 0 : baremoCompania.hashCode());
	result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
	result = prime * result + ((compania == null) ? 0 : compania.hashCode());
	result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((importe == null) ? 0 : importe.hashCode());
	result = prime * result + ((precioCoste == null) ? 0 : precioCoste.hashCode());
	result = prime * result + ((unidad == null) ? 0 : unidad.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ConceptoFacturable other = (ConceptoFacturable) obj;
	if (baremoCompania == null) {
	    if (other.baremoCompania != null)
		return false;
	} else if (!baremoCompania.equals(other.baremoCompania))
	    return false;
	if (codigo == null) {
	    if (other.codigo != null)
		return false;
	} else if (!codigo.equals(other.codigo))
	    return false;
	if (compania == null) {
	    if (other.compania != null)
		return false;
	} else if (!compania.equals(other.compania))
	    return false;
	if (descripcion == null) {
	    if (other.descripcion != null)
		return false;
	} else if (!descripcion.equals(other.descripcion))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (importe == null) {
	    if (other.importe != null)
		return false;
	} else if (!importe.equals(other.importe))
	    return false;
	if (precioCoste == null) {
	    if (other.precioCoste != null)
		return false;
	} else if (!precioCoste.equals(other.precioCoste))
	    return false;
	if (unidad == null) {
	    if (other.unidad != null)
		return false;
	} else if (!unidad.equals(other.unidad))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ConceptoFacturable [id=" + id + ", baremoCompania=" + baremoCompania + ", codigo=" + codigo + ", descripcion=" + descripcion + ", unidad=" + unidad
		+ ", precioCoste=" + precioCoste + ", importe=" + importe + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion
		+ ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja + ", compania=" + compania + "]";
    }

}
