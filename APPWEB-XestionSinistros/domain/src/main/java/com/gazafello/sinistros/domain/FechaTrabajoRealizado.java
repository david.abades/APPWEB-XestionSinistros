package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "FechaTrabajoRealizado.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.FechaTrabajoRealizado(ftr.id) FROM FechaTrabajoRealizado ftr")
@NamedEntityGraph(name = "fechaTrabajoRealizado", includeAllAttributes = true)
@Table(name = "fecha_trabajo_realizado")
public class FechaTrabajoRealizado implements Serializable {

    private static final long serialVersionUID = 1719369962052037607L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "fecha", nullable = false, unique = false)
    private Date fecha;

    @Column(name = "hora_ini_riesgo")
    private Date horaInicioRiesgo;

    @Column(name = "hora_fin_riesgo")
    private Date horaFinRiesgo;

    @Column(name = "hora_ini_taller")
    private Date horaInicioTaller;

    @Column(name = "hora_fin_taller")
    private Date horaFinTaller;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_trabajo_realizado", nullable = false)
    private TrabajoRealizado trabajoRealizado;

    public FechaTrabajoRealizado() {
	super();
    }

    public FechaTrabajoRealizado(Integer id) {
	super();
	this.id = id;
    }

    public FechaTrabajoRealizado(Integer id, Date fecha, Date horaInicioRiesgo, Date horaFinRiesgo, Date horaInicioTaller, Date horaFinTaller) {
	super();
	this.id = id;
	this.fecha = fecha;
	this.horaInicioRiesgo = horaInicioRiesgo;
	this.horaFinRiesgo = horaFinRiesgo;
	this.horaInicioTaller = horaInicioTaller;
	this.horaFinTaller = horaFinTaller;
    }

    public FechaTrabajoRealizado(Integer id, Date fecha, Date horaInicioRiesgo, Date horaFinRiesgo, Date horaInicioTaller, Date horaFinTaller, TrabajoRealizado trabajoRealizado) {
	super();
	this.id = id;
	this.fecha = fecha;
	this.horaInicioRiesgo = horaInicioRiesgo;
	this.horaFinRiesgo = horaFinRiesgo;
	this.horaInicioTaller = horaInicioTaller;
	this.horaFinTaller = horaFinTaller;
	this.trabajoRealizado = trabajoRealizado;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getFecha() {
	return fecha;
    }

    public void setFecha(Date fecha) {
	this.fecha = fecha;
    }

    public Date getHoraInicioRiesgo() {
	return horaInicioRiesgo;
    }

    public void setHoraInicioRiesgo(Date horaInicioRiesgo) {
	this.horaInicioRiesgo = horaInicioRiesgo;
    }

    public Date getHoraFinRiesgo() {
	return horaFinRiesgo;
    }

    public void setHoraFinRiesgo(Date horaFinRiesgo) {
	this.horaFinRiesgo = horaFinRiesgo;
    }

    public Date getHoraInicioTaller() {
	return horaInicioTaller;
    }

    public void setHoraInicioTaller(Date horaInicioTaller) {
	this.horaInicioTaller = horaInicioTaller;
    }

    public Date getHoraFinTaller() {
	return horaFinTaller;
    }

    public void setHoraFinTaller(Date horaFinTaller) {
	this.horaFinTaller = horaFinTaller;
    }

    public TrabajoRealizado getTrabajoRealizado() {
	return trabajoRealizado;
    }

    public void setTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
	this.trabajoRealizado = trabajoRealizado;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
	result = prime * result + ((horaFinRiesgo == null) ? 0 : horaFinRiesgo.hashCode());
	result = prime * result + ((horaFinTaller == null) ? 0 : horaFinTaller.hashCode());
	result = prime * result + ((horaInicioRiesgo == null) ? 0 : horaInicioRiesgo.hashCode());
	result = prime * result + ((horaInicioTaller == null) ? 0 : horaInicioTaller.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	FechaTrabajoRealizado other = (FechaTrabajoRealizado) obj;
	if (fecha == null) {
	    if (other.fecha != null)
		return false;
	} else if (!fecha.equals(other.fecha))
	    return false;
	if (horaFinRiesgo == null) {
	    if (other.horaFinRiesgo != null)
		return false;
	} else if (!horaFinRiesgo.equals(other.horaFinRiesgo))
	    return false;
	if (horaFinTaller == null) {
	    if (other.horaFinTaller != null)
		return false;
	} else if (!horaFinTaller.equals(other.horaFinTaller))
	    return false;
	if (horaInicioRiesgo == null) {
	    if (other.horaInicioRiesgo != null)
		return false;
	} else if (!horaInicioRiesgo.equals(other.horaInicioRiesgo))
	    return false;
	if (horaInicioTaller == null) {
	    if (other.horaInicioTaller != null)
		return false;
	} else if (!horaInicioTaller.equals(other.horaInicioTaller))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "FechaTrabajoRealizado [id=" + id + ", fecha=" + fecha + ", horaInicioRiesgo=" + horaInicioRiesgo + ", horaFinRiesgo=" + horaFinRiesgo + ", horaInicioTaller="
		+ horaInicioTaller + ", horaFinTaller=" + horaFinTaller + "]";
    }

}
