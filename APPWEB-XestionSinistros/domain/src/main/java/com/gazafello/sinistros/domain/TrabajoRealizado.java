package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@NamedQuery(name = "TrabajoRealizado.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.TrabajoRealizado(tr.id, tr.parteTrabajo) FROM TrabajoRealizado tr ORDER BY tr.fechaAlta")
@NamedEntityGraph(name = "trabajoRealizado", includeAllAttributes = true)
@Table(name = "trabajo_realizado")
public class TrabajoRealizado implements Serializable {

    private static final long serialVersionUID = -8925666216890688458L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "observaciones", nullable = true, unique = false)
    private String observaciones;

    @Column(name = "materiales", nullable = true, unique = false)
    private String materiales;

    @Column(name = "fecha_recepcion_materiales", nullable = true, unique = false)
    private Date fechaRecepcionMateriales;

    @Column(name = "peticiones", nullable = true, unique = false)
    private String peticiones;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_parte_trabajo")
    @JsonBackReference(value = "parteTrabajoTrabajosRealizados")
    private ParteTrabajo parteTrabajo;

    // @OneToMany(mappedBy = "trabajoRealizado", fetch = FetchType.EAGER)
    // @Fetch(FetchMode.SELECT)
    // private List<FechaTrabajoRealizado> fechasTrabajoRealizado;

    // @OneToMany(mappedBy = "trabajoRealizado", fetch = FetchType.EAGER)
    // private List<TrabajoPendiente> trabajosPendientes;

    public TrabajoRealizado() {
	super();
	// this.fechasTrabajoRealizado = new ArrayList<FechaTrabajoRealizado>();
	// this.trabajosPendientes = new ArrayList<TrabajoPendiente>();
    }

    public TrabajoRealizado(Integer id, ParteTrabajo parteTrabajo) {
	super();
	this.id = id;
	this.parteTrabajo = parteTrabajo;
    }

    public TrabajoRealizado(Integer id, String observaciones, String materiales, Date fechaRecepcionMateriales, String peticiones, Date fechaAlta, String usuarioAlta,
	    Date fechaModificacion, String usuarioModificacion, Date fechaBaja, String usuarioBaja, ParteTrabajo parteTrabajo) {
	super();
	this.id = id;
	this.observaciones = observaciones;
	this.materiales = materiales;
	this.fechaRecepcionMateriales = fechaRecepcionMateriales;
	this.peticiones = peticiones;
	this.fechaAlta = fechaAlta;
	this.usuarioAlta = usuarioAlta;
	this.fechaModificacion = fechaModificacion;
	this.usuarioModificacion = usuarioModificacion;
	this.fechaBaja = fechaBaja;
	this.usuarioBaja = usuarioBaja;
	this.parteTrabajo = parteTrabajo;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getObservaciones() {
	return observaciones;
    }

    public void setObservaciones(String observaciones) {
	this.observaciones = observaciones;
    }

    public String getMateriales() {
	return materiales;
    }

    public void setMateriales(String materiales) {
	this.materiales = materiales;
    }

    public Date getFechaRecepcionMateriales() {
	return fechaRecepcionMateriales;
    }

    public void setFechaRecepcionMateriales(Date fechaRecepcionMateriales) {
	this.fechaRecepcionMateriales = fechaRecepcionMateriales;
    }

    public String getPeticiones() {
	return peticiones;
    }

    public void setPeticiones(String peticiones) {
	this.peticiones = peticiones;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public ParteTrabajo getParteTrabajo() {
	return parteTrabajo;
    }

    public void setParteTrabajo(ParteTrabajo parteTrabajo) {
	this.parteTrabajo = parteTrabajo;
    }

    // public List<FechaTrabajoRealizado> getFechasTrabajoRealizado() {
    // return fechasTrabajoRealizado;
    // }
    //
    // public void setFechasTrabajoRealizado(List<FechaTrabajoRealizado>
    // fechasTrabajoRealizado) {
    // this.fechasTrabajoRealizado = fechasTrabajoRealizado;
    // }

    // public List<TrabajoPendiente> getTrabajosPendientes() {
    // return trabajosPendientes;
    // }
    //
    // public void setTrabajosPendientes(List<TrabajoPendiente>
    // trabajosPendientes) {
    // this.trabajosPendientes = trabajosPendientes;
    // }

    // public void addFechaTrabajoRealizado(FechaTrabajoRealizado
    // fechaTrabajoRealizado) {
    // this.fechasTrabajoRealizado.add(fechaTrabajoRealizado);
    // }

    // public void addTrabajosPendientes(TrabajoPendiente trabajoPendiente) {
    // this.trabajosPendientes.add(trabajoPendiente);
    // }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((fechaRecepcionMateriales == null) ? 0 : fechaRecepcionMateriales.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((materiales == null) ? 0 : materiales.hashCode());
	result = prime * result + ((observaciones == null) ? 0 : observaciones.hashCode());
	result = prime * result + ((parteTrabajo == null) ? 0 : parteTrabajo.hashCode());
	result = prime * result + ((peticiones == null) ? 0 : peticiones.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TrabajoRealizado other = (TrabajoRealizado) obj;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (fechaRecepcionMateriales == null) {
	    if (other.fechaRecepcionMateriales != null)
		return false;
	} else if (!fechaRecepcionMateriales.equals(other.fechaRecepcionMateriales))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (materiales == null) {
	    if (other.materiales != null)
		return false;
	} else if (!materiales.equals(other.materiales))
	    return false;
	if (observaciones == null) {
	    if (other.observaciones != null)
		return false;
	} else if (!observaciones.equals(other.observaciones))
	    return false;
	if (parteTrabajo == null) {
	    if (other.parteTrabajo != null)
		return false;
	} else if (!parteTrabajo.equals(other.parteTrabajo))
	    return false;
	if (peticiones == null) {
	    if (other.peticiones != null)
		return false;
	} else if (!peticiones.equals(other.peticiones))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "TrabajoRealizado [id=" + id + ", observaciones=" + observaciones + ", materiales=" + materiales + ", fechaRecepcionMateriales=" + fechaRecepcionMateriales
		+ ", peticiones=" + peticiones + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion + ", usuarioModificacion="
		+ usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja + ", parteTrabajo=" + parteTrabajo + "]";
    }

}
