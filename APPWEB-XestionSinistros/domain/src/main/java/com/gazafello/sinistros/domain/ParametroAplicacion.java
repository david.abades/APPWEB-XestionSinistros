package com.gazafello.sinistros.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@NamedQuery(name = "ParametroAplicacion.findAll", query = "SELECT DISTINCT NEW com.gazafello.sinistros.domain.ParametroAplicacion(p.id, p.nombreTramitadora) FROM ParametroAplicacion p WHERE p.fechaBaja IS NULL ORDER BY p.nombreTramitadora")
@NamedEntityGraph(name = "parametroAplicacion", includeAllAttributes = true)
@Table(name = "parametros_aplicacion")
public class ParametroAplicacion implements Serializable {
    @Transient
    private static final long serialVersionUID = -8875821061301950028L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "dias_desde_expediente", length = 3, nullable = false, unique = false)
    private String diasDesdeExpediente;

    @Column(name = "dias_desde_parte", length = 3, nullable = false, unique = false)
    private String diasDesdeParte;

    @Column(name = "copias_parte", length = 2, nullable = false, unique = false)
    private String copiasParte;

    @Column(name = "nombre_tramitadora", length = 200, nullable = false, unique = false)
    private String nombreTramitadora;

    @Column(name = "fecha_alta", nullable = false, unique = false)
    private Date fechaAlta;

    @Column(name = "usuario_alta", length = 100, nullable = false, unique = false)
    private String usuarioAlta;

    @Column(name = "fecha_modificacion", nullable = true, unique = false)
    private Date fechaModificacion;

    @Column(name = "usuario_modificacion", length = 100, nullable = true, unique = false)
    private String usuarioModificacion;

    @Column(name = "fecha_baja", nullable = true, unique = true)
    private Date fechaBaja;

    @Column(name = "usuario_baja", length = 100, nullable = true, unique = false)
    private String usuarioBaja;

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getDiasDesdeExpediente() {
	return diasDesdeExpediente;
    }

    public void setDiasDesdeExpediente(String diasDesdeExpediente) {
	this.diasDesdeExpediente = diasDesdeExpediente;
    }

    public String getDiasDesdeParte() {
	return diasDesdeParte;
    }

    public void setDiasDesdeParte(String diasDesdeParte) {
	this.diasDesdeParte = diasDesdeParte;
    }

    public String getCopiasParte() {
	return copiasParte;
    }

    public void setCopiasParte(String copiasParte) {
	this.copiasParte = copiasParte;
    }

    public String getNombreTramitadora() {
	return nombreTramitadora;
    }

    public void setNombreTramitadora(String nombreTramitadora) {
	this.nombreTramitadora = nombreTramitadora;
    }

    public Date getFechaAlta() {
	return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
	this.fechaAlta = fechaAlta;
    }

    public String getUsuarioAlta() {
	return usuarioAlta;
    }

    public void setUsuarioAlta(String usuarioAlta) {
	this.usuarioAlta = usuarioAlta;
    }

    public Date getFechaModificacion() {
	return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
	this.fechaModificacion = fechaModificacion;
    }

    public String getUsuarioModificacion() {
	return usuarioModificacion;
    }

    public void setUsuarioModificacion(String usuarioModificacion) {
	this.usuarioModificacion = usuarioModificacion;
    }

    public Date getFechaBaja() {
	return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
	this.fechaBaja = fechaBaja;
    }

    public String getUsuarioBaja() {
	return usuarioBaja;
    }

    public void setUsuarioBaja(String usuarioBaja) {
	this.usuarioBaja = usuarioBaja;
    }

    public ParametroAplicacion() {
	super();
    }

    public ParametroAplicacion(Integer id, String nombreTramitadora) {
	super();
	this.id = id;
	this.nombreTramitadora = nombreTramitadora;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((copiasParte == null) ? 0 : copiasParte.hashCode());
	result = prime * result + ((diasDesdeExpediente == null) ? 0 : diasDesdeExpediente.hashCode());
	result = prime * result + ((diasDesdeParte == null) ? 0 : diasDesdeParte.hashCode());
	result = prime * result + ((fechaAlta == null) ? 0 : fechaAlta.hashCode());
	result = prime * result + ((fechaBaja == null) ? 0 : fechaBaja.hashCode());
	result = prime * result + ((fechaModificacion == null) ? 0 : fechaModificacion.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((nombreTramitadora == null) ? 0 : nombreTramitadora.hashCode());
	result = prime * result + ((usuarioAlta == null) ? 0 : usuarioAlta.hashCode());
	result = prime * result + ((usuarioBaja == null) ? 0 : usuarioBaja.hashCode());
	result = prime * result + ((usuarioModificacion == null) ? 0 : usuarioModificacion.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ParametroAplicacion other = (ParametroAplicacion) obj;
	if (copiasParte == null) {
	    if (other.copiasParte != null)
		return false;
	} else if (!copiasParte.equals(other.copiasParte))
	    return false;
	if (diasDesdeExpediente == null) {
	    if (other.diasDesdeExpediente != null)
		return false;
	} else if (!diasDesdeExpediente.equals(other.diasDesdeExpediente))
	    return false;
	if (diasDesdeParte == null) {
	    if (other.diasDesdeParte != null)
		return false;
	} else if (!diasDesdeParte.equals(other.diasDesdeParte))
	    return false;
	if (fechaAlta == null) {
	    if (other.fechaAlta != null)
		return false;
	} else if (!fechaAlta.equals(other.fechaAlta))
	    return false;
	if (fechaBaja == null) {
	    if (other.fechaBaja != null)
		return false;
	} else if (!fechaBaja.equals(other.fechaBaja))
	    return false;
	if (fechaModificacion == null) {
	    if (other.fechaModificacion != null)
		return false;
	} else if (!fechaModificacion.equals(other.fechaModificacion))
	    return false;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (nombreTramitadora == null) {
	    if (other.nombreTramitadora != null)
		return false;
	} else if (!nombreTramitadora.equals(other.nombreTramitadora))
	    return false;
	if (usuarioAlta == null) {
	    if (other.usuarioAlta != null)
		return false;
	} else if (!usuarioAlta.equals(other.usuarioAlta))
	    return false;
	if (usuarioBaja == null) {
	    if (other.usuarioBaja != null)
		return false;
	} else if (!usuarioBaja.equals(other.usuarioBaja))
	    return false;
	if (usuarioModificacion == null) {
	    if (other.usuarioModificacion != null)
		return false;
	} else if (!usuarioModificacion.equals(other.usuarioModificacion))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ParametroAplicacion [id=" + id + ", diasDesdeExpediente=" + diasDesdeExpediente + ", diasDesdeParte=" + diasDesdeParte + ", copiasParte=" + copiasParte
		+ ", nombreTramitadora=" + nombreTramitadora + ", fechaAlta=" + fechaAlta + ", usuarioAlta=" + usuarioAlta + ", fechaModificacion=" + fechaModificacion
		+ ", usuarioModificacion=" + usuarioModificacion + ", fechaBaja=" + fechaBaja + ", usuarioBaja=" + usuarioBaja + "]";
    }
}
