package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.ParteTrabajo;

public interface ParteTrabajoServices {
    /**
     * Devolve lista de parteTrabajos
     * 
     * @return
     */
    public List<ParteTrabajo> getAllParteTrabajos();

    /**
     * Devolve parteTrabajo por id
     * 
     * @param parteTrabajoId
     * @return
     */
    public ParteTrabajo getParteTrabajo(Integer parteTrabajoId);

    /**
     * Crea un rexistro de parteTrabajo
     * 
     * @param newParteTrabajo
     * @return
     */
    public ParteTrabajo saveParteTrabajo(ParteTrabajo newParteTrabajo);

    /**
     * Elimina un parteTrabajo
     * 
     * @param parteTrabajo
     * @return
     */
    public void removeParteTrabajo(ParteTrabajo parteTrabajo);
}
