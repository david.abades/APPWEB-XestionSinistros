package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.TrabajoPendiente;

public interface TrabajoPendienteServices {
    /**
     * Devolve lista de trabajoPendientes
     * 
     * @return
     */
    public List<TrabajoPendiente> getAllTrabajoPendientes();

    /**
     * Devolve trabajoPendiente por id
     * 
     * @param trabajoPendienteId
     * @return
     */
    public TrabajoPendiente getTrabajoPendiente(Integer trabajoPendienteId);

    /**
     * Crea un rexistro de trabajoPendiente
     * 
     * @param newTrabajoPendiente
     * @return
     */
    public TrabajoPendiente saveTrabajoPendiente(TrabajoPendiente newTrabajoPendiente);

    /**
     * Elimina un trabajoPendiente
     * 
     * @param trabajoPendiente
     * @return
     */
    public void removeTrabajoPendiente(TrabajoPendiente trabajoPendiente);
}
