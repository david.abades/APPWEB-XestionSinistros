package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Estado;
import com.gazafello.sinistros.persistence.EstadoDao;

@Service("estadoServices")
@Profile("dev")
public class EstadoServicesImpl implements EstadoServices {
    private final Logger logger = LoggerFactory.getLogger(EstadoServicesImpl.class);
    private EstadoDao estadoDao;

    @Autowired
    public void setEstadoDao(EstadoDao estadoDao) {
	this.estadoDao = estadoDao;
    }

    @Override
    public List<Estado> getAllEstados() {
	logger.debug("Selecting all estados stored...");

	return estadoDao.getAllEstados();
    }

    @Override
    public Estado getEstado(Integer estadoId) {
	if (estadoId == null) {
	    return null;
	}
	logger.debug("Selecting estado with id " + estadoId + " ...");

	return estadoDao.getEstado(estadoId);
    }

    @Override
    public Estado saveEstado(Estado newEstado) {
	if (newEstado == null) {
	    return null;
	}
	logger.debug("Saving estado..." + newEstado);

	return estadoDao.saveEstado(newEstado);
    }

    @Override
    public void removeEstado(Estado estado) {
	logger.debug("Deleting estado with id " + estado.getId() + " ...");

	estadoDao.removeEstado(estado);
    }
}
