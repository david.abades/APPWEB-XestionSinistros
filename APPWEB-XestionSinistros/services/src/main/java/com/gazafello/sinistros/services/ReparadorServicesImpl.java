package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Reparador;
import com.gazafello.sinistros.persistence.ReparadorDao;

@Service("reparadorServices")
@Profile("dev")
public class ReparadorServicesImpl implements ReparadorServices {
    private final Logger logger = LoggerFactory.getLogger(ReparadorServicesImpl.class);
    private ReparadorDao reparadorDao;

    @Autowired
    public void setReparadorDao(ReparadorDao reparadorDao) {
	this.reparadorDao = reparadorDao;
    }

    @Override
    public List<Reparador> getAllReparadors() {
	logger.debug("Selecting all reparadors stored...");

	return reparadorDao.getAllReparadors();
    }

    @Override
    public Reparador getReparador(Integer reparadorId) {
	if (reparadorId == null) {
	    return null;
	}
	logger.debug("Selecting reparador with id " + reparadorId + " ...");

	return reparadorDao.getReparador(reparadorId);
    }

    @Override
    public Reparador saveReparador(Reparador newReparador) {
	if (newReparador == null) {
	    return null;
	}
	logger.debug("Saving reparador..." + newReparador);

	return reparadorDao.saveReparador(newReparador);
    }

    @Override
    public void removeReparador(Reparador reparador) {
	logger.debug("Deleting reparador with id " + reparador.getId() + " ...");

	reparadorDao.removeReparador(reparador);
    }
}
