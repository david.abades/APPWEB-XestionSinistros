package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Compania;
import com.gazafello.sinistros.persistence.CompaniaDao;

@Service("companiaServices")
@Profile("dev")
public class CompaniaServicesImpl implements CompaniaServices {
    private final Logger logger = LoggerFactory.getLogger(CompaniaServicesImpl.class);
    private CompaniaDao companiaDao;

    @Autowired
    public void setCompaniaDao(CompaniaDao companiaDao) {
	this.companiaDao = companiaDao;
    }

    @Override
    public List<Compania> getAllCompanias() {
	logger.debug("Selecting all companias stored...");

	return companiaDao.getAllCompanias();
    }

    @Override
    public Compania getCompania(Integer companiaId) {
	if (companiaId == null) {
	    return null;
	}
	logger.debug("Selecting compania with id " + companiaId + " ...");

	return companiaDao.getCompania(companiaId);
    }

    @Override
    public Compania saveCompania(Compania newCompania) {
	if (newCompania == null) {
	    return null;
	}
	logger.debug("Saving compania..." + newCompania);

	return companiaDao.saveCompania(newCompania);
    }

    @Override
    public void removeCompania(Compania compania) {
	logger.debug("Deleting compania with id " + compania.getId() + " ...");

	companiaDao.removeCompania(compania);
    }
}
