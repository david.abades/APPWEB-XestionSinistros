package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Perjudicado;
import com.gazafello.sinistros.persistence.PerjudicadoDao;

@Service("perjudicadoServices")
@Profile("dev")
public class PerjudicadoServicesImpl implements PerjudicadoServices {
    private final Logger logger = LoggerFactory.getLogger(PerjudicadoServicesImpl.class);
    private PerjudicadoDao perjudicadoDao;

    @Autowired
    public void setPerjudicadoDao(PerjudicadoDao perjudicadoDao) {
	this.perjudicadoDao = perjudicadoDao;
    }

    @Override
    public List<Perjudicado> getAllPerjudicados() {
	logger.debug("Selecting all perjudicados stored...");

	return perjudicadoDao.getAllPerjudicados();
    }

    @Override
    public Perjudicado getPerjudicado(Integer perjudicadoId) {
	if (perjudicadoId == null) {
	    return null;
	}
	logger.debug("Selecting perjudicado with id " + perjudicadoId + " ...");

	return perjudicadoDao.getPerjudicado(perjudicadoId);
    }

    @Override
    public Perjudicado savePerjudicado(Perjudicado newPerjudicado) {
	if (newPerjudicado == null) {
	    return null;
	}
	logger.debug("Saving perjudicado..." + newPerjudicado);

	return perjudicadoDao.savePerjudicado(newPerjudicado);
    }

    @Override
    public void removePerjudicado(Perjudicado perjudicado) {
	logger.debug("Deleting perjudicado with id " + perjudicado.getId() + " ...");

	perjudicadoDao.removePerjudicado(perjudicado);
    }
}
