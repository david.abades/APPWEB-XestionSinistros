package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Perito;
import com.gazafello.sinistros.persistence.PeritoDao;

@Service("peritoServices")
@Profile("dev")
public class PeritoServicesImpl implements PeritoServices {
    private final Logger logger = LoggerFactory.getLogger(PeritoServicesImpl.class);
    private PeritoDao peritoDao;

    @Autowired
    public void setPeritoDao(PeritoDao peritoDao) {
	this.peritoDao = peritoDao;
    }

    @Override
    public List<Perito> getAllPeritos() {
	logger.debug("Selecting all peritos stored...");

	return peritoDao.getAllPeritos();
    }

    @Override
    public Perito getPerito(Integer peritoId) {
	if (peritoId == null) {
	    return null;
	}
	logger.debug("Selecting perito with id " + peritoId + " ...");

	return peritoDao.getPerito(peritoId);
    }

    @Override
    public Perito savePerito(Perito newPerito) {
	if (newPerito == null) {
	    return null;
	}
	logger.debug("Saving perito..." + newPerito);

	return peritoDao.savePerito(newPerito);
    }

    @Override
    public void removePerito(Perito perito) {
	logger.debug("Deleting perito with id " + perito.getId() + " ...");

	peritoDao.removePerito(perito);
    }
}
