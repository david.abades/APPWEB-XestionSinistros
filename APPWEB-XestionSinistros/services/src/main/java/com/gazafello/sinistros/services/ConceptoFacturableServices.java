package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.ConceptoFacturable;

public interface ConceptoFacturableServices {
    /**
     * Devolve lista de conceptoFacturables
     * 
     * @return
     */
    public List<ConceptoFacturable> getAllConceptoFacturables();

    /**
     * Devolve conceptoFacturable por id
     * 
     * @param conceptoFacturableId
     * @return
     */
    public ConceptoFacturable getConceptoFacturable(Integer conceptoFacturableId);

    /**
     * Crea un rexistro de conceptoFacturable
     * 
     * @param newConceptoFacturable
     * @return
     */
    public ConceptoFacturable saveConceptoFacturable(ConceptoFacturable newConceptoFacturable);

    /**
     * Elimina un conceptoFacturable
     * 
     * @param conceptoFacturable
     * @return
     */
    public void removeConceptoFacturable(ConceptoFacturable conceptoFacturable);
}
