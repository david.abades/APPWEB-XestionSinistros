package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.constants.Constants;
import com.gazafello.sinistros.domain.ConceptoFacturable;
import com.gazafello.sinistros.persistence.ConceptoFacturableDao;

@Service("conceptoFacturableServices")
@Profile("dev")
public class ConceptoFacturableServicesImpl implements ConceptoFacturableServices {
    private final Logger logger = LoggerFactory.getLogger(ConceptoFacturableServicesImpl.class);
    private ConceptoFacturableDao conceptoFacturableDao;

    @Autowired
    public void setConceptoFacturableDao(ConceptoFacturableDao conceptoFacturableDao) {
	this.conceptoFacturableDao = conceptoFacturableDao;
    }

    @Override
    public List<ConceptoFacturable> getAllConceptoFacturables() {
	logger.debug("Selecting all conceptoFacturables stored...");

	return conceptoFacturableDao.getAllConceptoFacturables();
    }

    @Override
    public ConceptoFacturable getConceptoFacturable(Integer conceptoFacturableId) {
	if (conceptoFacturableId == null) {
	    return null;
	}
	logger.debug("Selecting conceptoFacturable with id " + conceptoFacturableId + " ...");

	return conceptoFacturableDao.getConceptoFacturable(conceptoFacturableId);
    }

    @Override
    public ConceptoFacturable saveConceptoFacturable(ConceptoFacturable newConceptoFacturable) {
	if (newConceptoFacturable == null) {
	    return null;
	}
	logger.debug("Saving conceptoFacturable..." + newConceptoFacturable);

	if (newConceptoFacturable.getBaremoCompania().equals("") || newConceptoFacturable.getBaremoCompania() == null) {
	    newConceptoFacturable.setBaremoCompania(Constants.BAREMO_COMPANIA_NO);
	}

	return conceptoFacturableDao.saveConceptoFacturable(newConceptoFacturable);
    }

    @Override
    public void removeConceptoFacturable(ConceptoFacturable conceptoFacturable) {
	logger.debug("Deleting conceptoFacturable with id " + conceptoFacturable.getId() + " ...");

	conceptoFacturableDao.removeConceptoFacturable(conceptoFacturable);
    }
}
