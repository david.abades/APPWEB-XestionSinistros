package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.FechaTrabajoRealizado;

public interface FechaTrabajoRealizadoServices {
    /**
     * Devolve lista de fechaTrabajoRealizados
     * 
     * @return
     */
    public List<FechaTrabajoRealizado> getAllFechaTrabajoRealizados();

    /**
     * Devolve fechaTrabajoRealizado por id
     * 
     * @param fechaTrabajoRealizadoId
     * @return
     */
    public FechaTrabajoRealizado getFechaTrabajoRealizado(Integer fechaTrabajoRealizadoId);

    /**
     * Crea un rexistro de fechaTrabajoRealizado
     * 
     * @param newFechaTrabajoRealizado
     * @return
     */
    public FechaTrabajoRealizado saveFechaTrabajoRealizado(FechaTrabajoRealizado newFechaTrabajoRealizado);

    /**
     * Elimina un fechaTrabajoRealizado
     * 
     * @param fechaTrabajoRealizado
     * @return
     */
    public void removeFechaTrabajoRealizado(FechaTrabajoRealizado fechaTrabajoRealizado);
}
