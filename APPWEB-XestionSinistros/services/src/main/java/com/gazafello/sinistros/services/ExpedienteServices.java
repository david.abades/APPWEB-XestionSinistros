package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Expediente;

public interface ExpedienteServices {
    /**
     * Devolve lista de expedientes
     * 
     * @return
     */
    public List<Expediente> getAllExpedientes();

    /**
     * Devolve expediente por id
     * 
     * @param expedienteId
     * @return
     */
    public Expediente getExpediente(Integer expedienteId);

    /**
     * Crea un rexistro de expediente
     * 
     * @param newExpediente
     * @return
     */
    public Expediente saveExpediente(Expediente newExpediente);

    /**
     * Elimina un expediente
     * 
     * @param expediente
     * @return
     */
    public void removeExpediente(Expediente expediente);
}
