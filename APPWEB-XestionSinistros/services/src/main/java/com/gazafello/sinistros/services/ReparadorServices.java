package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Reparador;

public interface ReparadorServices {
    /**
     * Devolve lista de reparadors
     * 
     * @return
     */
    public List<Reparador> getAllReparadors();

    /**
     * Devolve reparador por id
     * 
     * @param reparadorId
     * @return
     */
    public Reparador getReparador(Integer reparadorId);

    /**
     * Crea un rexistro de reparador
     * 
     * @param newReparador
     * @return
     */
    public Reparador saveReparador(Reparador newReparador);

    /**
     * Elimina un reparador
     * 
     * @param reparador
     * @return
     */
    public void removeReparador(Reparador reparador);
}
