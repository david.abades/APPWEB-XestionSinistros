package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.ParteTrabajo;
import com.gazafello.sinistros.persistence.ParteTrabajoDao;

@Service("parteTrabajoServices")
@Profile("dev")
public class ParteTrabajoServicesImpl implements ParteTrabajoServices {
    private final Logger logger = LoggerFactory.getLogger(ParteTrabajoServicesImpl.class);
    private ParteTrabajoDao parteTrabajoDao;

    @Autowired
    public void setParteTrabajoDao(ParteTrabajoDao parteTrabajoDao) {
	this.parteTrabajoDao = parteTrabajoDao;
    }

    @Override
    public List<ParteTrabajo> getAllParteTrabajos() {
	logger.debug("Selecting all parteTrabajos stored...");

	return parteTrabajoDao.getAllParteTrabajos();
    }

    @Override
    public ParteTrabajo getParteTrabajo(Integer parteTrabajoId) {
	if (parteTrabajoId == null) {
	    return null;
	}
	logger.debug("Selecting parteTrabajo with id " + parteTrabajoId + " ...");

	return parteTrabajoDao.getParteTrabajo(parteTrabajoId);
    }

    @Override
    public ParteTrabajo saveParteTrabajo(ParteTrabajo newParteTrabajo) {
	if (newParteTrabajo == null) {
	    return null;
	}
	logger.debug("Saving parteTrabajo..." + newParteTrabajo);

	return parteTrabajoDao.saveParteTrabajo(newParteTrabajo);
    }

    @Override
    public void removeParteTrabajo(ParteTrabajo parteTrabajo) {
	logger.debug("Deleting parteTrabajo with id " + parteTrabajo.getId() + " ...");

	parteTrabajoDao.removeParteTrabajo(parteTrabajo);
    }
}
