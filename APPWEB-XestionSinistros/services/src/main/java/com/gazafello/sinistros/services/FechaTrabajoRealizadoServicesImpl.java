package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.FechaTrabajoRealizado;
import com.gazafello.sinistros.persistence.FechaTrabajoRealizadoDao;

@Service("fechaTrabajoRealizadoServices")
@Profile("dev")
public class FechaTrabajoRealizadoServicesImpl implements FechaTrabajoRealizadoServices {
    private final Logger logger = LoggerFactory.getLogger(FechaTrabajoRealizadoServicesImpl.class);
    private FechaTrabajoRealizadoDao fechaTrabajoRealizadoDao;

    @Autowired
    public void setFechaTrabajoRealizadoDao(FechaTrabajoRealizadoDao fechaTrabajoRealizadoDao) {
	this.fechaTrabajoRealizadoDao = fechaTrabajoRealizadoDao;
    }

    @Override
    public List<FechaTrabajoRealizado> getAllFechaTrabajoRealizados() {
	logger.debug("Selecting all fechaTrabajoRealizados stored...");

	return fechaTrabajoRealizadoDao.getAllFechaTrabajoRealizados();
    }

    @Override
    public FechaTrabajoRealizado getFechaTrabajoRealizado(Integer fechaTrabajoRealizadoId) {
	if (fechaTrabajoRealizadoId == null) {
	    return null;
	}
	logger.debug("Selecting fechaTrabajoRealizado with id " + fechaTrabajoRealizadoId + " ...");

	return fechaTrabajoRealizadoDao.getFechaTrabajoRealizado(fechaTrabajoRealizadoId);
    }

    @Override
    public FechaTrabajoRealizado saveFechaTrabajoRealizado(FechaTrabajoRealizado newFechaTrabajoRealizado) {
	if (newFechaTrabajoRealizado == null) {
	    return null;
	}
	logger.debug("Saving fechaTrabajoRealizado..." + newFechaTrabajoRealizado);

	return fechaTrabajoRealizadoDao.saveFechaTrabajoRealizado(newFechaTrabajoRealizado);
    }

    @Override
    public void removeFechaTrabajoRealizado(FechaTrabajoRealizado fechaTrabajoRealizado) {
	logger.debug("Deleting fechaTrabajoRealizado with id " + fechaTrabajoRealizado.getId() + " ...");

	fechaTrabajoRealizadoDao.removeFechaTrabajoRealizado(fechaTrabajoRealizado);
    }
}
