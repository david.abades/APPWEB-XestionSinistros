package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Estado;

public interface EstadoServices {
    /**
     * Devolve lista de estados
     * 
     * @return
     */
    public List<Estado> getAllEstados();

    /**
     * Devolve estado por id
     * 
     * @param estadoId
     * @return
     */
    public Estado getEstado(Integer estadoId);

    /**
     * Crea un rexistro de estado
     * 
     * @param newEstado
     * @return
     */
    public Estado saveEstado(Estado newEstado);

    /**
     * Elimina un estado
     * 
     * @param estado
     * @return
     */
    public void removeEstado(Estado estado);
}
