package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.ReaperturaExpediente;

public interface ReaperturaExpedienteServices {
    /**
     * Devolve lista de reaperturaExpedientes
     * 
     * @return
     */
    public List<ReaperturaExpediente> getAllReaperturaExpedientes();

    /**
     * Devolve reaperturaExpediente por id
     * 
     * @param reaperturaExpedienteId
     * @return
     */
    public ReaperturaExpediente getReaperturaExpediente(Integer reaperturaExpedienteId);

    /**
     * Crea un rexistro de reaperturaExpediente
     * 
     * @param newReaperturaExpediente
     * @return
     */
    public ReaperturaExpediente saveReaperturaExpediente(ReaperturaExpediente newReaperturaExpediente);

    /**
     * Elimina un reaperturaExpediente
     * 
     * @param reaperturaExpediente
     * @return
     */
    public void removeReaperturaExpediente(ReaperturaExpediente reaperturaExpediente);
}
