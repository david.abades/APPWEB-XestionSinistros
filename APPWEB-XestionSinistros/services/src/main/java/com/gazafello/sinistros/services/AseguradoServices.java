package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Asegurado;

public interface AseguradoServices {
    /**
     * Devolve lista de asegurados
     * 
     * @return
     */
    public List<Asegurado> getAllAsegurados();

    /**
     * Devolve asegurado por id
     * 
     * @param aseguradoId
     * @return
     */
    public Asegurado getAsegurado(Integer aseguradoId);

    /**
     * Crea un rexistro de asegurado
     * 
     * @param newAsegurado
     * @return
     */
    public Asegurado saveAsegurado(Asegurado newAsegurado);

    /**
     * Elimina un asegurado
     * 
     * @param asegurado
     * @return
     */
    public void removeAsegurado(Asegurado asegurado);
}
