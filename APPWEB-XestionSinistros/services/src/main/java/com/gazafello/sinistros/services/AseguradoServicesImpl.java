package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Asegurado;
import com.gazafello.sinistros.persistence.AseguradoDao;

@Service("aseguradoServices")
@Profile("dev")
public class AseguradoServicesImpl implements AseguradoServices {
    private final Logger logger = LoggerFactory.getLogger(AseguradoServicesImpl.class);
    private AseguradoDao aseguradoDao;

    @Autowired
    public void setAseguradoDao(AseguradoDao aseguradoDao) {
	this.aseguradoDao = aseguradoDao;
    }

    @Override
    public List<Asegurado> getAllAsegurados() {
	logger.debug("Selecting all asegurados stored...");

	return aseguradoDao.getAllAsegurados();
    }

    @Override
    public Asegurado getAsegurado(Integer aseguradoId) {
	if (aseguradoId == null) {
	    return null;
	}
	logger.debug("Selecting asegurado with id " + aseguradoId + " ...");

	return aseguradoDao.getAsegurado(aseguradoId);
    }

    @Override
    public Asegurado saveAsegurado(Asegurado newAsegurado) {
	if (newAsegurado == null) {
	    return null;
	}
	logger.debug("Saving asegurado..." + newAsegurado);

	return aseguradoDao.saveAsegurado(newAsegurado);
    }

    @Override
    public void removeAsegurado(Asegurado asegurado) {
	logger.debug("Deleting asegurado with id " + asegurado.getId() + " ...");

	aseguradoDao.removeAsegurado(asegurado);
    }
}
