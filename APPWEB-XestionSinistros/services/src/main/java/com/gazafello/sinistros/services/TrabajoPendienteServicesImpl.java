package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.TrabajoPendiente;
import com.gazafello.sinistros.persistence.TrabajoPendienteDao;

@Service("trabajoPendienteServices")
@Profile("dev")
public class TrabajoPendienteServicesImpl implements TrabajoPendienteServices {
    private final Logger logger = LoggerFactory.getLogger(TrabajoPendienteServicesImpl.class);
    private TrabajoPendienteDao trabajoPendienteDao;

    @Autowired
    public void setTrabajoPendienteDao(TrabajoPendienteDao trabajoPendienteDao) {
	this.trabajoPendienteDao = trabajoPendienteDao;
    }

    @Override
    public List<TrabajoPendiente> getAllTrabajoPendientes() {
	logger.debug("Selecting all trabajoPendientes stored...");

	return trabajoPendienteDao.getAllTrabajoPendientes();
    }

    @Override
    public TrabajoPendiente getTrabajoPendiente(Integer trabajoPendienteId) {
	if (trabajoPendienteId == null) {
	    return null;
	}
	logger.debug("Selecting trabajoPendiente with id " + trabajoPendienteId + " ...");

	return trabajoPendienteDao.getTrabajoPendiente(trabajoPendienteId);
    }

    @Override
    public TrabajoPendiente saveTrabajoPendiente(TrabajoPendiente newTrabajoPendiente) {
	if (newTrabajoPendiente == null) {
	    return null;
	}
	logger.debug("Saving trabajoPendiente..." + newTrabajoPendiente);

	return trabajoPendienteDao.saveTrabajoPendiente(newTrabajoPendiente);
    }

    @Override
    public void removeTrabajoPendiente(TrabajoPendiente trabajoPendiente) {
	logger.debug("Deleting trabajoPendiente with id " + trabajoPendiente.getId() + " ...");

	trabajoPendienteDao.removeTrabajoPendiente(trabajoPendiente);
    }
}
