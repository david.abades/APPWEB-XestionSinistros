package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Gremio;
import com.gazafello.sinistros.persistence.GremioDao;

@Service("gremioServices")
@Profile("dev")
public class GremioServicesImpl implements GremioServices {
    private final Logger logger = LoggerFactory.getLogger(GremioServicesImpl.class);
    private GremioDao gremioDao;

    @Autowired
    public void setGremioDao(GremioDao gremioDao) {
	this.gremioDao = gremioDao;
    }

    @Override
    public List<Gremio> getAllGremios() {
	logger.debug("Selecting all gremios stored...");

	return gremioDao.getAllGremios();
    }

    @Override
    public Gremio getGremio(Integer gremioId) {
	if (gremioId == null) {
	    return null;
	}
	logger.debug("Selecting gremio with id " + gremioId + " ...");

	return gremioDao.getGremio(gremioId);
    }

    @Override
    public Gremio saveGremio(Gremio newGremio) {
	if (newGremio == null) {
	    return null;
	}
	logger.debug("Saving gremio..." + newGremio);

	return gremioDao.saveGremio(newGremio);
    }

    @Override
    public void removeGremio(Gremio gremio) {
	logger.debug("Deleting gremio with id " + gremio.getId() + " ...");

	gremioDao.removeGremio(gremio);
    }
}
