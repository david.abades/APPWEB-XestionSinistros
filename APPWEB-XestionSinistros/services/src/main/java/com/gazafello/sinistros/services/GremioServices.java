package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Gremio;

public interface GremioServices {
    /**
     * Devolve lista de gremios
     * 
     * @return
     */
    public List<Gremio> getAllGremios();

    /**
     * Devolve gremio por id
     * 
     * @param gremioId
     * @return
     */
    public Gremio getGremio(Integer gremioId);

    /**
     * Crea un rexistro de gremio
     * 
     * @param newGremio
     * @return
     */
    public Gremio saveGremio(Gremio newGremio);

    /**
     * Elimina un gremio
     * 
     * @param gremio
     * @return
     */
    public void removeGremio(Gremio gremio);
}
