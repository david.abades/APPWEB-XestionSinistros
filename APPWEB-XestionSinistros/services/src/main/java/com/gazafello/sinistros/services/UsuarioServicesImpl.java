package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Usuario;
import com.gazafello.sinistros.persistence.UsuarioDao;

@Service("usuarioServices")
@Profile("dev")
public class UsuarioServicesImpl implements UsuarioServices {
    private final Logger logger = LoggerFactory.getLogger(UsuarioServicesImpl.class);
    private UsuarioDao usuarioDao;

    @Autowired
    public void setUsuarioDao(UsuarioDao usuarioDao) {
	this.usuarioDao = usuarioDao;
    }

    @Override
    public List<Usuario> getAllUsuarios() {
	logger.debug("Selecting all companies stored...");

	return usuarioDao.getAllUsuarios();
    }

    @Override
    public Usuario getUsuario(Integer usuarioId) {
	if (usuarioId == null) {
	    return null;
	}
	logger.debug("Selecting usuario with id " + usuarioId + " ...");

	return usuarioDao.getUsuario(usuarioId);
    }

    @Override
    public Usuario saveUsuario(Usuario newUsuario) {
	if (newUsuario == null) {
	    return null;
	}
	logger.debug("Saving usuario..." + newUsuario);

	return usuarioDao.saveUsuario(newUsuario);
    }

    @Override
    public void removeUsuario(Usuario usuario) {
	logger.debug("Deleting usuario with id " + usuario.getId() + " ...");

	usuarioDao.removeUsuario(usuario);
    }
}
