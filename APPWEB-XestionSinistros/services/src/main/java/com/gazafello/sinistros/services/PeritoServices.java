package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Perito;

public interface PeritoServices {
    /**
     * Devolve lista de peritos
     * 
     * @return
     */
    public List<Perito> getAllPeritos();

    /**
     * Devolve perito por id
     * 
     * @param peritoId
     * @return
     */
    public Perito getPerito(Integer peritoId);

    /**
     * Crea un rexistro de perito
     * 
     * @param newPerito
     * @return
     */
    public Perito savePerito(Perito newPerito);

    /**
     * Elimina un perito
     * 
     * @param perito
     * @return
     */
    public void removePerito(Perito perito);
}
