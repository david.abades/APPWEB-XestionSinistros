package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.TrabajoRealizado;
import com.gazafello.sinistros.persistence.TrabajoRealizadoDao;

@Service("trabajoRealizadoServices")
@Profile("dev")
public class TrabajoRealizadoServicesImpl implements TrabajoRealizadoServices {
    private final Logger logger = LoggerFactory.getLogger(TrabajoRealizadoServicesImpl.class);
    private TrabajoRealizadoDao trabajoRealizadoDao;

    @Autowired
    public void setTrabajoRealizadoDao(TrabajoRealizadoDao trabajoRealizadoDao) {
	this.trabajoRealizadoDao = trabajoRealizadoDao;
    }

    @Override
    public List<TrabajoRealizado> getAllTrabajoRealizados() {
	logger.debug("Selecting all trabajoRealizados stored...");

	return trabajoRealizadoDao.getAllTrabajoRealizados();
    }

    @Override
    public TrabajoRealizado getTrabajoRealizado(Integer trabajoRealizadoId) {
	if (trabajoRealizadoId == null) {
	    return null;
	}
	logger.debug("Selecting trabajoRealizado with id " + trabajoRealizadoId + " ...");

	return trabajoRealizadoDao.getTrabajoRealizado(trabajoRealizadoId);
    }

    @Override
    public TrabajoRealizado saveTrabajoRealizado(TrabajoRealizado newTrabajoRealizado) {
	if (newTrabajoRealizado == null) {
	    return null;
	}
	logger.debug("Saving trabajoRealizado..." + newTrabajoRealizado);

	return trabajoRealizadoDao.saveTrabajoRealizado(newTrabajoRealizado);
    }

    @Override
    public void removeTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
	logger.debug("Deleting trabajoRealizado with id " + trabajoRealizado.getId() + " ...");

	trabajoRealizadoDao.removeTrabajoRealizado(trabajoRealizado);
    }
}
