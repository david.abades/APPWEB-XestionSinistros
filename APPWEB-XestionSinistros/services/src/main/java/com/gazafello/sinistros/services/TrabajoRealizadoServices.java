package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.TrabajoRealizado;

public interface TrabajoRealizadoServices {
    /**
     * Devolve lista de trabajoRealizados
     * 
     * @return
     */
    public List<TrabajoRealizado> getAllTrabajoRealizados();

    /**
     * Devolve trabajoRealizado por id
     * 
     * @param trabajoRealizadoId
     * @return
     */
    public TrabajoRealizado getTrabajoRealizado(Integer trabajoRealizadoId);

    /**
     * Crea un rexistro de trabajoRealizado
     * 
     * @param newTrabajoRealizado
     * @return
     */
    public TrabajoRealizado saveTrabajoRealizado(TrabajoRealizado newTrabajoRealizado);

    /**
     * Elimina un trabajoRealizado
     * 
     * @param trabajoRealizado
     * @return
     */
    public void removeTrabajoRealizado(TrabajoRealizado trabajoRealizado);
}
