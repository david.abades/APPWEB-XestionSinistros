package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Compania;

public interface CompaniaServices {
    /**
     * Devolve lista de companias
     * 
     * @return
     */
    public List<Compania> getAllCompanias();

    /**
     * Devolve compania por id
     * 
     * @param companiaId
     * @return
     */
    public Compania getCompania(Integer companiaId);

    /**
     * Crea un rexistro de compania
     * 
     * @param newCompania
     * @return
     */
    public Compania saveCompania(Compania newCompania);

    /**
     * Elimina un compania
     * 
     * @param compania
     * @return
     */
    public void removeCompania(Compania compania);
}
