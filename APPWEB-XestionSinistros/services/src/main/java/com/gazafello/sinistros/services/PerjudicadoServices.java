package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Perjudicado;

public interface PerjudicadoServices {
    /**
     * Devolve lista de perjudicados
     * 
     * @return
     */
    public List<Perjudicado> getAllPerjudicados();

    /**
     * Devolve perjudicado por id
     * 
     * @param perjudicadoId
     * @return
     */
    public Perjudicado getPerjudicado(Integer perjudicadoId);

    /**
     * Crea un rexistro de perjudicado
     * 
     * @param newPerjudicado
     * @return
     */
    public Perjudicado savePerjudicado(Perjudicado newPerjudicado);

    /**
     * Elimina un perjudicado
     * 
     * @param perjudicado
     * @return
     */
    public void removePerjudicado(Perjudicado perjudicado);
}
