package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.Expediente;
import com.gazafello.sinistros.persistence.ExpedienteDao;

@Service("expedienteServices")
@Profile("dev")
public class ExpedienteServicesImpl implements ExpedienteServices {
    private final Logger logger = LoggerFactory.getLogger(ExpedienteServicesImpl.class);
    private ExpedienteDao expedienteDao;

    @Autowired
    public void setExpedienteDao(ExpedienteDao expedienteDao) {
	this.expedienteDao = expedienteDao;
    }

    @Override
    public List<Expediente> getAllExpedientes() {
	logger.debug("Selecting all expedientes stored...");

	return expedienteDao.getAllExpedientes();
    }

    @Override
    public Expediente getExpediente(Integer expedienteId) {
	if (expedienteId == null) {
	    return null;
	}
	logger.debug("Selecting expediente with id " + expedienteId + " ...");

	return expedienteDao.getExpediente(expedienteId);
    }

    @Override
    public Expediente saveExpediente(Expediente newExpediente) {
	if (newExpediente == null) {
	    return null;
	}
	logger.debug("Saving expediente..." + newExpediente);

	return expedienteDao.saveExpediente(newExpediente);
    }

    @Override
    public void removeExpediente(Expediente expediente) {
	logger.debug("Deleting expediente with id " + expediente.getId() + " ...");

	expedienteDao.removeExpediente(expediente);
    }
}
