package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.CodigoPostal;

public interface CodigoPostalServices {
    /**
     * Devolve lista de codigosPostales
     * 
     * @return
     */
    public List<CodigoPostal> getAllCodigoPostals();

    /**
     * Devolve codigoPostal por id
     * 
     * @param codigoPostalId
     * @return
     */
    public CodigoPostal getCodigoPostal(Integer codigoPostalId);

    /**
     * Crea un rexistro de codigoPostal
     * 
     * @param newCodigoPostal
     * @return
     */
    public CodigoPostal saveCodigoPostal(CodigoPostal newCodigoPostal);

    /**
     * Elimina un codigoPostal
     * 
     * @param codigoPostal
     * @return
     */
    public void removeCodigoPostal(CodigoPostal codigoPostal);
}
