package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.Usuario;

public interface UsuarioServices {
    /**
     * Devolve lista de usuarios
     * 
     * @return
     */
    public List<Usuario> getAllUsuarios();

    /**
     * Devolve usuario por id
     * 
     * @param usuarioId
     * @return
     */
    public Usuario getUsuario(Integer usuarioId);

    /**
     * Crea un rexistro de usuario
     * 
     * @param newUsuario
     * @return
     */
    public Usuario saveUsuario(Usuario newUsuario);

    /**
     * Elimina un usuario
     * 
     * @param usuario
     * @return
     */
    public void removeUsuario(Usuario usuario);
}
