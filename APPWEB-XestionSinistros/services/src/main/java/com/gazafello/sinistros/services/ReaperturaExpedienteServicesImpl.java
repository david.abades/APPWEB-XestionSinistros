package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.ReaperturaExpediente;
import com.gazafello.sinistros.persistence.ReaperturaExpedienteDao;

@Service("reaperturaExpedienteServices")
@Profile("dev")
public class ReaperturaExpedienteServicesImpl implements ReaperturaExpedienteServices {
    private final Logger logger = LoggerFactory.getLogger(ReaperturaExpedienteServicesImpl.class);
    private ReaperturaExpedienteDao reaperturaExpedienteDao;

    @Autowired
    public void setReaperturaExpedienteDao(ReaperturaExpedienteDao reaperturaExpedienteDao) {
	this.reaperturaExpedienteDao = reaperturaExpedienteDao;
    }

    @Override
    public List<ReaperturaExpediente> getAllReaperturaExpedientes() {
	logger.debug("Selecting all reaperturaExpedientes stored...");

	return reaperturaExpedienteDao.getAllReaperturaExpedientes();
    }

    @Override
    public ReaperturaExpediente getReaperturaExpediente(Integer reaperturaExpedienteId) {
	if (reaperturaExpedienteId == null) {
	    return null;
	}
	logger.debug("Selecting reaperturaExpediente with id " + reaperturaExpedienteId + " ...");

	return reaperturaExpedienteDao.getReaperturaExpediente(reaperturaExpedienteId);
    }

    @Override
    public ReaperturaExpediente saveReaperturaExpediente(ReaperturaExpediente newReaperturaExpediente) {
	if (newReaperturaExpediente == null) {
	    return null;
	}
	logger.debug("Saving reaperturaExpediente..." + newReaperturaExpediente);

	return reaperturaExpedienteDao.saveReaperturaExpediente(newReaperturaExpediente);
    }

    @Override
    public void removeReaperturaExpediente(ReaperturaExpediente reaperturaExpediente) {
	logger.debug("Deleting reaperturaExpediente with id " + reaperturaExpediente.getId() + " ...");

	reaperturaExpedienteDao.removeReaperturaExpediente(reaperturaExpediente);
    }
}
