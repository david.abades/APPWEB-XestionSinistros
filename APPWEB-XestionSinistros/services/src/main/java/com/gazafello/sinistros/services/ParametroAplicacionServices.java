package com.gazafello.sinistros.services;

import java.util.List;

import com.gazafello.sinistros.domain.ParametroAplicacion;

public interface ParametroAplicacionServices {
    /**
     * Devolve lista de parametroAplicacions
     * 
     * @return
     */
    public List<ParametroAplicacion> getAllParametroAplicacion();

    /**
     * Devolve parametroAplicacion por id
     * 
     * @param parametroAplicacionId
     * @return
     */
    public ParametroAplicacion getParametroAplicacion(Integer parametroAplicacionId);

    /**
     * Crea un rexistro de parametroAplicacion
     * 
     * @param newParametroAplicacion
     * @return
     */
    public ParametroAplicacion saveParametroAplicacion(ParametroAplicacion newParametroAplicacion);

    /**
     * Elimina un parametroAplicacion
     * 
     * @param parametroAplicacion
     * @return
     */
    public void removeParametroAplicacion(ParametroAplicacion parametroAplicacion);
}
