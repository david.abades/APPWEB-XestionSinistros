package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.CodigoPostal;
import com.gazafello.sinistros.persistence.CodigoPostalDao;

@Service("codigoPostalServices")
@Profile("dev")
public class CodigoPostalServicesImpl implements CodigoPostalServices {
    private final Logger logger = LoggerFactory.getLogger(CodigoPostalServicesImpl.class);
    private CodigoPostalDao codigoPostalDao;

    @Autowired
    public void setCodigoPostalDao(CodigoPostalDao codigoPostalDao) {
	this.codigoPostalDao = codigoPostalDao;
    }

    @Override
    public List<CodigoPostal> getAllCodigoPostals() {
	logger.debug("Selecting all codigosPostales stored...");

	return codigoPostalDao.getAllCodigoPostals();
    }

    @Override
    public CodigoPostal getCodigoPostal(Integer codigoPostalId) {
	if (codigoPostalId == null) {
	    return null;
	}
	logger.debug("Selecting codigoPostal with id " + codigoPostalId + " ...");

	return codigoPostalDao.getCodigoPostal(codigoPostalId);
    }

    @Override
    public CodigoPostal saveCodigoPostal(CodigoPostal newCodigoPostal) {
	if (newCodigoPostal == null) {
	    return null;
	}
	logger.debug("Saving codigoPostal..." + newCodigoPostal);

	return codigoPostalDao.saveCodigoPostal(newCodigoPostal);
    }

    @Override
    public void removeCodigoPostal(CodigoPostal codigoPostal) {
	logger.debug("Deleting codigoPostal with id " + codigoPostal.getId() + " ...");

	codigoPostalDao.removeCodigoPostal(codigoPostal);
    }
}
