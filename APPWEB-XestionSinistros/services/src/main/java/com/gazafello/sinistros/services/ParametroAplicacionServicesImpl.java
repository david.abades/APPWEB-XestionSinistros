package com.gazafello.sinistros.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.gazafello.sinistros.domain.ParametroAplicacion;
import com.gazafello.sinistros.persistence.ParametroAplicacionDao;

@Service("parametroAplicacionServices")
@Profile("dev")
public class ParametroAplicacionServicesImpl implements ParametroAplicacionServices {
    private final Logger logger = LoggerFactory.getLogger(ParametroAplicacionServicesImpl.class);
    private ParametroAplicacionDao parametroAplicacionDao;

    @Autowired
    public void setParametroAplicacionDao(ParametroAplicacionDao parametroAplicacionDao) {
	this.parametroAplicacionDao = parametroAplicacionDao;
    }

    @Override
    public List<ParametroAplicacion> getAllParametroAplicacion() {
	logger.debug("Selecting all parametroAplicacions stored...");

	return parametroAplicacionDao.getAllParametroAplicacion();
    }

    @Override
    public ParametroAplicacion getParametroAplicacion(Integer parametroAplicacionId) {
	if (parametroAplicacionId == null) {
	    return null;
	}
	logger.debug("Selecting parametroAplicacion with id " + parametroAplicacionId + " ...");

	return parametroAplicacionDao.getParametroAplicacion(parametroAplicacionId);
    }

    @Override
    public ParametroAplicacion saveParametroAplicacion(ParametroAplicacion newParametroAplicacion) {
	if (newParametroAplicacion == null) {
	    return null;
	}
	logger.debug("Saving parametroAplicacion..." + newParametroAplicacion);

	return parametroAplicacionDao.saveParametroAplicacion(newParametroAplicacion);
    }

    @Override
    public void removeParametroAplicacion(ParametroAplicacion parametroAplicacion) {
	logger.debug("Deleting parametroAplicacion with id " + parametroAplicacion.getId() + " ...");

	parametroAplicacionDao.removeParametroAplicacion(parametroAplicacion);
    }
}
