package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.TrabajoRealizado;

@Repository
@Transactional
@Profile("dev")
public class TrabajoRealizadoDaoImpl implements TrabajoRealizadoDao {
    private Logger logger = LoggerFactory.getLogger(TrabajoRealizadoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrabajoRealizado> getAllTrabajoRealizados() {
	logger.debug("Selecting all trabajoRealizados stored...");

	Query query = entityManager.createNamedQuery("TrabajoRealizado.findAll", TrabajoRealizado.class);

	logger.debug("Querying database after all trabajoRealizados => " + query.toString());

	@SuppressWarnings("unchecked")
	List<TrabajoRealizado> companies = query.getResultList();

	logger.debug("List of trabajoRealizados found! Retrieving " + companies.size() + " trabajoRealizado(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public TrabajoRealizado getTrabajoRealizado(Integer TrabajoRealizadoId) {
	logger.debug("Selecting TrabajoRealizado with id " + TrabajoRealizadoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<TrabajoRealizado> graph = (EntityGraph<TrabajoRealizado>) entityManager.getEntityGraph("trabajoRealizado");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	TrabajoRealizado comp = entityManager.find(TrabajoRealizado.class, TrabajoRealizadoId, props);

	return comp;
    }

    @Override
    public TrabajoRealizado saveTrabajoRealizado(TrabajoRealizado newTrabajoRealizado) {
	logger.debug("Saving TrabajoRealizado..." + newTrabajoRealizado);
	if (newTrabajoRealizado == null) {
	    return null;
	}

	if (newTrabajoRealizado.getId() == null) {
	    logger.info("Creating new TrabajoRealizado " + newTrabajoRealizado + "...");
	    newTrabajoRealizado = entityManager.merge(newTrabajoRealizado);
	} else {
	    logger.info("Updating TrabajoRealizado " + newTrabajoRealizado + "...");
	    newTrabajoRealizado = entityManager.merge(newTrabajoRealizado);
	}

	logger.info("New TrabajoRealizado saved with id " + newTrabajoRealizado.getId() + ".");
	return newTrabajoRealizado;
    }

    @Override
    public void removeTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
	logger.debug("Removing TrabajoRealizado: " + trabajoRealizado + " ...");

	logger.info("Removing TrabajoRealizado " + trabajoRealizado + "...");
	entityManager.merge(trabajoRealizado);

	logger.info("Removed TrabajoRealizado with id " + trabajoRealizado.getId() + ".");
    }

    // @Override
    // public void removeTrabajoRealizado(TrabajoRealizado trabajoRealizado) {
    // logger.debug("Removing TrabajoRealizado: " + trabajoRealizado + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<TrabajoRealizado> graph = (EntityGraph<TrabajoRealizado>)
    // entityManager.getEntityGraph("trabajoRealizado");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(trabajoRealizado) ?
    // trabajoRealizado
    // :
    // entityManager.merge(trabajoRealizado));
    // }

}
