package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Gremio;

public interface GremioDao {
    /**
     * Devuelve la lista de todos los Gremios
     * 
     * @return
     */
    public List<Gremio> getAllGremios();

    /**
     * Devuelve un Gremio por ID
     * 
     * @param gremioId
     * @return
     */
    public Gremio getGremio(Integer gremioId);

    /**
     * Añade una Gremio nuevo
     * 
     * @param newGremio
     * @return
     */
    public Gremio saveGremio(Gremio newGremio);

    /**
     * Elimina un gremio
     * 
     * @param gremio
     * @return
     */
    public void removeGremio(Gremio gremio);
}
