package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.TrabajoRealizado;

public interface TrabajoRealizadoDao {
    /**
     * Devuelve la lista de todos los TrabajoRealizados
     * 
     * @return
     */
    public List<TrabajoRealizado> getAllTrabajoRealizados();

    /**
     * Devuelve un TrabajoRealizado por ID
     * 
     * @param trabajoRealizadoId
     * @return
     */
    public TrabajoRealizado getTrabajoRealizado(Integer trabajoRealizadoId);

    /**
     * Añade un TrabajoRealizado nuevo
     * 
     * @param newTrabajoRealizado
     * @return
     */
    public TrabajoRealizado saveTrabajoRealizado(TrabajoRealizado newTrabajoRealizado);

    /**
     * Elimina un trabajoRealizado
     * 
     * @param trabajoRealizado
     * @return
     */
    public void removeTrabajoRealizado(TrabajoRealizado trabajoRealizado);
}
