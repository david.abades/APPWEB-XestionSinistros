package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Perjudicado;

public interface PerjudicadoDao {
    /**
     * Devuelve la lista de todos los perjudicados
     * 
     * @return
     */
    public List<Perjudicado> getAllPerjudicados();

    /**
     * Devuelve un perjudicado por ID
     * 
     * @param PerjudicadoId
     * @return
     */
    public Perjudicado getPerjudicado(Integer PerjudicadoId);

    /**
     * Añade un perjudicado nuevo
     * 
     * @param newPerjudicado
     * @return
     */
    public Perjudicado savePerjudicado(Perjudicado newPerjudicado);

    /**
     * Elimina un perjudicado
     * 
     * @param perjudicado
     * @return
     */
    public void removePerjudicado(Perjudicado perjudicado);
}
