package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.FechaTrabajoRealizado;

public interface FechaTrabajoRealizadoDao {
    /**
     * Devuelve la lista de todos los FechaTrabajoRealizados
     * 
     * @return
     */
    public List<FechaTrabajoRealizado> getAllFechaTrabajoRealizados();

    /**
     * Devuelve un FechaTrabajoRealizado por ID
     * 
     * @param fechaTrabajoRealizadoId
     * @return
     */
    public FechaTrabajoRealizado getFechaTrabajoRealizado(Integer fechaTrabajoRealizadoId);

    /**
     * Añade un FechaTrabajoRealizado nuevo
     * 
     * @param newFechaTrabajoRealizado
     * @return
     */
    public FechaTrabajoRealizado saveFechaTrabajoRealizado(FechaTrabajoRealizado newFechaTrabajoRealizado);

    /**
     * Elimina un fechaTrabajoRealizado
     * 
     * @param fechaTrabajoRealizado
     * @return
     */
    public void removeFechaTrabajoRealizado(FechaTrabajoRealizado fechaTrabajoRealizado);
}
