package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.ParteTrabajo;

public interface ParteTrabajoDao {
    /**
     * Devuelve la lista de todos los ParteTrabajos
     * 
     * @return
     */
    public List<ParteTrabajo> getAllParteTrabajos();

    /**
     * Devuelve un ParteTrabajo por ID
     * 
     * @param parteTrabajoId
     * @return
     */
    public ParteTrabajo getParteTrabajo(Integer parteTrabajoId);

    /**
     * Añade un ParteTrabajo nuevo
     * 
     * @param newParteTrabajo
     * @return
     */
    public ParteTrabajo saveParteTrabajo(ParteTrabajo newParteTrabajo);

    /**
     * Elimina un parteTrabajo
     * 
     * @param parteTrabajo
     * @return
     */
    public void removeParteTrabajo(ParteTrabajo parteTrabajo);
}
