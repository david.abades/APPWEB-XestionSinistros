package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.ParametroAplicacion;

@Repository
@Transactional
@Profile("dev")
public class ParametroAplicacionDaoImpl implements ParametroAplicacionDao {
    private Logger logger = LoggerFactory.getLogger(ParametroAplicacionDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ParametroAplicacion> getAllParametroAplicacion() {
	logger.debug("Selecting all parametrosAplicacions stored...");

	Query query = entityManager.createNamedQuery("ParametroAplicacion.findAll", ParametroAplicacion.class);

	logger.debug("Querying database after all parametrosAplicacions => " + query.toString());

	@SuppressWarnings("unchecked")
	List<ParametroAplicacion> parametrosAplicacions = query.getResultList();

	logger.debug("List of parametrosAplicacions found! Retrieving {} parametrosAplicacion(s) : {}.", parametrosAplicacions.size(), parametrosAplicacions.toString());

	return parametrosAplicacions;
    }

    @Transactional(readOnly = true)
    @Override
    public ParametroAplicacion getParametroAplicacion(Integer ParametroAplicacionId) {
	logger.debug("Selecting ParametroAplicacion with id " + ParametroAplicacionId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<ParametroAplicacion> graph = (EntityGraph<ParametroAplicacion>) entityManager.getEntityGraph("parametroAplicacion");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	ParametroAplicacion comp = entityManager.find(ParametroAplicacion.class, ParametroAplicacionId, props);

	return comp;
    }

    @Override
    public ParametroAplicacion saveParametroAplicacion(ParametroAplicacion newParametroAplicacion) {
	logger.debug("Saving ParametroAplicacion..." + newParametroAplicacion);

	if (newParametroAplicacion == null) {
	    return null;
	}

	if (newParametroAplicacion.getId() == null) {
	    logger.info("Creating new ParametroAplicacion " + newParametroAplicacion + "...");
	    newParametroAplicacion = entityManager.merge(newParametroAplicacion);
	} else {
	    logger.info("Updating ParametroAplicacion " + newParametroAplicacion + "...");
	    newParametroAplicacion = entityManager.merge(newParametroAplicacion);
	}

	logger.info("New ParametroAplicacion " + newParametroAplicacion.getNombreTramitadora() + " saved with id " + newParametroAplicacion.getId() + ".");
	return newParametroAplicacion;
    }

    @Override
    public void removeParametroAplicacion(ParametroAplicacion parametrosAplicacion) {
	logger.debug("Removing ParametroAplicacion: " + parametrosAplicacion + " ...");

	logger.info("Removing ParametroAplicacion " + parametrosAplicacion + "...");
	entityManager.merge(parametrosAplicacion);

	logger.info("Removed ParametroAplicacion with id " + parametrosAplicacion.getId() + ".");
    }

    // @Override
    // public void removeParametroAplicacion(ParametroAplicacion
    // parametrosAplicacion) {
    // logger.debug("Removing ParametroAplicacion: " + parametrosAplicacion + "
    // ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<ParametroAplicacion> graph =
    // (EntityGraph<ParametroAplicacion>)
    // entityManager.getEntityGraph("ParametroAplicacion");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(parametrosAplicacion) ?
    // parametrosAplicacion : entityManager.merge(parametrosAplicacion));
    // }

}
