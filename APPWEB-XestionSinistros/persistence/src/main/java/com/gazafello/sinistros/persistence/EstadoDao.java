package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Estado;

public interface EstadoDao {
    /**
     * Devuelve la lista de todas las estados
     * 
     * @return
     */
    public List<Estado> getAllEstados();

    /**
     * Devuelve una estado por ID
     * 
     * @param EstadoId
     * @return
     */
    public Estado getEstado(Integer EstadoId);

    /**
     * Añade una estado nueva
     * 
     * @param newEstado
     * @return
     */
    public Estado saveEstado(Estado newEstado);

    /**
     * Elimina un estado
     * 
     * @param estado
     * @return
     */
    public void removeEstado(Estado estado);
}
