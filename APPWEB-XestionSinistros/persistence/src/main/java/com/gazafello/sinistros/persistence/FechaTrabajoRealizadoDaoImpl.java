package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.FechaTrabajoRealizado;

@Repository
@Transactional
@Profile("dev")
public class FechaTrabajoRealizadoDaoImpl implements FechaTrabajoRealizadoDao {
    private Logger logger = LoggerFactory.getLogger(FechaTrabajoRealizadoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<FechaTrabajoRealizado> getAllFechaTrabajoRealizados() {
	logger.debug("Selecting all fechaTrabajoRealizados stored...");

	Query query = entityManager.createNamedQuery("FechaTrabajoRealizado.findAll", FechaTrabajoRealizado.class);

	logger.debug("Querying database after all fechaTrabajoRealizados => " + query.toString());

	@SuppressWarnings("unchecked")
	List<FechaTrabajoRealizado> companies = query.getResultList();

	logger.debug("List of fechaTrabajoRealizados found! Retrieving " + companies.size() + " fechaTrabajoRealizado(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public FechaTrabajoRealizado getFechaTrabajoRealizado(Integer FechaTrabajoRealizadoId) {
	logger.debug("Selecting FechaTrabajoRealizado with id " + FechaTrabajoRealizadoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<FechaTrabajoRealizado> graph = (EntityGraph<FechaTrabajoRealizado>) entityManager.getEntityGraph("fechaTrabajoRealizado");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	FechaTrabajoRealizado comp = entityManager.find(FechaTrabajoRealizado.class, FechaTrabajoRealizadoId, props);

	return comp;
    }

    @Override
    public FechaTrabajoRealizado saveFechaTrabajoRealizado(FechaTrabajoRealizado newFechaTrabajoRealizado) {
	logger.debug("Saving FechaTrabajoRealizado..." + newFechaTrabajoRealizado);
	if (newFechaTrabajoRealizado == null) {
	    return null;
	}

	if (newFechaTrabajoRealizado.getId() == null) {
	    logger.info("Creating new FechaTrabajoRealizado " + newFechaTrabajoRealizado + "...");
	    newFechaTrabajoRealizado = entityManager.merge(newFechaTrabajoRealizado);
	} else {
	    logger.info("Updating FechaTrabajoRealizado " + newFechaTrabajoRealizado + "...");
	    newFechaTrabajoRealizado = entityManager.merge(newFechaTrabajoRealizado);
	}

	logger.info("New FechaTrabajoRealizado saved with id " + newFechaTrabajoRealizado.getId() + ".");
	return newFechaTrabajoRealizado;
    }

    @Override
    public void removeFechaTrabajoRealizado(FechaTrabajoRealizado fechaTrabajoRealizado) {
	logger.debug("Removing FechaTrabajoRealizado: " + fechaTrabajoRealizado + " ...");

	logger.info("Removing FechaTrabajoRealizado " + fechaTrabajoRealizado + "...");
	entityManager.merge(fechaTrabajoRealizado);

	logger.info("Removed FechaTrabajoRealizado with id " + fechaTrabajoRealizado.getId() + ".");
    }

    // @Override
    // public void removeFechaTrabajoRealizado(FechaTrabajoRealizado
    // fechaTrabajoRealizado) {
    // logger.debug("Removing FechaTrabajoRealizado: " + fechaTrabajoRealizado +
    // " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<FechaTrabajoRealizado> graph =
    // (EntityGraph<FechaTrabajoRealizado>)
    // entityManager.getEntityGraph("fechaTrabajoRealizado");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(fechaTrabajoRealizado) ?
    // fechaTrabajoRealizado
    // :
    // entityManager.merge(fechaTrabajoRealizado));
    // }

}
