package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.ReaperturaExpediente;

public interface ReaperturaExpedienteDao {
    /**
     * Devuelve la lista de todos los reaperturaExpedientes
     * 
     * @return
     */
    public List<ReaperturaExpediente> getAllReaperturaExpedientes();

    /**
     * Devuelve un reaperturaExpediente por ID
     * 
     * @param ReaperturaExpedienteId
     * @return
     */
    public ReaperturaExpediente getReaperturaExpediente(Integer ReaperturaExpedienteId);

    /**
     * Añade un reaperturaExpediente nuevo
     * 
     * @param newReaperturaExpediente
     * @return
     */
    public ReaperturaExpediente saveReaperturaExpediente(ReaperturaExpediente newReaperturaExpediente);

    /**
     * Elimina un reaperturaExpediente
     * 
     * @param reaperturaExpediente
     * @return
     */
    public void removeReaperturaExpediente(ReaperturaExpediente reaperturaExpediente);
}
