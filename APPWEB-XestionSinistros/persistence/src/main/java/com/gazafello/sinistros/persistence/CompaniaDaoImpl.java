package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Compania;

@Repository
@Transactional
@Profile("dev")
public class CompaniaDaoImpl implements CompaniaDao {
    private Logger logger = LoggerFactory.getLogger(CompaniaDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Compania> getAllCompanias() {
	logger.debug("Selecting all companias stored...");

	Query query = entityManager.createNamedQuery("Compania.findAll", Compania.class);

	logger.debug("Querying database after all companias => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Compania> companies = query.getResultList();

	logger.debug("List of companias found! Retrieving " + companies.size() + " compania(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public Compania getCompania(Integer companiaId) {
	logger.debug("Selecting Compania with id " + companiaId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Compania> graph = (EntityGraph<Compania>) entityManager.getEntityGraph("compania");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Compania comp = entityManager.find(Compania.class, companiaId, props);

	return comp;
    }

    @Override
    public Compania saveCompania(Compania newCompania) {
	logger.debug("Saving Compania..." + newCompania);
	if (newCompania == null) {
	    return null;
	}

	if (newCompania.getId() == null) {
	    logger.info("Creating new Compania " + newCompania + "...");
	    newCompania = entityManager.merge(newCompania);
	} else {
	    logger.info("Updating Compania " + newCompania + "...");
	    newCompania = entityManager.merge(newCompania);
	}

	logger.info("New Compania " + newCompania.getNombre() + " saved with id " + newCompania.getId() + ".");
	return newCompania;
    }

    @Override
    public void removeCompania(Compania compania) {
	logger.debug("Removing Compania: " + compania + " ...");

	logger.info("Removing Compania " + compania + "...");
	entityManager.merge(compania);

	logger.info("Removed Compania " + compania.getNombre() + " with id " + compania.getId() + ".");
    }

    // @Override
    // public void removeCompania(Compania compania) {
    // logger.debug("Removing Compania: " + compania + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Compania> graph = (EntityGraph<Compania>)
    // entityManager.getEntityGraph("compania");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(compania) ? compania :
    // entityManager.merge(compania));
    // }

}
