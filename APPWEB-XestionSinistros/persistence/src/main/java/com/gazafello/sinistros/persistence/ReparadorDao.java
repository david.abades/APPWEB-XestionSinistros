package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Reparador;

public interface ReparadorDao {
    /**
     * Devuelve la lista de todos los reparadors
     * 
     * @return
     */
    public List<Reparador> getAllReparadors();

    /**
     * Devuelve un reparador por ID
     * 
     * @param ReparadorId
     * @return
     */
    public Reparador getReparador(Integer ReparadorId);

    /**
     * Añade un reparador nuevo
     * 
     * @param newReparador
     * @return
     */
    public Reparador saveReparador(Reparador newReparador);

    /**
     * Elimina un reparador
     * 
     * @param reparador
     * @return
     */
    public void removeReparador(Reparador reparador);
}
