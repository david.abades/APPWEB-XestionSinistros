package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.ConceptoFacturable;

@Repository
@Transactional
@Profile("dev")
public class ConceptoFacturableDaoImpl implements ConceptoFacturableDao {
    private Logger logger = LoggerFactory.getLogger(ConceptoFacturableDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ConceptoFacturable> getAllConceptoFacturables() {
	logger.debug("Selecting all conceptosFacturabless stored...");

	Query query = entityManager.createNamedQuery("ConceptoFacturable.findAll", ConceptoFacturable.class);

	logger.debug("Querying database after all conceptosFacturabless => " + query.toString());

	@SuppressWarnings("unchecked")
	List<ConceptoFacturable> conceptosFacturabless = query.getResultList();

	logger.debug("List of conceptosFacturabless found! Retrieving {} conceptosFacturables(s) : {}.", conceptosFacturabless.size(), conceptosFacturabless.toString());

	return conceptosFacturabless;
    }

    @Transactional(readOnly = true)
    @Override
    public ConceptoFacturable getConceptoFacturable(Integer ConceptoFacturableId) {
	logger.debug("Selecting ConceptoFacturable with id " + ConceptoFacturableId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<ConceptoFacturable> graph = (EntityGraph<ConceptoFacturable>) entityManager.getEntityGraph("conceptoFacturable");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	ConceptoFacturable comp = entityManager.find(ConceptoFacturable.class, ConceptoFacturableId, props);

	return comp;
    }

    @Override
    public ConceptoFacturable saveConceptoFacturable(ConceptoFacturable newConceptoFacturable) {
	logger.debug("Saving ConceptoFacturable..." + newConceptoFacturable);

	if (newConceptoFacturable == null) {
	    return null;
	}

	if (newConceptoFacturable.getId() == null) {
	    logger.info("Creating new ConceptoFacturable " + newConceptoFacturable + "...");
	    newConceptoFacturable = entityManager.merge(newConceptoFacturable);
	} else {
	    logger.info("Updating ConceptoFacturable " + newConceptoFacturable + "...");
	    newConceptoFacturable = entityManager.merge(newConceptoFacturable);
	}

	logger.info("New ConceptoFacturable " + newConceptoFacturable.getDescripcion() + " saved with id " + newConceptoFacturable.getId() + ".");
	return newConceptoFacturable;
    }

    @Override
    public void removeConceptoFacturable(ConceptoFacturable conceptosFacturables) {
	logger.debug("Removing ConceptoFacturable: " + conceptosFacturables + " ...");

	logger.info("Removing ConceptoFacturable " + conceptosFacturables + "...");
	entityManager.merge(conceptosFacturables);

	logger.info("Removed ConceptoFacturable " + conceptosFacturables.getCodigo() + " with id " + conceptosFacturables.getId() + ".");
    }

    // @Override
    // public void removeConceptoFacturable(ConceptoFacturable
    // conceptosFacturables) {
    // logger.debug("Removing ConceptoFacturable: " + conceptosFacturables + "
    // ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<ConceptoFacturable> graph =
    // (EntityGraph<ConceptoFacturable>)
    // entityManager.getEntityGraph("ConceptoFacturable");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(conceptosFacturables) ?
    // conceptosFacturables : entityManager.merge(conceptosFacturables));
    // }

}
