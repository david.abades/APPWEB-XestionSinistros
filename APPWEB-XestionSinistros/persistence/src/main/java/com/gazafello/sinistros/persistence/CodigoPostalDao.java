package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.CodigoPostal;

public interface CodigoPostalDao {
    /**
     * Devuelve la lista de todos los codigoPostals
     * 
     * @return
     */
    public List<CodigoPostal> getAllCodigoPostals();

    /**
     * Devuelve un codigoPostal por ID
     * 
     * @param CodigoPostalId
     * @return
     */
    public CodigoPostal getCodigoPostal(Integer CodigoPostalId);

    /**
     * Añade un codigoPostal nuevo
     * 
     * @param newCodigoPostal
     * @return
     */
    public CodigoPostal saveCodigoPostal(CodigoPostal newCodigoPostal);

    /**
     * Elimina un codigoPostal
     * 
     * @param codigoPostal
     * @return
     */
    public void removeCodigoPostal(CodigoPostal codigoPostal);
}
