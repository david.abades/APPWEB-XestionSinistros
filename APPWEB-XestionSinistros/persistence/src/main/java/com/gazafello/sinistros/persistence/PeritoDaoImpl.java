package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Perito;

@Repository
@Transactional
@Profile("dev")
public class PeritoDaoImpl implements PeritoDao {
    private Logger logger = LoggerFactory.getLogger(PeritoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Perito> getAllPeritos() {
	logger.debug("Selecting all peritos stored...");

	Query query = entityManager.createNamedQuery("Perito.findAll", Perito.class);

	logger.debug("Querying database after all peritos => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Perito> companies = query.getResultList();

	logger.debug("List of peritos found! Retrieving " + companies.size() + " perito(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public Perito getPerito(Integer PeritoId) {
	logger.debug("Selecting Perito with id " + PeritoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Perito> graph = (EntityGraph<Perito>) entityManager.getEntityGraph("perito");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Perito comp = entityManager.find(Perito.class, PeritoId, props);

	return comp;
    }

    @Override
    public Perito savePerito(Perito newPerito) {
	logger.debug("Saving Perito..." + newPerito);
	if (newPerito == null) {
	    return null;
	}

	if (newPerito.getId() == null) {
	    logger.info("Creating new Perito " + newPerito + "...");
	    newPerito = entityManager.merge(newPerito);
	} else {
	    logger.info("Updating Perito " + newPerito + "...");
	    newPerito = entityManager.merge(newPerito);
	}

	logger.info("New Perito " + newPerito.getNombre() + " saved with id " + newPerito.getId() + ".");
	return newPerito;
    }

    @Override
    public void removePerito(Perito perito) {
	logger.debug("Removing Perito: " + perito + " ...");

	logger.info("Removing Perito " + perito + "...");
	entityManager.merge(perito);

	logger.info("Removed Perito " + perito.getNombre() + " with id " + perito.getId() + ".");
    }

    // @Override
    // public void removePerito(Perito perito) {
    // logger.debug("Removing Perito: " + perito + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Perito> graph = (EntityGraph<Perito>)
    // entityManager.getEntityGraph("perito");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(perito) ? perito :
    // entityManager.merge(perito));
    // }

}
