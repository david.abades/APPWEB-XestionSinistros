package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Usuario;

public interface UsuarioDao {
    /**
     * Devuelve la lista de todos los usuarios
     * 
     * @return
     */
    public List<Usuario> getAllUsuarios();

    /**
     * Devuelve un usuario por ID
     * 
     * @param UsuarioId
     * @return
     */
    public Usuario getUsuario(Integer UsuarioId);

    /**
     * Añade un usuario nuevo
     * 
     * @param newUsuario
     * @return
     */
    public Usuario saveUsuario(Usuario newUsuario);

    /**
     * Elimina un usuario
     * 
     * @param usuario
     * @return
     */
    public void removeUsuario(Usuario usuario);
}
