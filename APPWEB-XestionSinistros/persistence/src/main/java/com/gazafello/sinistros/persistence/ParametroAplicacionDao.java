package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.ParametroAplicacion;

public interface ParametroAplicacionDao {
    /**
     * Devuelve la lista de todos los parametrosAplicacions
     * 
     * @return
     */
    public List<ParametroAplicacion> getAllParametroAplicacion();

    /**
     * Devuelve un parametrosAplicacion por ID
     * 
     * @param ParametroAplicacionId
     * @return
     */
    public ParametroAplicacion getParametroAplicacion(Integer ParametroAplicacionId);

    /**
     * Añade un parametrosAplicacion nuevo
     * 
     * @param newParametroAplicacion
     * @return
     */
    public ParametroAplicacion saveParametroAplicacion(ParametroAplicacion newParametroAplicacion);

    /**
     * Elimina un parametrosAplicacion
     * 
     * @param parametrosAplicacion
     * @return
     */
    public void removeParametroAplicacion(ParametroAplicacion parametrosAplicacion);
}
