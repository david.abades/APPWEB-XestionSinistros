package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Gremio;

@Repository
@Transactional
@Profile("dev")
public class GremioDaoImpl implements GremioDao {
    private Logger logger = LoggerFactory.getLogger(GremioDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Gremio> getAllGremios() {
	logger.debug("Selecting all Gremios stored...");

	Query query = entityManager.createNamedQuery("Gremio.findAll", Gremio.class);

	logger.debug("Querying database after all Gremios => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Gremio> companies = query.getResultList();

	logger.debug("List of Gremios found! Retrieving " + companies.size() + " Gremio(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public Gremio getGremio(Integer gremioId) {
	logger.debug("Selecting Gremio with id " + gremioId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Gremio> graph = (EntityGraph<Gremio>) entityManager.getEntityGraph("gremio");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Gremio comp = entityManager.find(Gremio.class, gremioId, props);

	return comp;
    }

    @Override
    public Gremio saveGremio(Gremio newGremio) {
	logger.debug("Saving Gremio..." + newGremio);
	if (newGremio == null) {
	    return null;
	}

	if (newGremio.getId() == null) {
	    logger.info("Creating new Gremio " + newGremio + "...");
	    newGremio = entityManager.merge(newGremio);
	} else {
	    logger.info("Updating Gremio " + newGremio + "...");
	    newGremio = entityManager.merge(newGremio);
	}

	logger.info("New Gremio " + newGremio.getNombre() + " saved with id " + newGremio.getId() + ".");
	return newGremio;
    }

    @Override
    public void removeGremio(Gremio gremio) {
	logger.debug("Removing Gremio: " + gremio + " ...");

	logger.info("Removing Gremio " + gremio + "...");
	entityManager.merge(gremio);

	logger.info("Removed Gremio " + gremio.getNombre() + " with id " + gremio.getId() + ".");
    }

    // @Override
    // public void removeGremio(Gremio gremio) {
    // logger.debug("Removing Gremio: " + gremio + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Gremio> graph = (EntityGraph<Gremio>)
    // entityManager.getEntityGraph("gremio");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(gremio) ? gremio :
    // entityManager.merge(gremio));
    // }

}
