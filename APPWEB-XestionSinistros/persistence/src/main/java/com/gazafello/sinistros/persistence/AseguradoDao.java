package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Asegurado;

public interface AseguradoDao {
    /**
     * Devuelve la lista de todos los asegurados
     * 
     * @return
     */
    public List<Asegurado> getAllAsegurados();

    /**
     * Devuelve un asegurado por ID
     * 
     * @param AseguradoId
     * @return
     */
    public Asegurado getAsegurado(Integer AseguradoId);

    /**
     * Añade un asegurado nuevo
     * 
     * @param newAsegurado
     * @return
     */
    public Asegurado saveAsegurado(Asegurado newAsegurado);

    /**
     * Elimina un asegurado
     * 
     * @param asegurado
     * @return
     */
    public void removeAsegurado(Asegurado asegurado);
}
