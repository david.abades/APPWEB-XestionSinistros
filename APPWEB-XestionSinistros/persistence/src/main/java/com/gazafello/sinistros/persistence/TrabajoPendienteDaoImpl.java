package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.TrabajoPendiente;

@Repository
@Transactional
@Profile("dev")
public class TrabajoPendienteDaoImpl implements TrabajoPendienteDao {
    private Logger logger = LoggerFactory.getLogger(TrabajoPendienteDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<TrabajoPendiente> getAllTrabajoPendientes() {
	logger.debug("Selecting all trabajoPendientes stored...");

	Query query = entityManager.createNamedQuery("TrabajoPendiente.findAll", TrabajoPendiente.class);

	logger.debug("Querying database after all trabajoPendientes => " + query.toString());

	@SuppressWarnings("unchecked")
	List<TrabajoPendiente> companies = query.getResultList();

	logger.debug("List of trabajoPendientes found! Retrieving " + companies.size() + " trabajoPendiente(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public TrabajoPendiente getTrabajoPendiente(Integer TrabajoPendienteId) {
	logger.debug("Selecting TrabajoPendiente with id " + TrabajoPendienteId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<TrabajoPendiente> graph = (EntityGraph<TrabajoPendiente>) entityManager.getEntityGraph("trabajoPendiente");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	TrabajoPendiente comp = entityManager.find(TrabajoPendiente.class, TrabajoPendienteId, props);

	return comp;
    }

    @Override
    public TrabajoPendiente saveTrabajoPendiente(TrabajoPendiente newTrabajoPendiente) {
	logger.debug("Saving TrabajoPendiente..." + newTrabajoPendiente);
	if (newTrabajoPendiente == null) {
	    return null;
	}

	if (newTrabajoPendiente.getId() == null) {
	    logger.info("Creating new TrabajoPendiente " + newTrabajoPendiente + "...");
	    newTrabajoPendiente = entityManager.merge(newTrabajoPendiente);
	} else {
	    logger.info("Updating TrabajoPendiente " + newTrabajoPendiente + "...");
	    newTrabajoPendiente = entityManager.merge(newTrabajoPendiente);
	}

	logger.info("New TrabajoPendiente saved with id " + newTrabajoPendiente.getId() + ".");
	return newTrabajoPendiente;
    }

    @Override
    public void removeTrabajoPendiente(TrabajoPendiente trabajoPendiente) {
	logger.debug("Removing TrabajoPendiente: " + trabajoPendiente + " ...");

	logger.info("Removing TrabajoPendiente " + trabajoPendiente + "...");
	entityManager.merge(trabajoPendiente);

	logger.info("Removed TrabajoPendiente with id " + trabajoPendiente.getId() + ".");
    }

    // @Override
    // public void removeTrabajoPendiente(TrabajoPendiente trabajoPendiente) {
    // logger.debug("Removing TrabajoPendiente: " + trabajoPendiente + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<TrabajoPendiente> graph = (EntityGraph<TrabajoPendiente>)
    // entityManager.getEntityGraph("trabajoPendiente");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(trabajoPendiente) ?
    // trabajoPendiente
    // :
    // entityManager.merge(trabajoPendiente));
    // }

}
