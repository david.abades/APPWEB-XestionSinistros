package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.ConceptoFacturable;

public interface ConceptoFacturableDao {
    /**
     * Devuelve la lista de todos los conceptosFacturabless
     * 
     * @return
     */
    public List<ConceptoFacturable> getAllConceptoFacturables();

    /**
     * Devuelve un conceptosFacturables por ID
     * 
     * @param ConceptoFacturableId
     * @return
     */
    public ConceptoFacturable getConceptoFacturable(Integer ConceptoFacturableId);

    /**
     * Añade un conceptosFacturables nuevo
     * 
     * @param newConceptoFacturable
     * @return
     */
    public ConceptoFacturable saveConceptoFacturable(ConceptoFacturable newConceptoFacturable);

    /**
     * Elimina un conceptosFacturables
     * 
     * @param conceptosFacturables
     * @return
     */
    public void removeConceptoFacturable(ConceptoFacturable conceptosFacturables);
}
