package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Expediente;

@Repository
@Transactional
@Profile("dev")
public class ExpedienteDaoImpl implements ExpedienteDao {

    private Logger logger = LoggerFactory.getLogger(ExpedienteDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Expediente> getAllExpedientes() {
	logger.debug("Selecting all expedientes stored...");

	Query query = entityManager.createNamedQuery("Expediente.findAll", Expediente.class);

	logger.debug("Querying database after all expedientes => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Expediente> companies = query.getResultList();

	logger.debug("List of expedientes found! Retrieving " + companies.size() + " expediente(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public Expediente getExpediente(Integer expedienteId) {
	logger.debug("Selecting Expediente with id " + expedienteId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Expediente> graph = (EntityGraph<Expediente>) entityManager.getEntityGraph("expediente");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Expediente comp = entityManager.find(Expediente.class, expedienteId, props);

	return comp;
    }

    @Override
    public Expediente saveExpediente(Expediente newExpediente) {
	logger.debug("Saving Expediente..." + newExpediente);
	if (newExpediente == null) {
	    return null;
	}

	if (newExpediente.getId() == null) {
	    logger.info("Creating new Expediente " + newExpediente + "...");
	    newExpediente = entityManager.merge(newExpediente);
	} else {
	    logger.info("Updating Expediente " + newExpediente + "...");
	    newExpediente = entityManager.merge(newExpediente);
	}

	logger.info("New Expediente " + newExpediente.getNumeroExpediente() + " saved with id " + newExpediente.getId() + ".");
	return newExpediente;
    }

    @Override
    public void removeExpediente(Expediente expediente) {
	logger.debug("Removing Expediente: " + expediente + " ...");

	logger.info("Removing Expediente " + expediente + "...");
	entityManager.merge(expediente);

	logger.info("Removed Expediente " + expediente.getNumeroExpediente() + " with id " + expediente.getId() + ".");
    }

    // @Override
    // public void removeExpediente(Expediente expediente) {
    // logger.debug("Removing Expediente: " + expediente + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Expediente> graph = (EntityGraph<Expediente>)
    // entityManager.getEntityGraph("expediente");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(expediente) ? expediente :
    // entityManager.merge(expediente));
    // }

}
