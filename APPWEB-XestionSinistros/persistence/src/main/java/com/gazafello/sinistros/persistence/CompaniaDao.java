package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Compania;

public interface CompaniaDao {
    /**
     * Devuelve la lista de todas las companias
     * 
     * @return
     */
    public List<Compania> getAllCompanias();

    /**
     * Devuelve una compania por ID
     * 
     * @param CompaniaId
     * @return
     */
    public Compania getCompania(Integer CompaniaId);

    /**
     * Añade una compania nueva
     * 
     * @param newCompania
     * @return
     */
    public Compania saveCompania(Compania newCompania);

    /**
     * Elimina un compania
     * 
     * @param compania
     * @return
     */
    public void removeCompania(Compania compania);
}
