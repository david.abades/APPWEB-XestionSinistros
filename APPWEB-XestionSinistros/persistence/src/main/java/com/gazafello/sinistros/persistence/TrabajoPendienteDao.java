package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.TrabajoPendiente;

public interface TrabajoPendienteDao {
    /**
     * Devuelve la lista de todos los TrabajoPendientes
     * 
     * @return
     */
    public List<TrabajoPendiente> getAllTrabajoPendientes();

    /**
     * Devuelve un TrabajoPendiente por ID
     * 
     * @param trabajoPendienteId
     * @return
     */
    public TrabajoPendiente getTrabajoPendiente(Integer trabajoPendienteId);

    /**
     * Añade un TrabajoPendiente nuevo
     * 
     * @param newTrabajoPendiente
     * @return
     */
    public TrabajoPendiente saveTrabajoPendiente(TrabajoPendiente newTrabajoPendiente);

    /**
     * Elimina un trabajoPendiente
     * 
     * @param trabajoPendiente
     * @return
     */
    public void removeTrabajoPendiente(TrabajoPendiente trabajoPendiente);
}
