package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Estado;

@Repository
@Transactional
@Profile("dev")
public class EstadoDaoImpl implements EstadoDao {
    private Logger logger = LoggerFactory.getLogger(EstadoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Estado> getAllEstados() {
	logger.debug("Selecting all estados stored...");

	Query query = entityManager.createNamedQuery("Estado.findAll", Estado.class);

	logger.debug("Querying database after all estados => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Estado> companies = query.getResultList();

	logger.debug("List of estados found! Retrieving " + companies.size() + " estado(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public Estado getEstado(Integer estadoId) {
	logger.debug("Selecting Estado with id " + estadoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Estado> graph = (EntityGraph<Estado>) entityManager.getEntityGraph("estado");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Estado comp = entityManager.find(Estado.class, estadoId, props);

	return comp;
    }

    @Override
    public Estado saveEstado(Estado newEstado) {
	logger.debug("Saving Estado..." + newEstado);
	if (newEstado == null) {
	    return null;
	}

	if (newEstado.getId() == null) {
	    logger.info("Creating new Estado " + newEstado + "...");

	    entityManager.persist(newEstado);
	} else {
	    logger.info("Updating Estado " + newEstado + "...");
	    entityManager.merge(newEstado);
	}

	logger.info("New Estado " + newEstado.getDescripcion() + " saved with id " + newEstado.getId() + ".");
	return newEstado;
    }

    @Override
    public void removeEstado(Estado estado) {
	logger.debug("Removing Estado: " + estado + " ...");

	logger.info("Removing Estado " + estado + "...");
	entityManager.merge(estado);

	logger.info("Removed Estado " + estado.getDescripcion() + " with id " + estado.getId() + ".");
    }

    // @Override
    // public void removeEstado(Estado estado) {
    // logger.debug("Removing Estado: " + estado + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Estado> graph = (EntityGraph<Estado>)
    // entityManager.getEntityGraph("estado");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(estado) ? estado :
    // entityManager.merge(estado));
    // }

}
