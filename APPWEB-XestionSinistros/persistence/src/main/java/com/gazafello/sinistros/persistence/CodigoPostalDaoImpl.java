package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.CodigoPostal;

@Repository
@Transactional
@Profile("dev")
public class CodigoPostalDaoImpl implements CodigoPostalDao {
    private Logger logger = LoggerFactory.getLogger(CodigoPostalDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CodigoPostal> getAllCodigoPostals() {
	logger.debug("Selecting all codigoPostals stored...");

	Query query = entityManager.createNamedQuery("CodigoPostal.findAll", CodigoPostal.class);

	logger.debug("Querying database after all codigoPostals => " + query.toString());

	@SuppressWarnings("unchecked")
	List<CodigoPostal> codigoPostals = query.getResultList();

	logger.debug("List of codigoPostals found! Retrieving {} codigoPostal(s) : {}.", codigoPostals.size(), codigoPostals.toString());

	return codigoPostals;
    }

    @Transactional(readOnly = true)
    @Override
    public CodigoPostal getCodigoPostal(Integer CodigoPostalId) {
	logger.debug("Selecting CodigoPostal with id " + CodigoPostalId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<CodigoPostal> graph = (EntityGraph<CodigoPostal>) entityManager.getEntityGraph("codigo_postal");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	CodigoPostal comp = entityManager.find(CodigoPostal.class, CodigoPostalId, props);

	return comp;
    }

    @Override
    public CodigoPostal saveCodigoPostal(CodigoPostal newCodigoPostal) {
	logger.debug("Saving CodigoPostal..." + newCodigoPostal);

	if (newCodigoPostal == null) {
	    return null;
	}

	if (newCodigoPostal.getId() == null) {
	    logger.info("Creating new CodigoPostal " + newCodigoPostal + "...");
	    newCodigoPostal = entityManager.merge(newCodigoPostal);
	} else {
	    logger.info("Updating CodigoPostal " + newCodigoPostal + "...");
	    newCodigoPostal = entityManager.merge(newCodigoPostal);
	}

	logger.info("New CodigoPostal " + newCodigoPostal.getCodigo() + " saved with id " + newCodigoPostal.getId() + ".");
	return newCodigoPostal;
    }

    @Override
    public void removeCodigoPostal(CodigoPostal codigoPostal) {
	logger.debug("Removing CodigoPostal: " + codigoPostal + " ...");

	logger.info("Removing CodigoPostal " + codigoPostal + "...");
	entityManager.merge(codigoPostal);

	logger.info("Removed CodigoPostal " + codigoPostal.getCodigo() + " with id " + codigoPostal.getId() + ".");
    }

    // @Override
    // public void removeCodigoPostal(CodigoPostal codigoPostal) {
    // logger.debug("Removing CodigoPostal: " + codigoPostal + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<CodigoPostal> graph = (EntityGraph<CodigoPostal>)
    // entityManager.getEntityGraph("CodigoPostal");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(codigoPostal) ? codigoPostal
    // : entityManager.merge(codigoPostal));
    // }

}
