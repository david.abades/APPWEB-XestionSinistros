package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Expediente;

public interface ExpedienteDao {
    /**
     * Devuelve la lista de todas las expedientes
     * 
     * @return
     */
    public List<Expediente> getAllExpedientes();

    /**
     * Devuelve una expediente por ID
     * 
     * @param ExpedienteId
     * @return
     */
    public Expediente getExpediente(Integer ExpedienteId);

    /**
     * Añade una expediente nueva
     * 
     * @param newExpediente
     * @return
     */
    public Expediente saveExpediente(Expediente newExpediente);

    /**
     * Elimina un expediente
     * 
     * @param expediente
     * @return
     */
    public void removeExpediente(Expediente expediente);
}
