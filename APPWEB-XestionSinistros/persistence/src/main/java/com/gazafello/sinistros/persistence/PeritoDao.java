package com.gazafello.sinistros.persistence;

import java.util.List;

import com.gazafello.sinistros.domain.Perito;

public interface PeritoDao {
    /**
     * Devuelve la lista de todos los Peritos
     * 
     * @return
     */
    public List<Perito> getAllPeritos();

    /**
     * Devuelve un Perito por ID
     * 
     * @param peritoId
     * @return
     */
    public Perito getPerito(Integer peritoId);

    /**
     * Añade un Perito nuevo
     * 
     * @param newPerito
     * @return
     */
    public Perito savePerito(Perito newPerito);

    /**
     * Elimina un perito
     * 
     * @param perito
     * @return
     */
    public void removePerito(Perito perito);
}
