package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Perjudicado;

@Repository
@Transactional
@Profile("dev")
public class PerjudicadoDaoImpl implements PerjudicadoDao {
    private Logger logger = LoggerFactory.getLogger(PerjudicadoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Perjudicado> getAllPerjudicados() {
	logger.debug("Selecting all perjudicados stored...");

	Query query = entityManager.createNamedQuery("Perjudicado.findAll", Perjudicado.class);

	logger.debug("Querying database after all perjudicados => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Perjudicado> perjudicados = query.getResultList();

	logger.debug("List of perjudicados found! Retrieving {} perjudicado(s) : {}.", perjudicados.size(), perjudicados.toString());

	return perjudicados;
    }

    @Transactional(readOnly = true)
    @Override
    public Perjudicado getPerjudicado(Integer PerjudicadoId) {
	logger.debug("Selecting Perjudicado with id " + PerjudicadoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Perjudicado> graph = (EntityGraph<Perjudicado>) entityManager.getEntityGraph("perjudicado");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Perjudicado comp = entityManager.find(Perjudicado.class, PerjudicadoId, props);

	return comp;
    }

    @Override
    public Perjudicado savePerjudicado(Perjudicado newPerjudicado) {
	logger.debug("Saving Perjudicado..." + newPerjudicado);

	if (newPerjudicado == null) {
	    return null;
	}

	if (newPerjudicado.getId() == null) {
	    logger.info("Creating new Perjudicado " + newPerjudicado + "...");
	    newPerjudicado = entityManager.merge(newPerjudicado);
	} else {
	    logger.info("Updating Perjudicado " + newPerjudicado + "...");
	    newPerjudicado = entityManager.merge(newPerjudicado);
	}

	logger.info("New Perjudicado " + newPerjudicado.getNombre() + " saved with id " + newPerjudicado.getId() + ".");
	return newPerjudicado;
    }

    @Override
    public void removePerjudicado(Perjudicado perjudicado) {
	logger.debug("Removing Perjudicado: " + perjudicado + " ...");

	logger.info("Removing Perjudicado " + perjudicado + "...");
	entityManager.merge(perjudicado);

	logger.info("Removed Perjudicado " + perjudicado.getNombre() + " with id " + perjudicado.getId() + ".");
    }

    // @Override
    // public void removePerjudicado(Perjudicado perjudicado) {
    // logger.debug("Removing Perjudicado: " + perjudicado + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Perjudicado> graph = (EntityGraph<Perjudicado>)
    // entityManager.getEntityGraph("Perjudicado");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(perjudicado) ? perjudicado :
    // entityManager.merge(perjudicado));
    // }

}
