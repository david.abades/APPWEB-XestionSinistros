package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Reparador;

@Repository
@Transactional
@Profile("dev")
public class ReparadorDaoImpl implements ReparadorDao {
    private Logger logger = LoggerFactory.getLogger(ReparadorDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Reparador> getAllReparadors() {
	logger.debug("Selecting all reparadors stored...");

	Query query = entityManager.createNamedQuery("Reparador.findAll", Reparador.class);

	logger.debug("Querying database after all reparadors => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Reparador> reparadors = query.getResultList();

	logger.debug("List of reparadors found! Retrieving {} reparador(s) : {}.", reparadors.size(), reparadors.toString());

	return reparadors;
    }

    @Transactional(readOnly = true)
    @Override
    public Reparador getReparador(Integer ReparadorId) {
	logger.debug("Selecting Reparador with id " + ReparadorId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Reparador> graph = (EntityGraph<Reparador>) entityManager.getEntityGraph("reparador");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Reparador comp = entityManager.find(Reparador.class, ReparadorId, props);

	return comp;
    }

    @Override
    public Reparador saveReparador(Reparador newReparador) {
	logger.debug("Saving Reparador..." + newReparador);

	if (newReparador == null) {
	    return null;
	}

	if (newReparador.getId() == null) {
	    logger.info("Creating new Reparador " + newReparador + "...");
	    newReparador = entityManager.merge(newReparador);
	} else {
	    logger.info("Updating Reparador " + newReparador + "...");
	    newReparador = entityManager.merge(newReparador);
	}

	logger.info("New Reparador " + newReparador.getNombre() + " saved with id " + newReparador.getId() + ".");
	return newReparador;
    }

    @Override
    public void removeReparador(Reparador reparador) {
	logger.debug("Removing Reparador: " + reparador + " ...");

	logger.info("Removing Reparador " + reparador + "...");
	entityManager.merge(reparador);

	logger.info("Removed Reparador " + reparador.getNombre() + " with id " + reparador.getId() + ".");
    }

    // @Override
    // public void removeReparador(Reparador reparador) {
    // logger.debug("Removing Reparador: " + reparador + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Reparador> graph = (EntityGraph<Reparador>)
    // entityManager.getEntityGraph("Reparador");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(reparador) ? reparador :
    // entityManager.merge(reparador));
    // }

}
