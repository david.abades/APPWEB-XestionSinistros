package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.ReaperturaExpediente;

@Repository
@Transactional
@Profile("dev")
public class ReaperturaExpedienteDaoImpl implements ReaperturaExpedienteDao {
    private Logger logger = LoggerFactory.getLogger(ReaperturaExpedienteDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ReaperturaExpediente> getAllReaperturaExpedientes() {
	logger.debug("Selecting all reaperturaExpedientes stored...");

	Query query = entityManager.createNamedQuery("ReaperturaExpediente.findAll", ReaperturaExpediente.class);

	logger.debug("Querying database after all reaperturaExpedientes => " + query.toString());

	@SuppressWarnings("unchecked")
	List<ReaperturaExpediente> reaperturaExpedientes = query.getResultList();

	logger.debug("List of reaperturaExpedientes found! Retrieving {} reaperturaExpediente(s) : {}.", reaperturaExpedientes.size(), reaperturaExpedientes.toString());

	return reaperturaExpedientes;
    }

    @Transactional(readOnly = true)
    @Override
    public ReaperturaExpediente getReaperturaExpediente(Integer ReaperturaExpedienteId) {
	logger.debug("Selecting ReaperturaExpediente with id " + ReaperturaExpedienteId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<ReaperturaExpediente> graph = (EntityGraph<ReaperturaExpediente>) entityManager.getEntityGraph("reaperturaExpediente");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	ReaperturaExpediente comp = entityManager.find(ReaperturaExpediente.class, ReaperturaExpedienteId, props);

	return comp;
    }

    @Override
    public ReaperturaExpediente saveReaperturaExpediente(ReaperturaExpediente newReaperturaExpediente) {
	logger.debug("Saving ReaperturaExpediente..." + newReaperturaExpediente);

	if (newReaperturaExpediente == null) {
	    return null;
	}

	if (newReaperturaExpediente.getId() == null) {
	    logger.info("Creating new ReaperturaExpediente " + newReaperturaExpediente + "...");
	    newReaperturaExpediente = entityManager.merge(newReaperturaExpediente);
	} else {
	    logger.info("Updating ReaperturaExpediente " + newReaperturaExpediente + "...");
	    newReaperturaExpediente = entityManager.merge(newReaperturaExpediente);
	}

	logger.info("New ReaperturaExpediente saved with id " + newReaperturaExpediente.getId() + ".");
	return newReaperturaExpediente;
    }

    @Override
    public void removeReaperturaExpediente(ReaperturaExpediente reaperturaExpediente) {
	logger.debug("Removing ReaperturaExpediente: " + reaperturaExpediente + " ...");

	logger.info("Removing ReaperturaExpediente " + reaperturaExpediente + "...");
	entityManager.merge(reaperturaExpediente);

	logger.info("Removed ReaperturaExpediente with id " + reaperturaExpediente.getId() + ".");
    }

    // @Override
    // public void removeReaperturaExpediente(ReaperturaExpediente
    // reaperturaExpediente) {
    // logger.debug("Removing ReaperturaExpediente: " + reaperturaExpediente + "
    // ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<ReaperturaExpediente> graph =
    // (EntityGraph<ReaperturaExpediente>)
    // entityManager.getEntityGraph("ReaperturaExpediente");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(reaperturaExpediente) ?
    // reaperturaExpediente :
    // entityManager.merge(reaperturaExpediente));
    // }

}
