package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Usuario;

@Repository
@Transactional
@Profile("dev")
public class UsuarioDaoImpl implements UsuarioDao {
    private Logger logger = LoggerFactory.getLogger(UsuarioDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Usuario> getAllUsuarios() {
	logger.debug("Selecting all usuarios stored...");

	Query query = entityManager.createNamedQuery("Usuario.findAll", Usuario.class);

	logger.debug("Querying database after all usuarios => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Usuario> usuarios = query.getResultList();

	logger.debug("List of usuarios found! Retrieving {} usuario(s) : {}.", usuarios.size(), usuarios.toString());

	return usuarios;
    }

    @Transactional(readOnly = true)
    @Override
    public Usuario getUsuario(Integer UsuarioId) {
	logger.debug("Selecting Usuario with id " + UsuarioId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Usuario> graph = (EntityGraph<Usuario>) entityManager.getEntityGraph("Usuario");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Usuario comp = entityManager.find(Usuario.class, UsuarioId, props);

	return comp;
    }

    @Override
    public Usuario saveUsuario(Usuario newUsuario) {
	logger.debug("Saving Usuario..." + newUsuario);

	if (newUsuario == null) {
	    return null;
	}

	if (newUsuario.getId() == null) {
	    logger.info("Creating new Usuario " + newUsuario + "...");
	    newUsuario = entityManager.merge(newUsuario);
	} else {
	    logger.info("Updating Usuario " + newUsuario + "...");
	    newUsuario = entityManager.merge(newUsuario);
	}

	logger.info("New Usuario " + newUsuario.getNombre() + " saved with id " + newUsuario.getId() + ".");
	return newUsuario;
    }

    @Override
    public void removeUsuario(Usuario usuario) {
	logger.debug("Removing Usuario: " + usuario + " ...");

	logger.info("Removing Usuario " + usuario + "...");
	entityManager.merge(usuario);

	logger.info("Removed Usuario " + usuario.getNombre() + " with id " + usuario.getId() + ".");
    }

    // @Override
    // public void removeUsuario(Usuario usuario) {
    // logger.debug("Removing Usuario: " + usuario + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Usuario> graph = (EntityGraph<Usuario>)
    // entityManager.getEntityGraph("Usuario");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(usuario) ? usuario :
    // entityManager.merge(usuario));
    // }

}
