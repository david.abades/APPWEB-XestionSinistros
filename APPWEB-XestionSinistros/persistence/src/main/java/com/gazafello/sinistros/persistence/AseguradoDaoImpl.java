package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.Asegurado;

@Repository
@Transactional
@Profile("dev")
public class AseguradoDaoImpl implements AseguradoDao {
    private Logger logger = LoggerFactory.getLogger(AseguradoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Asegurado> getAllAsegurados() {
	logger.debug("Selecting all asegurados stored...");

	Query query = entityManager.createNamedQuery("Asegurado.findAll", Asegurado.class);

	logger.debug("Querying database after all asegurados => " + query.toString());

	@SuppressWarnings("unchecked")
	List<Asegurado> asegurados = query.getResultList();

	logger.debug("List of asegurados found! Retrieving {} asegurado(s) : {}.", asegurados.size(), asegurados.toString());

	return asegurados;
    }

    @Transactional(readOnly = true)
    @Override
    public Asegurado getAsegurado(Integer AseguradoId) {
	logger.debug("Selecting Asegurado with id " + AseguradoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<Asegurado> graph = (EntityGraph<Asegurado>) entityManager.getEntityGraph("asegurado");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	Asegurado comp = entityManager.find(Asegurado.class, AseguradoId, props);

	return comp;
    }

    @Override
    public Asegurado saveAsegurado(Asegurado newAsegurado) {
	logger.debug("Saving Asegurado..." + newAsegurado);

	if (newAsegurado == null) {
	    return null;
	}

	if (newAsegurado.getId() == null) {
	    logger.info("Creating new Asegurado " + newAsegurado + "...");
	    newAsegurado = entityManager.merge(newAsegurado);
	} else {
	    logger.info("Updating Asegurado " + newAsegurado + "...");
	    newAsegurado = entityManager.merge(newAsegurado);
	}

	logger.info("New Asegurado " + newAsegurado.getNombre() + " saved with id " + newAsegurado.getId() + ".");
	return newAsegurado;
    }

    @Override
    public void removeAsegurado(Asegurado asegurado) {
	logger.debug("Removing Asegurado: " + asegurado + " ...");

	logger.info("Removing Asegurado " + asegurado + "...");
	entityManager.merge(asegurado);

	logger.info("Removed Asegurado " + asegurado.getNombre() + " with id " + asegurado.getId() + ".");
    }

    // @Override
    // public void removeAsegurado(Asegurado asegurado) {
    // logger.debug("Removing Asegurado: " + asegurado + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<Asegurado> graph = (EntityGraph<Asegurado>)
    // entityManager.getEntityGraph("Asegurado");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(asegurado) ? asegurado :
    // entityManager.merge(asegurado));
    // }

}
