package com.gazafello.sinistros.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gazafello.sinistros.domain.ParteTrabajo;

@Repository
@Transactional
@Profile("dev")
public class ParteTrabajoDaoImpl implements ParteTrabajoDao {
    private Logger logger = LoggerFactory.getLogger(ParteTrabajoDaoImpl.class);
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    @Transactional(readOnly = true)
    @Override
    public List<ParteTrabajo> getAllParteTrabajos() {
	logger.debug("Selecting all parteTrabajos stored...");

	Query query = entityManager.createNamedQuery("ParteTrabajo.findAll", ParteTrabajo.class);

	logger.debug("Querying database after all parteTrabajos => " + query.toString());

	@SuppressWarnings("unchecked")
	List<ParteTrabajo> companies = query.getResultList();

	logger.debug("List of parteTrabajos found! Retrieving " + companies.size() + " parteTrabajo(s).");

	return companies;
    }

    @Transactional(readOnly = true)
    @Override
    public ParteTrabajo getParteTrabajo(Integer ParteTrabajoId) {
	logger.debug("Selecting ParteTrabajo with id " + ParteTrabajoId + " ...");

	@SuppressWarnings("unchecked")
	EntityGraph<ParteTrabajo> graph = (EntityGraph<ParteTrabajo>) entityManager.getEntityGraph("parteTrabajo");

	Map<String, Object> props = new HashMap<>();
	props.put("javax.persistence.fetchgraph", graph);

	ParteTrabajo comp = entityManager.find(ParteTrabajo.class, ParteTrabajoId, props);

	return comp;
    }

    @Override
    public ParteTrabajo saveParteTrabajo(ParteTrabajo newParteTrabajo) {
	logger.debug("Saving ParteTrabajo..." + newParteTrabajo);
	if (newParteTrabajo == null) {
	    return null;
	}

	if (newParteTrabajo.getId() == null) {
	    logger.info("Creating new ParteTrabajo " + newParteTrabajo + "...");
	    newParteTrabajo = entityManager.merge(newParteTrabajo);
	} else {
	    logger.info("Updating ParteTrabajo " + newParteTrabajo + "...");
	    newParteTrabajo = entityManager.merge(newParteTrabajo);
	}

	logger.info("New ParteTrabajo saved with id " + newParteTrabajo.getId() + ".");
	return newParteTrabajo;
    }

    @Override
    public void removeParteTrabajo(ParteTrabajo parteTrabajo) {
	logger.debug("Removing ParteTrabajo: " + parteTrabajo + " ...");

	logger.info("Removing ParteTrabajo " + parteTrabajo + "...");
	entityManager.merge(parteTrabajo);

	logger.info("Removed ParteTrabajo with id " + parteTrabajo.getId() + ".");
    }

    // @Override
    // public void removeParteTrabajo(ParteTrabajo parteTrabajo) {
    // logger.debug("Removing ParteTrabajo: " + parteTrabajo + " ...");
    //
    // @SuppressWarnings("unchecked")
    // EntityGraph<ParteTrabajo> graph = (EntityGraph<ParteTrabajo>)
    // entityManager.getEntityGraph("parteTrabajo");
    //
    // Map<String, Object> props = new HashMap<>();
    // props.put("javax.persistence.fetchgraph", graph);
    //
    // entityManager.remove(entityManager.contains(parteTrabajo) ? parteTrabajo
    // :
    // entityManager.merge(parteTrabajo));
    // }

}
