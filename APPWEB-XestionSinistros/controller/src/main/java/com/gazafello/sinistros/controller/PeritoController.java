package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Perito;
import com.gazafello.sinistros.services.PeritoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class PeritoController {
    private final Logger logger = LoggerFactory.getLogger(PeritoController.class);
    private PeritoServices peritoServices;

    @Autowired
    public void setPeritoServices(PeritoServices peritoServices) {
	this.peritoServices = peritoServices;
    }

    @RequestMapping(value = "/perito", method = RequestMethod.GET)

    public ResponseEntity<List<Perito>> listCompanies() {
	logger.debug("Listing peritos...");

	List<Perito> comps = peritoServices.getAllPeritos();

	return new ResponseEntity<List<Perito>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/perito/{peritoId}", method = RequestMethod.GET)

    public ResponseEntity<Perito> getPerito(@PathVariable Integer peritoId) {
	logger.debug("Looking for perito " + peritoId + "...");

	Perito comp = peritoServices.getPerito(peritoId);
	return new ResponseEntity<Perito>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/perito/remove", method = RequestMethod.POST)

    public void removePerito(@RequestBody Perito perito) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	perito.setUsuarioBaja(username);
	perito.setFechaBaja(fechaActual);

	logger.debug("Removing for perito " + perito + "...");

	peritoServices.removePerito(perito);
    }

    @RequestMapping(value = "/perito/save", method = RequestMethod.POST)

    public ResponseEntity<Perito> savePerito(@RequestBody Perito perito) {
	if (perito == null) {
	    return new ResponseEntity<Perito>(perito, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (perito.getId() == null) {
	    perito.setUsuarioAlta(username);
	    perito.setFechaAlta(fechaActual);
	} else {
	    perito.setUsuarioModificacion(username);
	    perito.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving perito" + perito + "...");

	perito = peritoServices.savePerito(perito);

	return new ResponseEntity<Perito>(perito, HttpStatus.OK);
    }
}
