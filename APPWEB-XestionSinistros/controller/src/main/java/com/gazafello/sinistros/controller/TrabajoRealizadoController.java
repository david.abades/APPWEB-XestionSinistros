package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.TrabajoRealizado;
import com.gazafello.sinistros.services.TrabajoRealizadoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class TrabajoRealizadoController {
    private final Logger logger = LoggerFactory.getLogger(TrabajoRealizadoController.class);
    private TrabajoRealizadoServices trabajoRealizadoServices;

    @Autowired
    public void setTrabajoRealizadoServices(TrabajoRealizadoServices trabajoRealizadoServices) {
	this.trabajoRealizadoServices = trabajoRealizadoServices;
    }

    @RequestMapping(value = "/trabajoRealizado", method = RequestMethod.GET)

    public ResponseEntity<List<TrabajoRealizado>> listCompanies() {
	logger.debug("Listing trabajoRealizados...");

	List<TrabajoRealizado> comps = trabajoRealizadoServices.getAllTrabajoRealizados();

	return new ResponseEntity<List<TrabajoRealizado>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/trabajoRealizado/{trabajoRealizadoId}", method = RequestMethod.GET)

    public ResponseEntity<TrabajoRealizado> getTrabajoRealizado(@PathVariable Integer trabajoRealizadoId) {
	logger.debug("Looking for trabajoRealizado " + trabajoRealizadoId + "...");

	TrabajoRealizado comp = trabajoRealizadoServices.getTrabajoRealizado(trabajoRealizadoId);
	return new ResponseEntity<TrabajoRealizado>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/trabajoRealizado/remove", method = RequestMethod.POST)

    public void removeTrabajoRealizado(@RequestBody TrabajoRealizado trabajoRealizado) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	trabajoRealizado.setUsuarioBaja(username);
	trabajoRealizado.setFechaBaja(fechaActual);

	logger.debug("Removing for trabajoRealizado " + trabajoRealizado + "...");

	trabajoRealizadoServices.removeTrabajoRealizado(trabajoRealizado);
    }

    @RequestMapping(value = "/trabajoRealizado/save", method = RequestMethod.POST)

    public ResponseEntity<TrabajoRealizado> saveTrabajoRealizado(@RequestBody TrabajoRealizado trabajoRealizado) {
	if (trabajoRealizado == null) {
	    return new ResponseEntity<TrabajoRealizado>(trabajoRealizado, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (trabajoRealizado.getId() == null) {
	    trabajoRealizado.setUsuarioAlta(username);
	    trabajoRealizado.setFechaAlta(fechaActual);
	} else {
	    trabajoRealizado.setUsuarioModificacion(username);
	    trabajoRealizado.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving trabajoRealizado" + trabajoRealizado + "...");

	trabajoRealizado = trabajoRealizadoServices.saveTrabajoRealizado(trabajoRealizado);

	return new ResponseEntity<TrabajoRealizado>(trabajoRealizado, HttpStatus.OK);
    }
}
