package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.TrabajoPendiente;
import com.gazafello.sinistros.services.TrabajoPendienteServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class TrabajoPendienteController {
    private final Logger logger = LoggerFactory.getLogger(TrabajoPendienteController.class);
    private TrabajoPendienteServices trabajoPendienteServices;

    @Autowired
    public void setTrabajoPendienteServices(TrabajoPendienteServices trabajoPendienteServices) {
	this.trabajoPendienteServices = trabajoPendienteServices;
    }

    @RequestMapping(value = "/trabajoPendiente", method = RequestMethod.GET)

    public ResponseEntity<List<TrabajoPendiente>> listCompanies() {
	logger.debug("Listing trabajoPendientes...");

	List<TrabajoPendiente> comps = trabajoPendienteServices.getAllTrabajoPendientes();

	return new ResponseEntity<List<TrabajoPendiente>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/trabajoPendiente/{trabajoPendienteId}", method = RequestMethod.GET)

    public ResponseEntity<TrabajoPendiente> getTrabajoPendiente(@PathVariable Integer trabajoPendienteId) {
	logger.debug("Looking for trabajoPendiente " + trabajoPendienteId + "...");

	TrabajoPendiente comp = trabajoPendienteServices.getTrabajoPendiente(trabajoPendienteId);
	return new ResponseEntity<TrabajoPendiente>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/trabajoPendiente/remove", method = RequestMethod.POST)

    public void removeTrabajoPendiente(@RequestBody TrabajoPendiente trabajoPendiente) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	logger.debug("Removing for trabajoPendiente " + trabajoPendiente + "...");

	trabajoPendienteServices.removeTrabajoPendiente(trabajoPendiente);
    }

    @RequestMapping(value = "/trabajoPendiente/save", method = RequestMethod.POST)

    public ResponseEntity<TrabajoPendiente> saveTrabajoPendiente(@RequestBody TrabajoPendiente trabajoPendiente) {
	if (trabajoPendiente == null) {
	    return new ResponseEntity<TrabajoPendiente>(trabajoPendiente, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	logger.debug("Saving trabajoPendiente" + trabajoPendiente + "...");

	trabajoPendiente = trabajoPendienteServices.saveTrabajoPendiente(trabajoPendiente);

	return new ResponseEntity<TrabajoPendiente>(trabajoPendiente, HttpStatus.OK);
    }
}
