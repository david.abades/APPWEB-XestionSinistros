package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Asegurado;
import com.gazafello.sinistros.services.AseguradoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class AseguradoController {
    private final Logger logger = LoggerFactory.getLogger(AseguradoController.class);
    private AseguradoServices aseguradoServices;

    @Autowired
    public void setAseguradoServices(AseguradoServices aseguradoServices) {
	this.aseguradoServices = aseguradoServices;
    }

    @RequestMapping(value = "/asegurado", method = RequestMethod.GET)

    public ResponseEntity<List<Asegurado>> listCompanies() {
	logger.debug("Listing asegurados...");

	List<Asegurado> comps = aseguradoServices.getAllAsegurados();

	return new ResponseEntity<List<Asegurado>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/asegurado/{aseguradoId}", method = RequestMethod.GET)

    public ResponseEntity<Asegurado> getAsegurado(@PathVariable Integer aseguradoId) {
	logger.debug("Looking for asegurado " + aseguradoId + "...");

	Asegurado comp = aseguradoServices.getAsegurado(aseguradoId);
	return new ResponseEntity<Asegurado>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/asegurado/remove", method = RequestMethod.POST)

    public void removeAsegurado(@RequestBody Asegurado asegurado) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	asegurado.setUsuarioBaja(username);
	asegurado.setFechaBaja(fechaActual);

	logger.debug("Removing for asegurado " + asegurado + "...");

	aseguradoServices.removeAsegurado(asegurado);
    }

    @RequestMapping(value = "/asegurado/save", method = RequestMethod.POST)

    public ResponseEntity<Asegurado> saveAsegurado(@RequestBody Asegurado asegurado) {
	if (asegurado == null) {
	    return new ResponseEntity<Asegurado>(asegurado, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (asegurado.getId() == null) {
	    asegurado.setUsuarioAlta(username);
	    asegurado.setFechaAlta(fechaActual);
	} else {
	    asegurado.setUsuarioModificacion(username);
	    asegurado.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving asegurado" + asegurado + "...");

	asegurado = aseguradoServices.saveAsegurado(asegurado);

	return new ResponseEntity<Asegurado>(asegurado, HttpStatus.OK);
    }
}
