package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Compania;
import com.gazafello.sinistros.services.CompaniaServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class CompaniaController {
    private final Logger logger = LoggerFactory.getLogger(CompaniaController.class);
    private CompaniaServices companiaServices;

    @Autowired
    public void setCompaniaServices(CompaniaServices companiaServices) {
	this.companiaServices = companiaServices;
    }

    @RequestMapping(value = "/compania", method = RequestMethod.GET)

    public ResponseEntity<List<Compania>> listCompanies() {
	logger.debug("Listing companias...");

	List<Compania> comps = companiaServices.getAllCompanias();

	return new ResponseEntity<List<Compania>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/compania/{companiaId}", method = RequestMethod.GET)

    public ResponseEntity<Compania> getCompania(@PathVariable Integer companiaId) {
	logger.debug("Looking for compania " + companiaId + "...");

	Compania comp = companiaServices.getCompania(companiaId);
	return new ResponseEntity<Compania>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/compania/remove", method = RequestMethod.POST)

    public void removeCompania(@RequestBody Compania compania) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	compania.setUsuarioBaja(username);
	compania.setFechaBaja(fechaActual);

	logger.debug("Removing for compania " + compania + "...");

	companiaServices.removeCompania(compania);
    }

    @RequestMapping(value = "/compania/save", method = RequestMethod.POST)

    public ResponseEntity<Compania> saveCompania(@RequestBody Compania compania) {
	if (compania == null) {
	    return new ResponseEntity<Compania>(compania, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (compania.getId() == null) {
	    compania.setUsuarioAlta(username);
	    compania.setFechaAlta(fechaActual);
	} else {
	    compania.setUsuarioModificacion(username);
	    compania.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving compania" + compania + "...");

	compania = companiaServices.saveCompania(compania);

	return new ResponseEntity<Compania>(compania, HttpStatus.OK);
    }
}
