package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Perjudicado;
import com.gazafello.sinistros.services.PerjudicadoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class PerjudicadoController {
    private final Logger logger = LoggerFactory.getLogger(PerjudicadoController.class);
    private PerjudicadoServices perjudicadoServices;

    @Autowired
    public void setPerjudicadoServices(PerjudicadoServices perjudicadoServices) {
	this.perjudicadoServices = perjudicadoServices;
    }

    @RequestMapping(value = "/perjudicado", method = RequestMethod.GET)

    public ResponseEntity<List<Perjudicado>> listCompanies() {
	logger.debug("Listing perjudicados...");

	List<Perjudicado> comps = perjudicadoServices.getAllPerjudicados();

	return new ResponseEntity<List<Perjudicado>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/perjudicado/{perjudicadoId}", method = RequestMethod.GET)

    public ResponseEntity<Perjudicado> getPerjudicado(@PathVariable Integer perjudicadoId) {
	logger.debug("Looking for perjudicado " + perjudicadoId + "...");

	Perjudicado comp = perjudicadoServices.getPerjudicado(perjudicadoId);
	return new ResponseEntity<Perjudicado>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/perjudicado/remove", method = RequestMethod.POST)

    public void removePerjudicado(@RequestBody Perjudicado perjudicado) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	perjudicado.setUsuarioBaja(username);
	perjudicado.setFechaBaja(fechaActual);

	logger.debug("Removing for perjudicado " + perjudicado + "...");

	perjudicadoServices.removePerjudicado(perjudicado);
    }

    @RequestMapping(value = "/perjudicado/save", method = RequestMethod.POST)

    public ResponseEntity<Perjudicado> savePerjudicado(@RequestBody Perjudicado perjudicado) {
	if (perjudicado == null) {
	    return new ResponseEntity<Perjudicado>(perjudicado, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (perjudicado.getId() == null) {
	    perjudicado.setUsuarioAlta(username);
	    perjudicado.setFechaAlta(fechaActual);
	} else {
	    perjudicado.setUsuarioModificacion(username);
	    perjudicado.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving perjudicado" + perjudicado + "...");

	perjudicado = perjudicadoServices.savePerjudicado(perjudicado);

	return new ResponseEntity<Perjudicado>(perjudicado, HttpStatus.OK);

    }
}
