package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.ConceptoFacturable;
import com.gazafello.sinistros.services.ConceptoFacturableServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ConceptoFacturableController {
    private final Logger logger = LoggerFactory.getLogger(ConceptoFacturableController.class);
    private ConceptoFacturableServices conceptoFacturableServices;

    @Autowired
    public void setConceptoFacturableServices(ConceptoFacturableServices conceptoFacturableServices) {
	this.conceptoFacturableServices = conceptoFacturableServices;
    }

    @RequestMapping(value = "/conceptoFacturable", method = RequestMethod.GET)

    public ResponseEntity<List<ConceptoFacturable>> listCompanies() {
	logger.debug("Listing conceptoFacturables...");

	List<ConceptoFacturable> comps = conceptoFacturableServices.getAllConceptoFacturables();

	return new ResponseEntity<List<ConceptoFacturable>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/conceptoFacturable/{conceptoFacturableId}", method = RequestMethod.GET)

    public ResponseEntity<ConceptoFacturable> getConceptoFacturable(@PathVariable Integer conceptoFacturableId) {
	logger.debug("Looking for conceptoFacturable " + conceptoFacturableId + "...");

	ConceptoFacturable comp = conceptoFacturableServices.getConceptoFacturable(conceptoFacturableId);
	return new ResponseEntity<ConceptoFacturable>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/conceptoFacturable/remove", method = RequestMethod.POST)

    public void removeConceptoFacturable(@RequestBody ConceptoFacturable conceptoFacturable) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	conceptoFacturable.setUsuarioBaja(username);
	conceptoFacturable.setFechaBaja(fechaActual);

	logger.debug("Removing for conceptoFacturable " + conceptoFacturable + "...");

	conceptoFacturableServices.removeConceptoFacturable(conceptoFacturable);
    }

    @RequestMapping(value = "/conceptoFacturable/save", method = RequestMethod.POST)

    public ResponseEntity<ConceptoFacturable> saveConceptoFacturable(@RequestBody ConceptoFacturable conceptoFacturable) {
	if (conceptoFacturable == null) {
	    return new ResponseEntity<ConceptoFacturable>(conceptoFacturable, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (conceptoFacturable.getId() == null) {
	    conceptoFacturable.setUsuarioAlta(username);
	    conceptoFacturable.setFechaAlta(fechaActual);
	} else {
	    conceptoFacturable.setUsuarioModificacion(username);
	    conceptoFacturable.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving conceptoFacturable" + conceptoFacturable + "...");

	conceptoFacturable = conceptoFacturableServices.saveConceptoFacturable(conceptoFacturable);

	return new ResponseEntity<ConceptoFacturable>(conceptoFacturable, HttpStatus.OK);
    }
}
