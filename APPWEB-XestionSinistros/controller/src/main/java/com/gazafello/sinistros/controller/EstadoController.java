package com.gazafello.sinistros.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Estado;
import com.gazafello.sinistros.services.EstadoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class EstadoController {
    private final Logger logger = LoggerFactory.getLogger(EstadoController.class);
    private EstadoServices estadoServices;

    @Autowired
    public void setEstadoServices(EstadoServices estadoServices) {
	this.estadoServices = estadoServices;
    }

    @RequestMapping(value = "/estado", method = RequestMethod.GET)
    // @PreAuthorize("#oauth2.isUser() and #oauth2.clientHasRole('ROLE_ADMIN')
    // and #oauth2.hasScope('read')")
    public ResponseEntity<List<Estado>> listCompanies() {
	logger.debug("Listing estados...");

	List<Estado> comps = estadoServices.getAllEstados();

	return new ResponseEntity<List<Estado>>(comps, HttpStatus.OK);
    }
}
