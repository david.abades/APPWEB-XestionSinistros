package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Expediente;
import com.gazafello.sinistros.services.ExpedienteServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ExpedienteController {
    private final Logger logger = LoggerFactory.getLogger(ExpedienteController.class);
    private ExpedienteServices expedienteServices;

    @Autowired
    public void setExpedienteServices(ExpedienteServices expedienteServices) {
	this.expedienteServices = expedienteServices;
    }

    @RequestMapping(value = "/expediente", method = RequestMethod.GET)
    public ResponseEntity<List<Expediente>> listCompanies() {
	logger.debug("Listing expedientes...");

	List<Expediente> comps = expedienteServices.getAllExpedientes();

	return new ResponseEntity<List<Expediente>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/expediente/{expedienteId}", method = RequestMethod.GET)
    public ResponseEntity<Expediente> getExpediente(@PathVariable Integer expedienteId) {
	logger.debug("Looking for expediente " + expedienteId + "...");

	Expediente comp = expedienteServices.getExpediente(expedienteId);
	return new ResponseEntity<Expediente>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/expediente/remove", method = RequestMethod.POST)
    public void removeExpediente(@RequestBody Expediente expediente) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	expediente.setUsuarioBaja(username);
	expediente.setFechaBaja(fechaActual);

	logger.debug("Removing for expediente " + expediente + "...");

	expedienteServices.removeExpediente(expediente);
    }

    @RequestMapping(value = "/expediente/save", method = RequestMethod.POST)
    public ResponseEntity<Expediente> saveExpediente(@RequestBody Expediente expediente) {
	if (expediente == null) {
	    return new ResponseEntity<Expediente>(expediente, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (expediente.getId() == null) {
	    expediente.setUsuarioAlta(username);
	    expediente.setFechaAlta(fechaActual);
	} else {
	    expediente.setUsuarioModificacion(username);
	    expediente.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving expediente" + expediente + "...");

	expediente = expedienteServices.saveExpediente(expediente);

	return new ResponseEntity<Expediente>(expediente, HttpStatus.OK);
    }
}
