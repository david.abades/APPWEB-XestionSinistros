package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.ParteTrabajo;
import com.gazafello.sinistros.services.ParteTrabajoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ParteTrabajoController {
    private final Logger logger = LoggerFactory.getLogger(ParteTrabajoController.class);
    private ParteTrabajoServices parteTrabajoServices;

    @Autowired
    public void setParteTrabajoServices(ParteTrabajoServices parteTrabajoServices) {
	this.parteTrabajoServices = parteTrabajoServices;
    }

    @RequestMapping(value = "/parteTrabajo", method = RequestMethod.GET)

    public ResponseEntity<List<ParteTrabajo>> listCompanies() {
	logger.debug("Listing parteTrabajos...");

	List<ParteTrabajo> comps = parteTrabajoServices.getAllParteTrabajos();

	return new ResponseEntity<List<ParteTrabajo>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/parteTrabajo/{parteTrabajoId}", method = RequestMethod.GET)

    public ResponseEntity<ParteTrabajo> getParteTrabajo(@PathVariable Integer parteTrabajoId) {
	logger.debug("Looking for parteTrabajo " + parteTrabajoId + "...");

	ParteTrabajo comp = parteTrabajoServices.getParteTrabajo(parteTrabajoId);
	return new ResponseEntity<ParteTrabajo>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/parteTrabajo/remove", method = RequestMethod.POST)

    public void removeParteTrabajo(@RequestBody ParteTrabajo parteTrabajo) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	parteTrabajo.setUsuarioBaja(username);
	parteTrabajo.setFechaBaja(fechaActual);

	logger.debug("Removing for parteTrabajo " + parteTrabajo + "...");

	parteTrabajoServices.removeParteTrabajo(parteTrabajo);
    }

    @RequestMapping(value = "/parteTrabajo/save", method = RequestMethod.POST)

    public ResponseEntity<ParteTrabajo> saveParteTrabajo(@RequestBody ParteTrabajo parteTrabajo) {
	if (parteTrabajo == null) {
	    return new ResponseEntity<ParteTrabajo>(parteTrabajo, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (parteTrabajo.getId() == null) {
	    parteTrabajo.setUsuarioAlta(username);
	    parteTrabajo.setFechaAlta(fechaActual);
	} else {
	    parteTrabajo.setUsuarioModificacion(username);
	    parteTrabajo.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving parteTrabajo" + parteTrabajo + "...");

	parteTrabajo = parteTrabajoServices.saveParteTrabajo(parteTrabajo);

	return new ResponseEntity<ParteTrabajo>(parteTrabajo, HttpStatus.OK);
    }
}
