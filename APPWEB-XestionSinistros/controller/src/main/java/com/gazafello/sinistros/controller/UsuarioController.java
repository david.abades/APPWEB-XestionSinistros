package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Usuario;
import com.gazafello.sinistros.services.UsuarioServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class UsuarioController {
    private final Logger logger = LoggerFactory.getLogger(UsuarioController.class);
    private UsuarioServices usuarioServices;

    @Autowired
    public void setUsuarioServices(UsuarioServices usuarioServices) {
	this.usuarioServices = usuarioServices;
    }

    @RequestMapping(value = "/usuario", method = RequestMethod.GET)

    public ResponseEntity<List<Usuario>> listUsuarios() {
	logger.debug("Listing usuario...");

	List<Usuario> comps = usuarioServices.getAllUsuarios();

	return new ResponseEntity<List<Usuario>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/usuario/{usuarioId}", method = RequestMethod.GET)

    public ResponseEntity<Usuario> getUsuario(@PathVariable Integer usuarioId) {
	logger.debug("Looking for usuario " + usuarioId + "...");

	Usuario usuario = usuarioServices.getUsuario(usuarioId);
	return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
    }

    @RequestMapping(value = "/usuario/remove", method = RequestMethod.POST)

    public void removeUsuario(@RequestBody Usuario usuario) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	usuario.setUsuarioBaja(username);
	usuario.setFechaBaja(fechaActual);

	logger.debug("Removing for usuario " + usuario + "...");

	usuarioServices.removeUsuario(usuario);
    }

    @RequestMapping(value = "/usuario/save", method = RequestMethod.POST)

    public ResponseEntity<Usuario> saveUsuario(@RequestBody Usuario usuario) {
	if (usuario == null) {
	    return new ResponseEntity<Usuario>(usuario, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (usuario.getId() == null) {
	    usuario.setUsuarioAlta(username);
	    usuario.setFechaAlta(fechaActual);
	} else {
	    usuario.setUsuarioModificacion(username);
	    usuario.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving usuario" + usuario + "...");

	usuario = usuarioServices.saveUsuario(usuario);

	return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
    }
}
