package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.ReaperturaExpediente;
import com.gazafello.sinistros.services.ReaperturaExpedienteServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ReaperturaExpedienteController {
    private final Logger logger = LoggerFactory.getLogger(ReaperturaExpedienteController.class);
    private ReaperturaExpedienteServices reaperturaExpedienteServices;

    @Autowired
    public void setReaperturaExpedienteServices(ReaperturaExpedienteServices reaperturaExpedienteServices) {
	this.reaperturaExpedienteServices = reaperturaExpedienteServices;
    }

    @RequestMapping(value = "/reaperturaExpediente", method = RequestMethod.GET)

    public ResponseEntity<List<ReaperturaExpediente>> listCompanies() {
	logger.debug("Listing reaperturaExpedientes...");

	List<ReaperturaExpediente> comps = reaperturaExpedienteServices.getAllReaperturaExpedientes();

	return new ResponseEntity<List<ReaperturaExpediente>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/reaperturaExpediente/{reaperturaExpedienteId}", method = RequestMethod.GET)

    public ResponseEntity<ReaperturaExpediente> getReaperturaExpediente(@PathVariable Integer reaperturaExpedienteId) {
	logger.debug("Looking for reaperturaExpediente " + reaperturaExpedienteId + "...");

	ReaperturaExpediente comp = reaperturaExpedienteServices.getReaperturaExpediente(reaperturaExpedienteId);
	return new ResponseEntity<ReaperturaExpediente>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/reaperturaExpediente/remove", method = RequestMethod.POST)

    public void removeReaperturaExpediente(@RequestBody ReaperturaExpediente reaperturaExpediente) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	reaperturaExpediente.setUsuarioBaja(username);
	reaperturaExpediente.setFechaBaja(fechaActual);

	logger.debug("Removing for reaperturaExpediente " + reaperturaExpediente + "...");

	reaperturaExpedienteServices.removeReaperturaExpediente(reaperturaExpediente);
    }

    @RequestMapping(value = "/reaperturaExpediente/save", method = RequestMethod.POST)

    public ResponseEntity<ReaperturaExpediente> saveReaperturaExpediente(@RequestBody ReaperturaExpediente reaperturaExpediente) {
	if (reaperturaExpediente == null) {
	    return new ResponseEntity<ReaperturaExpediente>(reaperturaExpediente, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (reaperturaExpediente.getId() == null) {
	    reaperturaExpediente.setUsuarioAlta(username);
	    reaperturaExpediente.setFechaAlta(fechaActual);
	} else {
	    reaperturaExpediente.setUsuarioModificacion(username);
	    reaperturaExpediente.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving reaperturaExpediente" + reaperturaExpediente + "...");

	reaperturaExpediente = reaperturaExpedienteServices.saveReaperturaExpediente(reaperturaExpediente);

	return new ResponseEntity<ReaperturaExpediente>(reaperturaExpediente, HttpStatus.OK);

    }
}
