package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.ParametroAplicacion;
import com.gazafello.sinistros.services.ParametroAplicacionServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ParametroAplicacionController {
    private final Logger logger = LoggerFactory.getLogger(ParametroAplicacionController.class);
    private ParametroAplicacionServices parametroAplicacionServices;

    @Autowired
    public void setParametroAplicacionServices(ParametroAplicacionServices parametroAplicacionServices) {
	this.parametroAplicacionServices = parametroAplicacionServices;
    }

    @RequestMapping(value = "/parametroAplicacion", method = RequestMethod.GET)

    public ResponseEntity<List<ParametroAplicacion>> listCompanies() {
	logger.debug("Listing parametroAplicacions...");

	List<ParametroAplicacion> comps = parametroAplicacionServices.getAllParametroAplicacion();

	return new ResponseEntity<List<ParametroAplicacion>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/parametroAplicacion/{parametroAplicacionId}", method = RequestMethod.GET)

    public ResponseEntity<ParametroAplicacion> getParametroAplicacion(@PathVariable Integer parametroAplicacionId) {
	logger.debug("Looking for parametroAplicacion " + parametroAplicacionId + "...");

	ParametroAplicacion comp = parametroAplicacionServices.getParametroAplicacion(parametroAplicacionId);
	return new ResponseEntity<ParametroAplicacion>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/parametroAplicacion/remove", method = RequestMethod.POST)

    public void removeParametroAplicacion(@RequestBody ParametroAplicacion parametroAplicacion) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	parametroAplicacion.setUsuarioBaja(username);
	parametroAplicacion.setFechaBaja(fechaActual);

	logger.debug("Removing for parametroAplicacion " + parametroAplicacion + "...");

	parametroAplicacionServices.removeParametroAplicacion(parametroAplicacion);
    }

    @RequestMapping(value = "/parametroAplicacion/save", method = RequestMethod.POST)

    public ResponseEntity<ParametroAplicacion> saveParametroAplicacion(@RequestBody ParametroAplicacion parametroAplicacion) {
	if (parametroAplicacion == null) {
	    return new ResponseEntity<ParametroAplicacion>(parametroAplicacion, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (parametroAplicacion.getId() == null) {
	    parametroAplicacion.setUsuarioAlta(username);
	    parametroAplicacion.setFechaAlta(fechaActual);
	} else {
	    parametroAplicacion.setUsuarioModificacion(username);
	    parametroAplicacion.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving parametroAplicacion" + parametroAplicacion + "...");

	parametroAplicacion = parametroAplicacionServices.saveParametroAplicacion(parametroAplicacion);

	return new ResponseEntity<ParametroAplicacion>(parametroAplicacion, HttpStatus.OK);
    }
}
