package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Reparador;
import com.gazafello.sinistros.services.ReparadorServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class ReparadorController {
    private final Logger logger = LoggerFactory.getLogger(ReparadorController.class);
    private ReparadorServices reparadorServices;

    @Autowired
    public void setReparadorServices(ReparadorServices reparadorServices) {
	this.reparadorServices = reparadorServices;
    }

    @RequestMapping(value = "/reparador", method = RequestMethod.GET)

    public ResponseEntity<List<Reparador>> listCompanies() {
	logger.debug("Listing reparadors...");

	List<Reparador> comps = reparadorServices.getAllReparadors();

	return new ResponseEntity<List<Reparador>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/reparador/{reparadorId}", method = RequestMethod.GET)

    public ResponseEntity<Reparador> getReparador(@PathVariable Integer reparadorId) {
	logger.debug("Looking for reparador " + reparadorId + "...");

	Reparador comp = reparadorServices.getReparador(reparadorId);
	return new ResponseEntity<Reparador>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/reparador/remove", method = RequestMethod.POST)

    public void removeReparador(@RequestBody Reparador reparador) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	reparador.setUsuarioBaja(username);
	reparador.setFechaBaja(fechaActual);

	logger.debug("Removing for reparador " + reparador + "...");

	reparadorServices.removeReparador(reparador);
    }

    @RequestMapping(value = "/reparador/save", method = RequestMethod.POST)

    public ResponseEntity<Reparador> saveReparador(@RequestBody Reparador reparador) {
	if (reparador == null) {
	    return new ResponseEntity<Reparador>(reparador, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (reparador.getId() == null) {
	    reparador.setUsuarioAlta(username);
	    reparador.setFechaAlta(fechaActual);
	} else {
	    reparador.setUsuarioModificacion(username);
	    reparador.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving reparador" + reparador + "...");

	reparador = reparadorServices.saveReparador(reparador);

	return new ResponseEntity<Reparador>(reparador, HttpStatus.OK);

    }
}
