package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.CodigoPostal;
import com.gazafello.sinistros.services.CodigoPostalServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class CodigoPostalController {
    private final Logger logger = LoggerFactory.getLogger(CodigoPostalController.class);
    private CodigoPostalServices codigoPostalServices;

    @Autowired
    public void setCodigoPostalServices(CodigoPostalServices codigoPostalServices) {
	this.codigoPostalServices = codigoPostalServices;
    }

    @RequestMapping(value = "/codigoPostal", method = RequestMethod.GET)

    public ResponseEntity<List<CodigoPostal>> listCompanies() {
	logger.debug("Listing codigosPostales...");

	List<CodigoPostal> comps = codigoPostalServices.getAllCodigoPostals();

	return new ResponseEntity<List<CodigoPostal>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/codigoPostal/{codigoPostalId}", method = RequestMethod.GET)

    public ResponseEntity<CodigoPostal> getCodigoPostal(@PathVariable Integer codigoPostalId) {
	logger.debug("Looking for codigoPostal " + codigoPostalId + "...");

	CodigoPostal comp = codigoPostalServices.getCodigoPostal(codigoPostalId);
	return new ResponseEntity<CodigoPostal>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/codigoPostal/remove", method = RequestMethod.POST)

    public void removeCodigoPostal(@RequestBody CodigoPostal codigoPostal) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	codigoPostal.setUsuarioBaja(username);
	codigoPostal.setFechaBaja(fechaActual);

	logger.debug("Removing for codigoPostal " + codigoPostal + "...");

	codigoPostalServices.removeCodigoPostal(codigoPostal);
    }

    @RequestMapping(value = "/codigoPostal/save", method = RequestMethod.POST)

    public ResponseEntity<CodigoPostal> saveCodigoPostal(@RequestBody CodigoPostal codigoPostal) {
	if (codigoPostal == null) {
	    return new ResponseEntity<CodigoPostal>(codigoPostal, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (codigoPostal.getId() == null) {
	    codigoPostal.setUsuarioAlta(username);
	    codigoPostal.setFechaAlta(fechaActual);
	} else {
	    codigoPostal.setUsuarioModificacion(username);
	    codigoPostal.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving codigoPostal" + codigoPostal + "...");

	codigoPostal = codigoPostalServices.saveCodigoPostal(codigoPostal);

	return new ResponseEntity<CodigoPostal>(codigoPostal, HttpStatus.OK);
    }
}
