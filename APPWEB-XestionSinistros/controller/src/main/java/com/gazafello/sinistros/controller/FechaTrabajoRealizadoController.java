package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.FechaTrabajoRealizado;
import com.gazafello.sinistros.services.FechaTrabajoRealizadoServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class FechaTrabajoRealizadoController {
    private final Logger logger = LoggerFactory.getLogger(FechaTrabajoRealizadoController.class);
    private FechaTrabajoRealizadoServices fechaTrabajoRealizadoServices;

    @Autowired
    public void setFechaTrabajoRealizadoServices(FechaTrabajoRealizadoServices fechaTrabajoRealizadoServices) {
	this.fechaTrabajoRealizadoServices = fechaTrabajoRealizadoServices;
    }

    @RequestMapping(value = "/fechaTrabajoRealizado", method = RequestMethod.GET)

    public ResponseEntity<List<FechaTrabajoRealizado>> listCompanies() {
	logger.debug("Listing fechaTrabajoRealizados...");

	List<FechaTrabajoRealizado> comps = fechaTrabajoRealizadoServices.getAllFechaTrabajoRealizados();

	return new ResponseEntity<List<FechaTrabajoRealizado>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/fechaTrabajoRealizado/{fechaTrabajoRealizadoId}", method = RequestMethod.GET)

    public ResponseEntity<FechaTrabajoRealizado> getFechaTrabajoRealizado(@PathVariable Integer fechaTrabajoRealizadoId) {
	logger.debug("Looking for fechaTrabajoRealizado " + fechaTrabajoRealizadoId + "...");

	FechaTrabajoRealizado comp = fechaTrabajoRealizadoServices.getFechaTrabajoRealizado(fechaTrabajoRealizadoId);
	return new ResponseEntity<FechaTrabajoRealizado>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/fechaTrabajoRealizado/remove", method = RequestMethod.POST)

    public void removeFechaTrabajoRealizado(@RequestBody FechaTrabajoRealizado fechaTrabajoRealizado) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	logger.debug("Removing for fechaTrabajoRealizado " + fechaTrabajoRealizado + "...");

	fechaTrabajoRealizadoServices.removeFechaTrabajoRealizado(fechaTrabajoRealizado);
    }

    @RequestMapping(value = "/fechaTrabajoRealizado/save", method = RequestMethod.POST)

    public ResponseEntity<FechaTrabajoRealizado> saveFechaTrabajoRealizado(@RequestBody FechaTrabajoRealizado fechaTrabajoRealizado) {
	if (fechaTrabajoRealizado == null) {
	    return new ResponseEntity<FechaTrabajoRealizado>(fechaTrabajoRealizado, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	logger.debug("Saving fechaTrabajoRealizado" + fechaTrabajoRealizado + "...");

	fechaTrabajoRealizado = fechaTrabajoRealizadoServices.saveFechaTrabajoRealizado(fechaTrabajoRealizado);

	return new ResponseEntity<FechaTrabajoRealizado>(fechaTrabajoRealizado, HttpStatus.OK);
    }
}
