package com.gazafello.sinistros.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gazafello.sinistros.domain.Gremio;
import com.gazafello.sinistros.services.GremioServices;

@RestController
@RequestMapping("/services")
@Profile("dev")
public class GremioController {
    private final Logger logger = LoggerFactory.getLogger(GremioController.class);
    private GremioServices gremioServices;

    @Autowired
    public void setGremioServices(GremioServices gremioServices) {
	this.gremioServices = gremioServices;
    }

    @RequestMapping(value = "/gremio", method = RequestMethod.GET)

    public ResponseEntity<List<Gremio>> listCompanies() {
	logger.debug("Listing gremios...");

	List<Gremio> comps = gremioServices.getAllGremios();

	return new ResponseEntity<List<Gremio>>(comps, HttpStatus.OK);
    }

    @RequestMapping(value = "/gremio/{gremioId}", method = RequestMethod.GET)

    public ResponseEntity<Gremio> getGremio(@PathVariable Integer gremioId) {
	logger.debug("Looking for gremio " + gremioId + "...");

	Gremio comp = gremioServices.getGremio(gremioId);
	return new ResponseEntity<Gremio>(comp, HttpStatus.OK);
    }

    @RequestMapping(value = "/gremio/remove", method = RequestMethod.POST)

    public void removeGremio(@RequestBody Gremio gremio) {

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	gremio.setUsuarioBaja(username);
	gremio.setFechaBaja(fechaActual);

	logger.debug("Removing for gremio " + gremio + "...");

	gremioServices.removeGremio(gremio);
    }

    @RequestMapping(value = "/gremio/save", method = RequestMethod.POST)

    public ResponseEntity<Gremio> saveGremio(@RequestBody Gremio gremio) {
	if (gremio == null) {
	    return new ResponseEntity<Gremio>(gremio, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	final String username = userDetails.getUsername();
	final Date fechaActual = new Date();

	if (gremio.getId() == null) {
	    gremio.setUsuarioAlta(username);
	    gremio.setFechaAlta(fechaActual);
	} else {
	    gremio.setUsuarioModificacion(username);
	    gremio.setFechaModificacion(fechaActual);
	}

	logger.debug("Saving gremio" + gremio + "...");

	gremio = gremioServices.saveGremio(gremio);

	return new ResponseEntity<Gremio>(gremio, HttpStatus.OK);
    }
}
