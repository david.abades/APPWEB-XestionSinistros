sinistrosApp.controller('expedienteController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('aseguradoController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));

	angular.extend($controller('peritoController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));

	angular.extend($controller('parteTrabajoController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));

	angular.extend($controller('perjudicadoController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));

	angular.extend($controller('trabajoRealizadoController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));
	
	$scope.seleccionarMenu('expedientes');
	
	$scope.isNewExpediente = false;
	$scope.expedienteForm = null;
	
	$scope.textPeritoKeyDown = function($event) {
		$event.stopPropagation();
	};
	
	$scope.textAseguradoKeyDown = function($event) {
		$event.stopPropagation();
	};
	
	$http.get('/sinistros/services/expediente')
	.success(function (data, status, headers, config){
		$scope.expedientes = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo los expedientes. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	$http.get('/sinistros/services/compania')
	.success(function (data, status, headers, config){
		$scope.companias = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo las compa\u00F1\u00EDas. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	$http.get('/sinistros/services/perito')
	.success(function (data, status, headers, config){
		$scope.peritos = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo los peritos. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	$http.get('/sinistros/services/estado')
	.success(function (data, status, headers, config){
		$scope.estados = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo los estados. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all details of the expediente with the given id in the side panel. 
	$scope.expedienteDetails = function(id){
		$http.get('/sinistros/services/expediente/'+ id)
		.success(function (data, status, headers, config){
			data.fechaInicial  = data.fechaInicial  === null ? null : new Date(data.fechaInicial);
	        data.fechaCreacion = data.fechaCreacion === null ? null : new Date(data.fechaCreacion);
			$scope.expediente = data;
		    $scope.dialogAction = 'Modificar expediente: ' + data.nombre;
			
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando el expediente. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Expediente cargado correctamente.');
	};
	

	
	//ajax call to get all details of the expediente with the given id in the side panel. 
	$scope.removeExpediente = function(expediente){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating expediente, this condition ensures getting the index position for later update in the array of expedientes
	    if(expediente.id != null){
	      for(var i = 0; i < $scope.expedientes.length; i++){
	        if($scope.expedientes[i].id == expediente.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/expediente/remove', expediente)
	    .success(function (data, status, headers, config){
	      $scope.expedientes.splice(index, 1);
	      $scope.clearExpediente();
	      
	      $scope.showToast('Expediente eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Expediente no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the expediente object to allow creation of a new one.
	$scope.newExpediente = function(){
		
		$scope.clearExpediente();
	    
	    $scope.dialogAction = 'Nuevo expediente';
	    
	    $scope.isNewExpediente = true;
	};
	
	$scope.clearExpediente = function(){
		$scope.expediente = new Object();
		$scope.expediente.urgente = 'N';
		if ($scope.expedienteForm != null) {
			$scope.expedienteForm.$setPristine();
			$scope.expedienteForm.$setUntouched();
		}
		$scope.isNewExpediente = false;
	}
	
	
	//save the given expediente and all changes applied to that in the repository of this application.
	$scope.saveExpediente = function(expediente){
		var index = -1;
		
		//if updating expediente, this condition ensures getting the index position for later update in the array of expedientes
		if(expediente.id != null){
			for(var i = 0; i < $scope.expedientes.length; i++){
				if($scope.expedientes[i].id == expediente.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/expediente/save', expediente)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created expediente to the list of expedientes. 
					$scope.expedientes.push(data);
				} else { //save the updated expediente in the list of expedientes.
					$scope.expedientes[index] = data;			
				}
				$scope.clearExpediente();
				$mdDialog.hide();
				
				$scope.showToast('Expediente guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Expediente no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
		
	/**
	 * Filtro de busqueda asegurados
	 */
	$scope.aseguradosExpedienteFilter = function(asegurado) {
		var isMatch = false;
	      
		if ($scope.textAseguradoExpedienteFilter) {
			var parts = $scope.textAseguradoExpedienteFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(asegurado.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
		
	/**
	 * Filtro de busqueda peritos
	 */
	$scope.peritosExpedienteFilter = function(perito) {
		var isMatch = false;
	      
		if ($scope.textPeritoExpedienteFilter) {
			var parts = $scope.textPeritoExpedienteFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(perito.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
		
	/**
	 * Filtro de busqueda
	 */
	$scope.expedienteFilter = function(expediente) {
		var isMatch = false;
	      
		if ($scope.textExpedienteFilter) {
			var parts = $scope.textExpedienteFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(expediente.numeroExpediente)
						|| new RegExp(part.toLowerCase()).test(expediente.estado);
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
});