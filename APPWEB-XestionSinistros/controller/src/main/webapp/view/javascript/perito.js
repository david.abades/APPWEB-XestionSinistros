sinistrosApp.controller('peritoController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('peritos');
	
    $scope.selectedCompaniasId = [];
    $scope.selectedCompanias = [];
    
    $scope.peritoForm = null
	
	//ajax call to get all peritos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/perito')
		.success(function (data, status, headers, config){
			$scope.peritos = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las peritos. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all peritos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/compania')
		.success(function (data, status, headers, config){
			$scope.companias = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las compa\u00F1\u00EDas. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	
	//ajax call to get all details of the perito with the given id in the side panel. 
	$scope.peritoDetails = function(id){
		$http.get('/sinistros/services/perito/'+ id)
		.success(function (data, status, headers, config){
			$scope.perito = data;
		    $scope.peritoDialogAction = 'Modificar perito: ' + data.nombre;
		    
		    angular.forEach(data.companias, function(compania){
		        $scope.selectedCompaniasId.push(compania.id);
		    });
		    
			 $mdDialog.show({
			      contentElement: '#peritoDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la perito. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Perito cargada correctamente.');
	};
	

	
	//ajax call to get all details of the perito with the given id in the side panel. 
	$scope.removePerito = function(perito){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating perito, this condition ensures getting the index position for later update in the array of peritos
	    if(perito.id != null){
	      for(var i = 0; i < $scope.peritos.length; i++){
	        if($scope.peritos[i].id == perito.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/perito/remove', perito)
	    .success(function (data, status, headers, config){
	      $scope.peritos.splice(index, 1);
	      $scope.clearPerito();
	      
	      $scope.showToast('Perito eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Perito no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the perito object to allow creation of a new one.
	$scope.newPerito = function(){
		
		$scope.clearPerito();
	    
	    $scope.peritoDialogAction = 'Nuevo perito';
	    
		$mdDialog.show({
		      contentElement: '#peritoDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearPerito = function(){
		$scope.perito = new Object();		
	    $scope.selectedCompaniasId = [];
	    $scope.selectedCompanias = [];
	    if ($scope.peritoForm != null) {
	    	$scope.peritoForm.$setPristine();
			$scope.peritoForm.$setUntouched();
	    }
	}
	
	
	//save the given perito and all changes applied to that in the repository of this application.
	$scope.savePerito = function(perito){
		var index = -1;
		
		//if updating perito, this condition ensures getting the index position for later update in the array of peritos
		if(perito.id != null){
			for(var i = 0; i < $scope.peritos.length; i++){
				if($scope.peritos[i].id == perito.id){
					index = i;
					break;
				}
			}
		}
		
		perito.companias =  $scope.selectedCompanias;
		
		$http.post('/sinistros/services/perito/save', perito)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created perito to the list of peritos. 
					$scope.peritos.push(data);
				} else { //save the updated perito in the list of peritos.
					$scope.peritos[index] = data;			
				}
				$scope.clearPerito();
				$mdDialog.hide();
				
				$scope.showToast('Perito guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Perito no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.peritosFilter = function(perito) {
		var isMatch = false;
	      
		if ($scope.textPeritoFilter) {
			var parts = $scope.textPeritoFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(perito.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	  
	$scope.isSelected = function(value) {
		return $scope.selectedCompaniasId.indexOf(value) !== -1;
	};
	
});