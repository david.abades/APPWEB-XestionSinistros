sinistrosApp.controller('usuarioController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('usuarios');
	
	$scope.selected = [];
	
	$http.get('/sinistros/services/compania')
	.success(function (data, status, headers, config){
		$scope.companias = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo las compa\u00F1\u00EDas. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all usuarios stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/usuario')
		.success(function (data, status, headers, config){
			$scope.usuarios = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno al obtener los usuarios. Intente de nuevo en unos segundos o contacte con el administrador.');
		});
	
	
	//ajax call to get all details of the usuario with the given id in the side panel. 
	$scope.usuarioDetails = function(id){
		$http.get('/sinistros/services/usuario/'+ id)
		.success(function (data, status, headers, config){
			$scope.usuario = data;
		    $scope.dialogAction = 'Modificar usuario: ' + data.nombre;
			 $mdDialog.show({
			      contentElement: '#userDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando el usuario. Intente de nuevo en unos segundos o contacte con el administrador.');
			return;
		});
		
		$scope.showToast('Usuario cargado correctamente.');
	};
	
	//ajax call to get all details of the usuario with the given id in the side panel. 
	$scope.removeUsuario = function(usuario){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating usuario, this condition ensures getting the index position for later update in the array of usuarios
	    if(usuario.id != null){
	      for(var i = 0; i < $scope.usuarios.length; i++){
	        if($scope.usuarios[i].id == usuario.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/usuario/remove', usuario)
	    .success(function (data, status, headers, config){
	      $scope.usuarios.splice(index, 1);
	      $scope.clearUsuario();
	      
	      $scope.showToast('Usuario eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Usuario no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the usuario object to allow creation of a new one.
	$scope.newUsuario = function(){
		
		$scope.clearUsuario();
	    
	    $scope.dialogAction = 'Nuevo usuario';
	    
		$mdDialog.show({
		      contentElement: '#userDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearUsuario = function(){
		$scope.usuario = new Object();
		$scope.usuario.rol = 'ROLE_USER';
		$scope.usuarioForm.$setPristine();
		$scope.usuarioForm.$setUntouched();
	}
	
	
	//save the given usuario and all changes applied to that in the repository of this application.
	$scope.saveUsuario = function(usuario){
		var index = -1;
		
		//if updating usuario, this condition ensures getting the index position for later update in the array of usuarios
		if(usuario.id != null){
			for(var i = 0; i < $scope.usuarios.length; i++){
				if($scope.usuarios[i].id == usuario.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/usuario/save', usuario)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created usuario to the list of usuarios. 
					$scope.usuarios.push(data);
				} else { //save the updated usuario in the list of usuarios.
					$scope.usuarios[index] = data;			
				}
				
				$scope.clearUsuario();
				$mdDialog.hide();
				
				$scope.showToast('Usuario guardado.');
				
			})
			.error(function (data, status, headers, config){
				$scope.showToast('Usuario no guardado. Se ha producido un error. Intente de nuevo en unos segundos o contacte con el administrador.');
				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.usuariosFilter = function(usuario) {
		var isMatch = false;
	      
		if ($scope.textUsuariosFilter) {
			var parts = $scope.textUsuariosFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(usuario.nombre.toLowerCase()) 
				|| new RegExp(part.toLowerCase()).test(usuario.username.toLowerCase())
				|| new RegExp(part.toLowerCase()).test(usuario.compania.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	  
	$scope.isSelectedCompania = function(usuario, compania) {
		var seleccionada = (usuario.compania.id === compania.id);
		return usuario.compania.id === compania.id;
	};
	
});