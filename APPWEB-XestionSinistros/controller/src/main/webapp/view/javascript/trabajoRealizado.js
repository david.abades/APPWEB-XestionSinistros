sinistrosApp.controller('trabajoRealizadoController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('partesTrabajo');
	
	$scope.isNewTrabajoRealizado = false;
	$scope.trabajoRealizadoForm = null;
	
	$scope.textPeritoKeyDown = function($event) {
		$event.stopPropagation();
	};
	
	//ajax call to get all trabajoRealizados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/expediente')
		.success(function (data, status, headers, config){
			$scope.expedientes = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Expedientes. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all trabajoRealizados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/gremio')
		.success(function (data, status, headers, config){
			$scope.gremios = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Gremios. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all trabajoRealizados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/reparador')
		.success(function (data, status, headers, config){
			$scope.reparadores = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Reparador. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all trabajoRealizados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/trabajoRealizado')
		.success(function (data, status, headers, config){
			$scope.trabajoRealizados = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Partes de trabajo. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all details of the trabajoRealizado with the given id in the side panel. 
	$scope.trabajoRealizadoDetails = function(id){
		$http.get('/sinistros/services/trabajoRealizado/'+ id)
		.success(function (data, status, headers, config){
			$scope.trabajoRealizado = data;
		    $scope.trabajoRealizadoDialogAction = 'Modificar Trabajo realizado: ' + data.nombre;
		    
			$mdDialog.show({
			      contentElement: '#trabajoRealizadoDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			});
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando los Partes de trabajo. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Trabajo realizado cargada correctamente.');
	};
	

	
	//ajax call to get all details of the trabajoRealizado with the given id in the side panel. 
	$scope.removeTrabajoRealizado = function(trabajoRealizado){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating trabajoRealizado, this condition ensures getting the index position for later update in the array of trabajoRealizados
	    if(trabajoRealizado.id != null){
	      for(var i = 0; i < $scope.trabajoRealizados.length; i++){
	        if($scope.trabajoRealizados[i].id == trabajoRealizado.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/trabajoRealizado/remove', trabajoRealizado)
	    .success(function (data, status, headers, config){
	      $scope.trabajoRealizados.splice(index, 1);
	      $scope.clearTrabajoRealizado();
	      
	      $scope.showToast('Trabajo realizado eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Trabajo realizado no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the trabajoRealizado object to allow creation of a new one.
	$scope.newTrabajoRealizado = function(parteTrabajo){
		
		$scope.clearTrabajoRealizado();
	    
	    $scope.trabajoRealizadoDialogAction = 'Nuevo Trabajo realizado';
		
		$scope.isNewTrabajoRealizado = true;
		
		$scope.trabajoRealizado.parteTrabajo = parteTrabajo;
	    
		$mdDialog.show({
		      contentElement: '#trabajoRealizadoDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true,
		      preserveScope: true,
              autoWrap: true,
              skipHide: true
		});
	};
	
	$scope.clearTrabajoRealizado = function(){
		$scope.trabajoRealizado = new Object();
		if ($scope.trabajoRealizadoForm != null) {
			$scope.trabajoRealizadoForm.$setPristine();
			$scope.trabajoRealizadoForm.$setUntouched();
		}
		
		$scope.isNewTrabajoRealizado = false;
	}
	
	
	//save the given trabajoRealizado and all changes applied to that in the repository of this application.
	$scope.saveTrabajoRealizado = function(trabajoRealizado){
		var index = -1;
		
		//if updating trabajoRealizado, this condition ensures getting the index position for later update in the array of trabajoRealizados
		if(trabajoRealizado.id != null){
			for(var i = 0; i < $scope.trabajoRealizados.length; i++){
				if($scope.trabajoRealizados[i].id == trabajoRealizado.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/trabajoRealizado/save', trabajoRealizado)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created trabajoRealizado to the list of trabajoRealizados. 
					$scope.trabajoRealizados.push(data);
				} else { //save the updated trabajoRealizado in the list of trabajoRealizados.
					$scope.trabajoRealizados[index] = data;			
				}
				$scope.clearTrabajoRealizado();
				$mdDialog.hide();
				
				$scope.showToast('Trabajo realizado guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Trabajo realizado no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.trabajoRealizadoFilter = function(trabajoRealizado) {
		var isMatch = false;
	      
		if ($scope.selectTrabajoRealizadoFilter) {
			var expediente = $scope.selectTrabajoRealizadoFilter;	        
				isMatch = (expediente == trabajoRealizado.expediente);
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	
});