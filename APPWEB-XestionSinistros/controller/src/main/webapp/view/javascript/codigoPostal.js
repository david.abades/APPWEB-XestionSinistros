sinistrosApp.controller('codigoPostalController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('codigosPostales');
	
	$scope.codigoPostalForm = null;
	
	//ajax call to get all codigosPostales stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/codigoPostal')
		.success(function (data, status, headers, config){
			$scope.codigosPostales = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los C\u00F3digos Postales. Intente de nuevo en unos segundos o contacte con el administrador.');

		});
	
	
	//ajax call to get all details of the codigoPostal with the given id in the side panel. 
	$scope.codigoPostalDetails = function(id){
		$http.get('/sinistros/services/codigoPostal/'+ id)
		.success(function (data, status, headers, config){
			$scope.codigoPostal = data;
		    $scope.codigoPostalDialogAction = 'Modificar C\u00F3digo Postal: ' + data.codigo;
			$mdDialog.show({
			      contentElement: '#codigoPostalDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando el C\u00F3digo Postal. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('C\u00F3digo Postal cargada correctamente.');
	};
	
	//ajax call to get all details of the codigoPostal with the given id in the side panel. 
	$scope.removeCodigoPostal = function(codigoPostal){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating codigoPostal, this condition ensures getting the index position for later update in the array of codigosPostales
	    if(codigoPostal.id != null){
	      for(var i = 0; i < $scope.codigosPostales.length; i++){
	        if($scope.codigosPostales[i].id == codigoPostal.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/codigoPostal/remove', codigoPostal)
	    .success(function (data, status, headers, config){
	      $scope.codigosPostales.splice(index, 1);
	      $scope.clearCodigoPostal();
	      
	      $scope.showToast('C\u00F3digo Postal eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('C\u00F3digo Postal no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the codigoPostal object to allow creation of a new one.
	$scope.newCodigoPostal = function(){
		
		$scope.clearCodigoPostal();
	    
	    $scope.codigoPostalDialogAction = 'Nuevo C\u00F3digo Postal';
	    
		$mdDialog.show({
		      contentElement: '#codigoPostalDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true,
		      preserveScope: true,
              autoWrap: true,
              skipHide: true
		    });
	};
	
	$scope.clearCodigoPostal = function(){
		$scope.codigoPostal = new Object();
		if ($scope.codigoPostalForm != null) {
			$scope.codigoPostalForm.$setPristine();
			$scope.codigoPostalForm.$setUntouched();
		}
	}
	
	
	//save the given codigoPostal and all changes applied to that in the repository of this application.
	$scope.saveCodigoPostal = function(codigoPostal){
		var index = -1;
		var saveItem = true;
		
		//if updating codigoPostal, this condition ensures getting the index position for later update in the array of codigosPostales
		for(var i = 0; i < $scope.codigosPostales.length; i++){
			if($scope.codigosPostales[i].codigo == codigoPostal.codigo){
				$scope.showToast('C\u00F3digo Postal ya existente.');
				saveItem = false;
			}
				
			if(codigoPostal.id != null && $scope.codigosPostales[i].id == codigoPostal.id){
				index = i;
			}
		};
		
		if (saveItem) {
			$http.post('/sinistros/services/codigoPostal/save', codigoPostal)
				.success(function (data, status, headers, config){
					if(index < 0){ // add created codigoPostal to the list of codigosPostales. 
						$scope.codigosPostales.push(data);
					} else { //save the updated codigoPostal in the list of codigosPostales.
						$scope.codigosPostales[index] = data;			
					}
				
					$scope.clearCodigoPostal();
					$mdDialog.hide();
				
					$scope.showToast('C\u00F3digo Postal guardado.');

				})
				.error(function (data, status, headers, config){
					$scope.showToast('C\u00F3digo Postal no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

					return;
				});
		};
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.codigoPostalFilter = function(codigoPostal) {
		var isMatch = false;
	      
		if ($scope.textCodigoPostalFilter) {
			var parts = $scope.textCodigoPostalFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(codigoPostal.codigo) 
							|| new RegExp(part.toLowerCase()).test(codigoPostal.provincia.toLowerCase())
							|| new RegExp(part.toLowerCase()).test(codigoPostal.poblacion.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	
});