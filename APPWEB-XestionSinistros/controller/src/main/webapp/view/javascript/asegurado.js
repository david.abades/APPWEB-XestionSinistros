sinistrosApp.controller('aseguradoController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('codigoPostalController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast,
		$http: $http, 
		$controller: $controller}));
	
	$scope.seleccionarMenu('asegurados');
	$scope.aseguradoForm = null;
		
	//ajax call to get all asegurados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/asegurado')
		.success(function (data, status, headers, config){
			$scope.asegurados = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las asegurados. Intente de nuevo en unos segundos o contacte con el administrador.');

	});	
	
	$scope.textCodigosPostalesKeyDown = function($event) {
		$event.stopPropagation();
	}
	
	//ajax call to get all details of the asegurado with the given id in the side panel. 
	$scope.aseguradoDetails = function(id){
		$http.get('/sinistros/services/asegurado/'+ id)
		.success(function (data, status, headers, config){
			$scope.asegurado = data;
		    $scope.aseguradoDialogAction = 'Modificar asegurado: ' + data.nombre;
			 $mdDialog.show({
			      contentElement: '#aseguradoDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la asegurado. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Asegurado cargada correctamente.');
	};
	

	
	//ajax call to get all details of the asegurado with the given id in the side panel. 
	$scope.removeAsegurado = function(asegurado){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating asegurado, this condition ensures getting the index position for later update in the array of asegurados
	    if(asegurado.id != null){
	      for(var i = 0; i < $scope.asegurados.length; i++){
	        if($scope.asegurados[i].id == asegurado.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/asegurado/remove', asegurado)
	    .success(function (data, status, headers, config){
	      $scope.asegurados.splice(index, 1);
	      $scope.clearAsegurado();
	      
	      $scope.showToast('Asegurado eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Asegurado no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the asegurado object to allow creation of a new one.
	$scope.newAsegurado = function(){
	    
	    $scope.aseguradoDialogAction = 'Nuevo asegurado';
	    
		$mdDialog.show({
		      contentElement: '#aseguradoDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
		
		$scope.clearAsegurado();
	};
	
	$scope.clearAsegurado = function(){
		$scope.asegurado = new Object();
		if ($scope.aseguradoForm != null) {
			$scope.aseguradoForm.$setPristine();
			$scope.aseguradoForm.$setUntouched();
		}
	}
	
	
	//save the given asegurado and all changes applied to that in the repository of this application.
	$scope.saveAsegurado = function(asegurado){
		var index = -1;
		
		//if updating asegurado, this condition ensures getting the index position for later update in the array of asegurados
		if(asegurado.id != null){
			for(var i = 0; i < $scope.asegurados.length; i++){
				if($scope.asegurados[i].id == asegurado.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/asegurado/save', asegurado)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created asegurado to the list of asegurados. 
					$scope.asegurados.push(data);
				} else { //save the updated asegurado in the list of asegurados.
					$scope.asegurados[index] = data;			
				}
				$scope.clearAsegurado();
				$mdDialog.hide();
				
				$scope.showToast('Asegurado guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Asegurado no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.aseguradosFilter = function(asegurado) {
		var isMatch = false;
	      
		if ($scope.textAseguradoFilter) {
			var parts = $scope.textAseguradoFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(asegurado.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
		
	/**
	 * Filtro de busqueda
	 */
	$scope.codigosPostalesFilter = function(codigoPostal) {
		var isMatch = false;
	      
		if ($scope.textCodigosPostalesFilter) {
			var parts = $scope.textCodigosPostalesFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(codigoPostal.poblacion.toLowerCase())
						|| new RegExp(part.toLowerCase()).test(codigoPostal.provincia.toLowerCase())
						|| new RegExp(part.toLowerCase()).test(codigoPostal.codigo);
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
});