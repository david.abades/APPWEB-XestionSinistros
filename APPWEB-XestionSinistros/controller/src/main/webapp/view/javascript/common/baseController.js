sinistrosApp.controller('baseController', function($scope, $mdDialog, $mdToast) {
	
	$scope.seleccionarMenu = function(menu) {
		$('lateral-slide-menu a').removeClass('selected');
		$('a[href="#' + menu + '"]').addClass('selected');
	};

	$scope.hide = function() {
		$mdDialog.hide();
	};

	$scope.cancel = function() {
		$mdDialog.cancel();
	};

	$scope.answer = function(answer) {
		$mdDialog.hide(answer);
	};

	$scope.showToast = function(status) {
		$mdToast.show($mdToast.simple({
			  hideDelay: 3000,
			  position: 'top right',
			  content: status,
			  toastClass: 'error'}));
	};
});