sinistrosApp.controller('parametroAplicacionController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('parametroAplicacions');
	
	//ajax call to get all parametroAplicacions stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/parametroAplicacion')
		.success(function (data, status, headers, config){
			$scope.parametroAplicacions = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Par\u00E1metros Aplicaci\u00F3n. Intente de nuevo en unos segundos o contacte con el administrador.');

		});
	
	
	//ajax call to get all details of the parametroAplicacion with the given id in the side panel. 
	$scope.parametroAplicacionDetails = function(id){
		$http.get('/sinistros/services/parametroAplicacion/'+ id)
		.success(function (data, status, headers, config){
			$scope.parametroAplicacion = data;
		    $scope.dialogAction = 'Modificar Par\u00E1metro Aplicaci\u00F3n: ' + data.nombre;
			 $mdDialog.show({
			      contentElement: '#parametroAplicacionDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la Par\u00E1metro Aplicaci\u00F3n. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Par\u00E1metro Aplicaci\u00F3n cargada correctamente.');
	};
	

	
	//ajax call to get all details of the parametroAplicacion with the given id in the side panel. 
	$scope.removeParametroAplicacion = function(parametroAplicacion){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating parametroAplicacion, this condition ensures getting the index position for later update in the array of parametroAplicacions
	    if(parametroAplicacion.id != null){
	      for(var i = 0; i < $scope.parametroAplicacions.length; i++){
	        if($scope.parametroAplicacions[i].id == parametroAplicacion.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/parametroAplicacion/remove', parametroAplicacion)
	    .success(function (data, status, headers, config){
	      $scope.parametroAplicacions.splice(index, 1);
	      $scope.clearParametroAplicacion();
	      
	      $scope.showToast('Par\u00E1metro Aplicaci\u00F3n eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Par\u00E1metro Aplicaci\u00F3n no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the parametroAplicacion object to allow creation of a new one.
	$scope.newParametroAplicacion = function(){
		
		$scope.clearParametroAplicacion();
	    
	    $scope.dialogAction = 'Nuevo Par\u00E1metro Aplicaci\u00F3n';
	    
		$mdDialog.show({
		      contentElement: '#parametroAplicacionDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearParametroAplicacion = function(){
		$scope.parametroAplicacion = new Object();
		$scope.parametroAplicacionForm.$setPristine();
		$scope.parametroAplicacionForm.$setUntouched();
	}
	
	
	//save the given parametroAplicacion and all changes applied to that in the repository of this application.
	$scope.saveParametroAplicacion = function(parametroAplicacion){
		var index = -1;
		
		//if updating parametroAplicacion, this condition ensures getting the index position for later update in the array of parametroAplicacions
		if(parametroAplicacion.id != null){
			for(var i = 0; i < $scope.parametroAplicacions.length; i++){
				if($scope.parametroAplicacions[i].id == parametroAplicacion.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/parametroAplicacion/save', parametroAplicacion)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created parametroAplicacion to the list of parametroAplicacions. 
					$scope.parametroAplicacions.push(data);
				} else { //save the updated parametroAplicacion in the list of parametroAplicacions.
					$scope.parametroAplicacions[index] = data;			
				}
				$scope.clearParametroAplicacion();
				$mdDialog.hide();
				
				$scope.showToast('Par\u00E1metro Aplicaci\u00F3n guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Par\u00E1metro Aplicaci\u00F3n no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.parametroAplicacionsFilter = function(parametroAplicacion) {
		var isMatch = false;
	      
		if ($scope.textPar\u00E1metroAplicacionsFilter) {
			var parts = $scope.textParametroAplicacionsFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(parametroAplicacion.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	
});