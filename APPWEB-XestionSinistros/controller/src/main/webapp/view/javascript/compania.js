sinistrosApp.controller('companiaController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('companias');
	
	//ajax call to get all companias stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/compania')
		.success(function (data, status, headers, config){
			$scope.companias = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las compa\u00F1\u00EDas. Intente de nuevo en unos segundos o contacte con el administrador.');

		});
	
	
	//ajax call to get all details of the compania with the given id in the side panel. 
	$scope.companiaDetails = function(id){
		$http.get('/sinistros/services/compania/'+ id)
		.success(function (data, status, headers, config){
			$scope.compania = data;
		    $scope.dialogAction = 'Modificar compa\u00F1\u00EDa: ' + data.nombre;
			 $mdDialog.show({
			      contentElement: '#companiaDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la compa\u00F1\u00EDa. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Compa\u00F1\u00EDa cargada correctamente.');
	};
	
	//ajax call to get all details of the compania with the given id in the side panel. 
	$scope.removeCompania = function(compania){
		    var confirm = $mdDialog.confirm()
		          .title('Se va a proceder a eliminar el registro.')
		          .textContent('Confirma la eliminaci\u00F3n?')
		          .ariaLabel('Eliminaci\u00F3n')
		          .ok('Aceptar')
		          .cancel('Cancelar');
		    
		    $mdDialog.show(confirm).then(function() {
		    	var index = -1;
		    	
				//if updating compania, this condition ensures getting the index position for later update in the array of companias
				if(compania.id != null){
					for(var i = 0; i < $scope.companias.length; i++){
						if($scope.companias[i].id == compania.id){
							index = i;
							break;
						}
					}
				}
				
				$http.post('/sinistros/services/compania/remove', compania)
				.success(function (data, status, headers, config){
					$scope.companias.splice(index, 1);
					$scope.clearCompania();

					$scope.showToast('Compa\u00F1\u00EDa eliminada.');
				})
				.error(function (data, status, headers, config){

					$scope.showToast('Compa\u00F1\u00EDa no eliminada. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

					return;
				});
		      }, function() {
		    	  $scope.showToast('Eliminaci\u00F3n cancelada.');
		      });
	};
	
	
	//clean the compania object to allow creation of a new one.
	$scope.newCompania = function(){
		
		$scope.clearCompania();
	    
	    $scope.dialogAction = 'Nueva compa\u00F1\u00EDa';
	    
		$mdDialog.show({
		      contentElement: '#companiaDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearCompania = function(){
		$scope.compania = new Object();
		$scope.companiaForm.$setPristine();
		$scope.companiaForm.$setUntouched();
	}
	
	
	//save the given compania and all changes applied to that in the repository of this application.
	$scope.saveCompania = function(compania){
		var index = -1;
		
		//if updating compania, this condition ensures getting the index position for later update in the array of companias
		if(compania.id != null){
			for(var i = 0; i < $scope.companias.length; i++){
				if($scope.companias[i].id == compania.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/compania/save', compania)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created compania to the list of companias. 
					$scope.companias.push(data);
				} else { //save the updated compania in the list of companias.
					$scope.companias[index] = data;			
				}
				$scope.clearCompania();
				$mdDialog.hide();
				
				$scope.showToast('Compa\u00F1\u00EDa guardada.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Compa\u00F1\u00EDa no guardada. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.companiasFilter = function(compania) {
		var isMatch = false;
	      
		if ($scope.textCompaniasFilter) {
			var parts = $scope.textCompaniasFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(compania.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	
});