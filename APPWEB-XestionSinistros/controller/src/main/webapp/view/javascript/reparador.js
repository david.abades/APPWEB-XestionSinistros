sinistrosApp.controller('reparadorController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));	
	
	$scope.seleccionarMenu('reparadores');
	
    $scope.selectedGremiosId = [];
    $scope.selectedGremios = [];
	
	//ajax call to get all reparadores stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/reparador')
		.success(function (data, status, headers, config){
			$scope.reparadores = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las reparadores. Intente de nuevo en unos segundos o contacte con el administrador.');

		});
	
	$http.get('/sinistros/services/gremio')
	.success(function (data, status, headers, config){
		$scope.gremios = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo las gremios. Intente de nuevo en unos segundos o contacte con el administrador.');

	});	
	
	//ajax call to get all details of the reparador with the given id in the side panel. 
	$scope.reparadorDetails = function(id){
		$http.get('/sinistros/services/reparador/'+ id)
		.success(function (data, status, headers, config){
			$scope.reparador = data;
		    $scope.dialogAction = 'Modificar reparador: ' + data.nombre;
		    angular.forEach(data.gremios, function(gremio){
		        $scope.selectedGremiosId.push(gremio.id);
		    });
		    
			$mdDialog.show({
			      contentElement: '#reparadorDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			});
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la reparador. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Reparador cargada correctamente.');
	};
	

	
	//ajax call to get all details of the reparador with the given id in the side panel. 
	$scope.removeReparador = function(reparador){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating reparador, this condition ensures getting the index position for later update in the array of reparadores
	    if(reparador.id != null){
	      for(var i = 0; i < $scope.reparadores.length; i++){
	        if($scope.reparadores[i].id == reparador.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/reparador/remove', reparador)
	    .success(function (data, status, headers, config){
	      $scope.reparadores.splice(index, 1);
	      $scope.clearReparador();
	      
	      $scope.showToast('Reparador eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Reparador no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the reparador object to allow creation of a new one.
	$scope.newReparador = function(){
		
		$scope.clearReparador();
	    
	    $scope.dialogAction = 'Nuevo reparador';
	    
		$mdDialog.show({
		      contentElement: '#reparadorDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearReparador = function(){
		$scope.reparador = new Object();
		$scope.reparador.tipo = 'P';
	    $scope.selectedGremiosId = [];
	    $scope.selectedGremios = [];
		$scope.reparadorForm.$setPristine();
		$scope.reparadorForm.$setUntouched();
	}
	
	
	//save the given reparador and all changes applied to that in the repository of this application.
	$scope.saveReparador = function(reparador){
		var index = -1;
		
		//if updating reparador, this condition ensures getting the index position for later update in the array of reparadores
		if(reparador.id != null){
			for(var i = 0; i < $scope.reparadores.length; i++){
				if($scope.reparadores[i].id == reparador.id){
					index = i;
					break;
				}
			}
		}
		
		reparador.gremios = $scope.selectedGremios;
		
		$http.post('/sinistros/services/reparador/save', reparador)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created reparador to the list of reparadores. 
					$scope.reparadores.push(data);
				} else { //save the updated reparador in the list of reparadores.
					$scope.reparadores[index] = data;			
				}
				$scope.clearReparador();
				$mdDialog.hide();
				
				$scope.showToast('Reparador guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Reparador no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.reparadoresFilter = function(reparador) {
		var isMatch = false;
	      
		if ($scope.textReparadoresFilter) {
			var parts = $scope.textReparadoresFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(reparador.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	  
	$scope.isSelected = function(value) {
		return $scope.selectedGremiosId.indexOf(value) !== -1;
	};
	
});