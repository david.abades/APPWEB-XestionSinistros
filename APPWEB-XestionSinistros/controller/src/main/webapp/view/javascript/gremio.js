sinistrosApp.controller('gremioController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('gremios');
	
	//ajax call to get all gremios stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/gremio')
		.success(function (data, status, headers, config){
			$scope.gremios = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las gremios. Intente de nuevo en unos segundos o contacte con el administrador.');

	});	
	
	//ajax call to get all details of the gremio with the given id in the side panel. 
	$scope.gremioDetails = function(id){
		$http.get('/sinistros/services/gremio/'+ id)
		.success(function (data, status, headers, config){
			$scope.gremio = data;
		    $scope.dialogAction = 'Modificar gremio: ' + data.nombre;
			$mdDialog.show({
			      contentElement: '#gremioDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la gremio. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Gremio cargada correctamente.');
	};
	
	//ajax call to get all details of the gremio with the given id in the side panel. 
	$scope.removeGremio = function(gremio){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating gremio, this condition ensures getting the index position for later update in the array of gremios
	    if(gremio.id != null){
	      for(var i = 0; i < $scope.gremios.length; i++){
	        if($scope.gremios[i].id == gremio.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/gremio/remove', gremio)
	    .success(function (data, status, headers, config){
	      $scope.gremios.splice(index, 1);
	      $scope.clearGremio();
	      
	      $scope.showToast('Gremio eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Gremio no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the gremio object to allow creation of a new one.
	$scope.newGremio = function(){
		
		$scope.clearGremio();
	    
	    $scope.dialogAction = 'Nuevo gremio';
	    
		$mdDialog.show({
		      contentElement: '#gremioDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearGremio = function(){
		$scope.gremio = new Object();
		$scope.gremioForm.$setPristine();
		$scope.gremioForm.$setUntouched();
	}
	
	
	//save the given gremio and all changes applied to that in the repository of this application.
	$scope.saveGremio = function(gremio){
		var index = -1;
		
		//if updating gremio, this condition ensures getting the index position for later update in the array of gremios
		if(gremio.id != null){
			for(var i = 0; i < $scope.gremios.length; i++){
				if($scope.gremios[i].id == gremio.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/gremio/save', gremio)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created gremio to the list of gremios. 
					$scope.gremios.push(data);
				} else { //save the updated gremio in the list of gremios.
					$scope.gremios[index] = data;			
				}
				$scope.clearGremio();
				$mdDialog.hide();
				
				$scope.showToast('Gremio guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Gremio no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.gremiosFilter = function(gremio) {
		var isMatch = false;
	      
		if ($scope.textGremiosFilter) {
			var parts = $scope.textGremiosFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(gremio.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	
});