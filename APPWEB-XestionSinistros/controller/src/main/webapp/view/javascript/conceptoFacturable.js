sinistrosApp.controller('conceptoFacturableController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('conceptosFacturables');
	
	$('#textCodigosPostalesFilter').on('keydown', function(ev) {
        ev.stopPropagation();
    });
	
	//ajax call to get all conceptoFacturablees stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/conceptoFacturable')
		.success(function (data, status, headers, config){
			$scope.conceptosFacturables = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Conceptos Facturables. Intente de nuevo en unos segundos o contacte con el administrador.');

		});
	
	$http.get('/sinistros/services/compania')
	.success(function (data, status, headers, config){
		$scope.companias = data;
	})
	.error(function (data, status, headers, config){
		$scope.showToast('Error interno obteniendo las compan\u00F1\u00EDas. Intente de nuevo en unos segundos o contacte con el administrador.');

	});	
	
	//ajax call to get all details of the conceptoFacturable with the given id in the side panel. 
	$scope.conceptoFacturableDetails = function(id){
		$http.get('/sinistros/services/conceptoFacturable/'+ id)
		.success(function (data, status, headers, config){
			$scope.conceptoFacturable = data;
		    $scope.dialogAction = 'Modificar Concepto Facturable: ' + data.codigo;
			 $mdDialog.show({
			      contentElement: '#conceptoFacturableDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			    });
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando el Concepto Facturable. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Concepto Facturable cargado correctamente.');
	};
	

	
	//ajax call to get all details of the conceptoFacturable with the given id in the side panel. 
	$scope.removeConceptoFacturable = function(conceptoFacturable){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating conceptoFacturable, this condition ensures getting the index position for later update in the array of conceptoFacturablees
	    if(conceptoFacturable.id != null){
	      for(var i = 0; i < $scope.conceptosFacturables.length; i++){
	        if($scope.conceptosFacturables[i].id == conceptoFacturable.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/conceptoFacturable/remove', conceptoFacturable)
	    .success(function (data, status, headers, config){
	      $scope.conceptosFacturables.splice(index, 1);
	      $scope.clearConceptoFacturable();
	      
	      $scope.showToast('Concepto Facturable eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Concepto Facturable no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the conceptoFacturable object to allow creation of a new one.
	$scope.newConceptoFacturable = function(){
		
		$scope.clearConceptoFacturable();
	    
	    $scope.dialogAction = 'Nuevo Concepto Facturable';
	    
		$mdDialog.show({
		      contentElement: '#conceptoFacturableDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		    });
	};
	
	$scope.clearConceptoFacturable = function(){
		$scope.conceptoFacturable = new Object();
		$scope.conceptoFacturableForm.$setPristine();
		$scope.conceptoFacturableForm.$setUntouched();
	}
	
	
	//save the given conceptoFacturable and all changes applied to that in the repository of this application.
	$scope.saveConceptoFacturable = function(conceptoFacturable){
		var index = -1;
		
		//if updating conceptoFacturable, this condition ensures getting the index position for later update in the array of conceptoFacturablees
		if(conceptoFacturable.id != null){
			for(var i = 0; i < $scope.conceptosFacturables.length; i++){
				if($scope.conceptosFacturables[i].id == conceptoFacturable.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/conceptoFacturable/save', conceptoFacturable)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created conceptoFacturable to the list of conceptoFacturablees. 
					$scope.conceptosFacturables.push(data);
				} else { //save the updated conceptoFacturable in the list of conceptoFacturablees.
					$scope.conceptosFacturables[index] = data;			
				}
				$scope.clearConceptoFacturable();
				$mdDialog.hide();
				
				$scope.showToast('Concepto Facturable guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Concepto Facturable no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.conceptoFacturableFilter = function(conceptoFacturable) {
		var isMatch = false;
	      
		if ($scope.textConceptoFacturableFilter) {
			var parts = $scope.textConceptoFacturableFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(conceptoFacturable.codigo.toLowerCase())
						|| new RegExp(part.toLowerCase()).test(conceptoFacturable.descripcion.toLowerCase())
						|| new RegExp(part.toLowerCase()).test(conceptoFacturable.compania.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	
});