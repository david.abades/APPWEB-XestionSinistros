// Creacion del modulo
var sinistrosApp = angular.module('sinistrosApp', ['ngRoute','angular-loading-bar','ngMaterial','ngMessages','md.data.table','fixed.table.header']);

// Configuracion de las rutas
sinistrosApp.config(function($mdThemingProvider, $routeProvider, $mdDateLocaleProvider) {

    $routeProvider
        .when('/companias', {
            templateUrl : 'compania.html',
            controller  : 'companiaController',
            bindToController: 'true'
        })
        .when('/usuarios', {
            templateUrl : 'usuario.html',
            controller  : 'usuarioController',
            bindToController: 'true'
        })
        .when('/gremios', {
            templateUrl : 'gremio.html',
            controller  : 'gremioController',
            bindToController: 'true'
        })
        .when('/peritos', {
            templateUrl : 'perito.html',
            controller  : 'peritoController',
            bindToController: 'true'
        })
        .when('/reparadores', {
            templateUrl : 'reparador.html',
            controller  : 'reparadorController',
            bindToController: 'true'
        })
        .when('/codigosPostales', {
            templateUrl : 'codigoPostal.html',
            controller  : 'codigoPostalController',
            bindToController: 'true'
        })
        .when('/asegurados', {
            templateUrl : 'asegurado.html',
            controller  : 'aseguradoController',
            bindToController: 'true'
        })
        .when('/conceptosFacturables', {
            templateUrl : 'conceptoFacturable.html',
            controller  : 'conceptoFacturableController',
            bindToController: 'true'
        })
        .when('/parametrosAplicacion', {
            templateUrl : 'parametroAplicacion.html',
            controller  : 'parametroAplicacionController',
            bindToController: 'true'
        })
        .when('/expedientes', {
            templateUrl : 'expediente.html',
            controller  : 'expedienteController',
            bindToController: 'true'
        })
        .when('/partesTrabajo', {
            templateUrl : 'parteTrabajo.html',
            controller  : 'parteTrabajoController',
            bindToController: 'true'
        })
        .when('/perjudicados', {
            templateUrl : 'perjudicado.html',
            controller  : 'perjudicadoController',
            bindToController: 'true'
        })
        .otherwise({
            redirectTo: '/'
        });
    
    $mdDateLocaleProvider.formatDate = function(date) {
    	return date ? moment(date).format('DD/MM/YYYY') : '';
    };
    
    $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'DD/MM/YYYY', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };
});
