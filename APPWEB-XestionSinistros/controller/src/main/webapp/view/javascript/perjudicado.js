sinistrosApp.controller('perjudicadoController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));	
	
	$scope.seleccionarMenu('perjudicados');
	$scope.perjudicadoForm = null;
	
	//ajax call to get all perjudicados stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/perjudicado')
		.success(function (data, status, headers, config){
			$scope.perjudicados = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo las perjudicados. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all details of the perjudicado with the given id in the side panel. 
	$scope.perjudicadoDetails = function(id){
		$http.get('/sinistros/services/perjudicado/'+ id)
		.success(function (data, status, headers, config){
			$scope.perjudicado = data;
		    $scope.perjudicadoDialogAction = 'Modificar perjudicado: ' + data.nombre;
		    
			$mdDialog.show({
			      contentElement: '#perjudicadoDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true,
			      fullscreen: $scope.customFullscreen
			});
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando la perjudicado. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Perjudicado cargada correctamente.');
	};
	

	
	//ajax call to get all details of the perjudicado with the given id in the side panel. 
	$scope.removePerjudicado = function(perjudicado){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating perjudicado, this condition ensures getting the index position for later update in the array of perjudicados
	    if(perjudicado.id != null){
	      for(var i = 0; i < $scope.perjudicados.length; i++){
	        if($scope.perjudicados[i].id == perjudicado.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/perjudicado/remove', perjudicado)
	    .success(function (data, status, headers, config){
	      $scope.perjudicados.splice(index, 1);
	      $scope.clearPerjudicado();
	      
	      $scope.showToast('Perjudicado eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Perjudicado no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the perjudicado object to allow creation of a new one.
	$scope.newPerjudicado = function(expediente){
		
		$scope.clearPerjudicado();
	    
	    $scope.perjudicadoDialogAction = 'Nuevo perjudicado';
	    
	    $scope.perjudicado.expediente = expediente;
	    
		$mdDialog.show({
		      contentElement: '#perjudicadoDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true,
		      fullscreen: $scope.customFullscreen
		    });
	};
	
	$scope.clearPerjudicado = function(){
		$scope.perjudicado = new Object();
		if ($scope.perjudicadoForm != null) {
			$scope.perjudicadoForm.$setPristine();
			$scope.perjudicadoForm.$setUntouched();
		}
	}
	
	
	//save the given perjudicado and all changes applied to that in the repository of this application.
	$scope.savePerjudicado = function(perjudicado){
		var index = -1;
		
		//if updating perjudicado, this condition ensures getting the index position for later update in the array of perjudicados
		if(perjudicado.id != null){
			for(var i = 0; i < $scope.perjudicados.length; i++){
				if($scope.perjudicados[i].id == perjudicado.id){
					index = i;
					break;
				}
			}
		}
		
		perjudicado.expediente = $scope.expediente;
		
		$http.post('/sinistros/services/perjudicado/save', perjudicado)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created perjudicado to the list of perjudicados. 
					$scope.perjudicados.push(data);
				} else { //save the updated perjudicado in the list of perjudicados.
					$scope.perjudicados[index] = data;			
				}
				$scope.clearPerjudicado();
				$mdDialog.hide();
				
				$scope.showToast('Perjudicado guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Perjudicado no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.perjudicadosFilter = function(perjudicado) {
		var isMatch = false;
	      
		if ($scope.textPerjudicadosFilter) {
			var parts = $scope.textPerjudicadosFilter.split(' ');
	        
			parts.forEach(function(part) {
				isMatch = new RegExp(part.toLowerCase()).test(perjudicado.nombre.toLowerCase());
	        });
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	
});