sinistrosApp.controller('parteTrabajoController', function($scope, $mdToast, $mdDialog, $http, $controller){

	angular.extend($controller('baseController', {
		$scope: $scope, 
		$mdDialog: $mdDialog, 
		$mdToast: $mdToast}));
	
	$scope.seleccionarMenu('partesTrabajo');
	
	$scope.isNewParteTrabajo = false;
	$scope.parteTrabajoForm = null;
	
	$scope.textPeritoKeyDown = function($event) {
		$event.stopPropagation();
	};
	
	//ajax call to get all parteTrabajos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/expediente')
		.success(function (data, status, headers, config){
			$scope.expedientes = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Expedientes. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all parteTrabajos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/gremio')
		.success(function (data, status, headers, config){
			$scope.gremios = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Gremios. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all parteTrabajos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/reparador')
		.success(function (data, status, headers, config){
			$scope.reparadores = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Reparador. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all parteTrabajos stored in the application repository. Happens whenever page is loaded.
	$http.get('/sinistros/services/parteTrabajo')
		.success(function (data, status, headers, config){
			$scope.parteTrabajos = data;
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno obteniendo los Partes de trabajo. Intente de nuevo en unos segundos o contacte con el administrador.');

	});
	
	//ajax call to get all details of the parteTrabajo with the given id in the side panel. 
	$scope.parteTrabajoDetails = function(id){
		$http.get('/sinistros/services/parteTrabajo/'+ id)
		.success(function (data, status, headers, config){
			data.fechaParte  = data.fechaParte  === null ? null : new Date(data.fechaParte);
			$scope.parteTrabajo = data;
		    $scope.parteTrabajoDialogAction = 'Modificar Parte de trabajo: ' + data.nombre;
		    
			$mdDialog.show({
			      contentElement: '#parteTrabajoDialog',
			      parent: angular.element(document.body),
			      clickOutsideToClose: true
			});
		})
		.error(function (data, status, headers, config){
			$scope.showToast('Error interno cargando los Partes de trabajo. Intente de nuevo en unos segundos o contacte con el administrador.');
			
			return;
		});
		
		$scope.showToast('Parte de trabajo cargada correctamente.');
	};
	

	
	//ajax call to get all details of the parteTrabajo with the given id in the side panel. 
	$scope.removeParteTrabajo = function(parteTrabajo){
	  var confirm = $mdDialog.confirm()
	  .title('Se va a proceder a eliminar el registro.')
	  .textContent('Confirma la eliminaci\u00F3n?')
	  .ariaLabel('Eliminaci\u00F3n')
	  .ok('Aceptar')
	  .cancel('Cancelar');
	  
	  $mdDialog.show(confirm).then(function() {
	    var index = -1;
	    
	    //if updating parteTrabajo, this condition ensures getting the index position for later update in the array of parteTrabajos
	    if(parteTrabajo.id != null){
	      for(var i = 0; i < $scope.parteTrabajos.length; i++){
	        if($scope.parteTrabajos[i].id == parteTrabajo.id){
	          index = i;
	          break;
	        }
	      }
	    }
	    
	    $http.post('/sinistros/services/parteTrabajo/remove', parteTrabajo)
	    .success(function (data, status, headers, config){
	      $scope.parteTrabajos.splice(index, 1);
	      $scope.clearParteTrabajo();
	      
	      $scope.showToast('Parte de trabajo eliminado.');
	    })
	    .error(function (data, status, headers, config){
	      
	      $scope.showToast('Parte de trabajo no eliminado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');
	      
	      return;
	    });
	  }, function() {
	    $scope.showToast('Eliminaci\u00F3n cancelada.');
	  });
	};
	
	
	//clean the parteTrabajo object to allow creation of a new one.
	$scope.newParteTrabajo = function(expediente){
		
		$scope.clearParteTrabajo();
	    
	    $scope.parteTrabajoDialogAction = 'Nuevo Parte de trabajo';
		
		$scope.isNewParteTrabajo = true;
		
		$scope.parteTrabajo.expediente = expediente;
	    
		$mdDialog.show({
		      contentElement: '#parteTrabajoDialog',
		      parent: angular.element(document.body),
		      clickOutsideToClose: true
		});
	};
	
	$scope.clearParteTrabajo = function(){
		$scope.parteTrabajo = new Object();
		if ($scope.parteTrabajoForm != null) {
			$scope.parteTrabajoForm.$setPristine();
			$scope.parteTrabajoForm.$setUntouched();
		}
		
		$scope.isNewParteTrabajo = false;
	}
	
	
	//save the given parteTrabajo and all changes applied to that in the repository of this application.
	$scope.saveParteTrabajo = function(parteTrabajo){
		var index = -1;
		
		//if updating parteTrabajo, this condition ensures getting the index position for later update in the array of parteTrabajos
		if(parteTrabajo.id != null){
			for(var i = 0; i < $scope.parteTrabajos.length; i++){
				if($scope.parteTrabajos[i].id == parteTrabajo.id){
					index = i;
					break;
				}
			}
		}
		
		$http.post('/sinistros/services/parteTrabajo/save', parteTrabajo)
			.success(function (data, status, headers, config){
				if(index < 0){ // add created parteTrabajo to the list of parteTrabajos. 
					$scope.parteTrabajos.push(data);
				} else { //save the updated parteTrabajo in the list of parteTrabajos.
					$scope.parteTrabajos[index] = data;			
				}
				$scope.clearParteTrabajo();
				$mdDialog.hide();
				
				$scope.showToast('Parte de trabajo guardado.');

			})
			.error(function (data, status, headers, config){
				$scope.showToast('Parte de trabajo no guardado. Se ha producido un error interno. Intente de nuevo en unos segundos o contacte con el administrador.');

				return;
			});
		
	};
	
	/**
	 * Filtro de busqueda
	 */
	$scope.parteTrabajoFilter = function(parteTrabajo) {
		var isMatch = false;
	      
		if ($scope.selectParteTrabajoFilter) {
			var expediente = $scope.selectParteTrabajoFilter;	        
				isMatch = (expediente == parteTrabajo.expediente);
		} else {
			isMatch = true;
		}
	    return isMatch;
	};
	
	/**
	 * Orden
	 */
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	};
	
});